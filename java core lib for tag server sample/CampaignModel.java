/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mgm.core.data.model.campaign;

import com.mgm.core.data.model.BaseModel;
import com.mgm.core.data.model.CountryModel;
import com.mgm.core.data.model.SiteModel;
import java.util.Date;
import java.util.List;

/**
 *
 * @author alexrodin
 */
public class CampaignModel extends BaseModel {
    
    private String title;
    private List<TagOwnedModel> tags;
    private List<SiteModel> sites;
    private List<CountryModel> countries;
    private List<CampaignTagModel> campaignTags;
    private List<CampaignCountryModel> campaignCountries;
    private List<CampaignSiteModel> campaignSites;
    private Integer stateId;
    private double budget;
    private double dailyBudget;
    private double bidPrice;
    private int userCap;
    private Date dateStart;
    private Date dateEnd;
    
    
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<TagOwnedModel> getTags() {
        return tags;
    }

    public void setTags(List<TagOwnedModel> tags) {
        this.tags = tags;
    }

    public List<SiteModel> getSites() {
        return sites;
    }

    public void setSites(List<SiteModel> sites) {
        this.sites = sites;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public double getDailyBudget() {
        return dailyBudget;
    }

    public void setDailyBudget(double dailyBudget) {
        this.dailyBudget = dailyBudget;
    }

    public double getBidPrice() {
        return bidPrice;
    }

    public void setBidPrice(double bidPrice) {
        this.bidPrice = bidPrice;
    }

    public Integer getStateId() {
        return stateId;
    }

    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    public int getUserCap() {
        return userCap;
    }

    public void setUserCap(int userCap) {
        this.userCap = userCap;
    }

    @Override
    public String toString() {
        return "CampaignModel{" + "title=" + title + ", tags=" + tags + ", sites=" + sites + ", stateId=" + stateId + ", budget=" + budget + ", dailyBudget=" + dailyBudget + ", bidPrice=" + bidPrice + ", userCap=" + userCap + '}';
    }

    public List<CountryModel> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryModel> countries) {
        this.countries = countries;
    }

    public List<CampaignTagModel> getCampaignTags() {
        return campaignTags;
    }

    public void setCampaignTags(List<CampaignTagModel> campaignTags) {
        this.campaignTags = campaignTags;
    }

    public List<CampaignCountryModel> getCampaignCountries() {
        return campaignCountries;
    }

    public void setCampaignCountries(List<CampaignCountryModel> campaignCountries) {
        this.campaignCountries = campaignCountries;
    }

    public List<CampaignSiteModel> getCampaignSites() {
        return campaignSites;
    }

    public void setCampaignSites(List<CampaignSiteModel> campaignSites) {
        this.campaignSites = campaignSites;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }
}
