/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mgm.core.data.dao.mapper;

import com.mgm.core.data.model.CountryModel;
import com.mgm.core.data.model.SiteModel;
import com.mgm.core.data.model.campaign.CampaignCountryModel;
import com.mgm.core.data.model.campaign.CampaignModel;
import com.mgm.core.data.model.campaign.CampaignSearchModel;
import com.mgm.core.data.model.campaign.CampaignSiteModel;
import com.mgm.core.data.model.campaign.CampaignStateModel;
import com.mgm.core.data.model.campaign.CampaignTagModel;
import com.mgm.core.data.model.campaign.SiteSellPriceModel;
import com.mgm.core.data.model.campaign.TagOwnedModel;
import java.util.List;

/**
 *
 * @author alexrodin
 */
public interface CampaignMapper extends BaseMapper {
    List<CampaignStateModel> getCampaignStates();
    List<SiteModel> getCampaignSites(CampaignModel model);
    List<TagOwnedModel> getCampaignTags(CampaignModel model);
    List<CountryModel> getCampaignCountries(CampaignModel model);
    List<CampaignSearchModel> getSearchList();
    List<SiteSellPriceModel> getSellPrices();
    void setCampaignCountries(List<CampaignCountryModel> countries);
    void setCampaignTags(List<CampaignTagModel> tags);
    void setCampaignSites(List<CampaignSiteModel> sites);
    void removeMapping(CampaignModel model);
}
