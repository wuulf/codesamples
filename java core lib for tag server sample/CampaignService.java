/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mgm.core.service;

import com.mgm.core.data.dao.campaign.CampaignDAO;
import com.mgm.core.data.model.CountryModel;
import com.mgm.core.data.model.SiteModel;
import com.mgm.core.data.model.campaign.CampaignCountryModel;
import com.mgm.core.data.model.campaign.CampaignModel;
import com.mgm.core.data.model.campaign.CampaignSearchModel;
import com.mgm.core.data.model.campaign.CampaignSiteModel;
import com.mgm.core.data.model.campaign.CampaignStateModel;
import com.mgm.core.data.model.campaign.CampaignTagModel;
import com.mgm.core.data.model.campaign.SiteSellPriceModel;
import com.mgm.core.data.model.campaign.TagOwnedModel;
import java.util.List;

/**
 *
 * @author alexrodin
 */
public class CampaignService {
      
    private static final CampaignDAO dao = new CampaignDAO();

    private CampaignService() {}

    public static CampaignModel getById(Integer id) {
        return dao.getById(id);
    }
    
    public static List<CampaignModel> getAll() {
        return dao.getAll();
    }
    
    public static List<CampaignSearchModel> getSearchList() {
        return dao.getSearchList();
    }

    public static void insert(CampaignModel campaignModel) {
        dao.insert(campaignModel);
    }

    public static void update(CampaignModel campaignModel) {
        dao.update(campaignModel);
    }

    public static void delete(CampaignModel campaignModel) {
        dao.delete(campaignModel);
    }
    
    public static List<CampaignStateModel> getCampaignStates() {
        return dao.getCampaignStates();
    }
    
    public static List<SiteModel> getCampaignSites(CampaignModel model) {
        return dao.getCampaignSites(model);
    }
     
    public static List<TagOwnedModel> getCampaignTags(CampaignModel model) {
        return dao.getCampaignTags(model);
    }
    
    public static List<CountryModel> getCampaignCountries(CampaignModel model) {
        return dao.getCampaignCountries(model);
    }
    
    public static List<SiteSellPriceModel> getSellPrices() {
        return dao.getSellPrices();
    }
    
    public static void setCampaignTags(List<CampaignTagModel> tags) {
        dao.setCampaignTags(tags);
    }
    
    public static void setCampaignSites(List<CampaignSiteModel> sites) {
        dao.setCampaignSites(sites);
    }
    
    public static void setCampaignCountries(List<CampaignCountryModel> countries) {
        dao.setCampaignCountries(countries);
    }
    
    public static void removeMapping(CampaignModel model) {
        dao.removeMapping(model);
    }
}
