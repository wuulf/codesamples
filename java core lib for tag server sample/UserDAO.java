/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mgm.core.data.dao;

import com.mgm.core.data.dao.mapper.UserMapper;
import com.mgm.core.data.model.RoleModel;
import com.mgm.core.data.model.UserModel;
import java.util.List;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author alexrodin
 */
public class UserDAO extends BaseDAO implements UserMapper{
    
    public UserDAO() {
        super(UserMapper.class);
    }

    @Override
    public UserModel getById(Integer id) {
        return (UserModel)super.getById(id);
    }
    
    @Override
    public UserModel getByOldId(Integer oldId) {
        UserModel userModel;
        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = (UserMapper)session.getMapper(super.mapperClass);
            userModel = mapper.getByOldId(oldId);
            session.commit();
        }
        return userModel;
    }

    @Override
    public List<UserModel> getAll() {
        return (List<UserModel>) super.getAll();
    }
    
    @Override
    public Integer getAccountId(UserModel user) {
        Integer result;
        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = (UserMapper)session.getMapper(super.mapperClass);
            result = mapper.getAccountId(user);
            session.commit();
        }
        return result;
    }
    
    @Override
    public Integer exists(UserModel user) {
        Integer result;
        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = (UserMapper)session.getMapper(super.mapperClass);
            result = mapper.exists(user);
            session.commit();
        }
        return result;
    }

    @Override
    public UserModel getByEmail(String email) {
        UserModel userModel;
        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = (UserMapper)session.getMapper(super.mapperClass);
            userModel = mapper.getByEmail(email);
            session.commit();
        }
        return userModel;
    }

    @Override
    public List<UserModel> getByAccount(Integer account_id) {
        List<UserModel> userList;
        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = (UserMapper)session.getMapper(super.mapperClass);
            userList = mapper.getByAccount(account_id);
            session.commit();
        }
        return userList;
    }

    @Override
    public void upsertUser(UserModel userModel) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            UserMapper mapper = (UserMapper) session.getMapper(mapperClass);
            mapper.upsertUser(userModel);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public RoleModel getRoleById(Integer roleId) {
        RoleModel role;
        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = (UserMapper)session.getMapper(super.mapperClass);
            role = mapper.getRoleById(roleId);
            session.commit();
        }
        return role;
    }

    @Override
    public List<RoleModel> getRoles() {
        List<RoleModel> roleList;
        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = (UserMapper)session.getMapper(super.mapperClass);
            roleList = mapper.getRoles();
            session.commit();
        }
        return roleList;    
    }

    @Override
    public void deleteUser(UserModel userModel) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            UserMapper mapper = (UserMapper) session.getMapper(mapperClass);
            mapper.deleteUser(userModel);
            session.commit();
        } finally {
            session.close();
        }
    }
}