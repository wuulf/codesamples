/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mgm.core.data.dao.campaign;

import com.mgm.core.data.dao.BaseDAO;
import com.mgm.core.data.dao.mapper.CampaignMapper;
import com.mgm.core.data.model.CountryModel;
import com.mgm.core.data.model.SiteModel;
import com.mgm.core.data.model.campaign.CampaignCountryModel;
import com.mgm.core.data.model.campaign.CampaignModel;
import com.mgm.core.data.model.campaign.CampaignSearchModel;
import com.mgm.core.data.model.campaign.CampaignSiteModel;
import com.mgm.core.data.model.campaign.CampaignStateModel;
import com.mgm.core.data.model.campaign.CampaignTagModel;
import com.mgm.core.data.model.campaign.SiteSellPriceModel;
import com.mgm.core.data.model.campaign.TagOwnedModel;
import java.util.List;
import org.apache.ibatis.session.SqlSession;

/**
 *
 * @author alexrodin
 */
public class CampaignDAO extends BaseDAO implements CampaignMapper {
    
    public CampaignDAO() {
        super(CampaignMapper.class);
    }
    
     @Override
    public CampaignModel getById(Integer id) {
        return (CampaignModel)super.getById(id);
    }

    @Override
    public List<CampaignModel> getAll() {
        return (List<CampaignModel>) super.getAll();
    }
    
    
    @Override
    public List<CampaignStateModel> getCampaignStates() {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            CampaignMapper mapper = (CampaignMapper) session.getMapper(mapperClass);
            return mapper.getCampaignStates();
        } finally {
            session.close();
        }
    }

    @Override
    public List<SiteModel> getCampaignSites(CampaignModel model) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            CampaignMapper mapper = (CampaignMapper) session.getMapper(mapperClass);
            return mapper.getCampaignSites(model);
        } finally {
            session.close();
        }
    }

    @Override
    public List<TagOwnedModel> getCampaignTags(CampaignModel model) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            CampaignMapper mapper = (CampaignMapper) session.getMapper(mapperClass);
            return mapper.getCampaignTags(model);
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<CountryModel> getCampaignCountries(CampaignModel model) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            CampaignMapper mapper = (CampaignMapper) session.getMapper(mapperClass);
            return mapper.getCampaignCountries(model);
        } finally {
            session.close();
        }
    }

    @Override
    public List<CampaignSearchModel> getSearchList() {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            CampaignMapper mapper = (CampaignMapper) session.getMapper(mapperClass);
            return mapper.getSearchList();
        } finally {
            session.close();
        }
    }
    
    @Override
    public List<SiteSellPriceModel> getSellPrices() {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            CampaignMapper mapper = (CampaignMapper) session.getMapper(mapperClass);
            return mapper.getSellPrices();
        } finally {
            session.close();
        }
    }

    @Override
    public void setCampaignCountries(List<CampaignCountryModel> countries) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            CampaignMapper mapper = (CampaignMapper) session.getMapper(mapperClass);
            mapper.setCampaignCountries(countries);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void setCampaignTags(List<CampaignTagModel> tags) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            CampaignMapper mapper = (CampaignMapper) session.getMapper(mapperClass);
            mapper.setCampaignTags(tags);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void setCampaignSites(List<CampaignSiteModel> sites) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            CampaignMapper mapper = (CampaignMapper) session.getMapper(mapperClass);
            mapper.setCampaignSites(sites);
            session.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public void removeMapping(CampaignModel model) {
        SqlSession session = sqlSessionFactory.openSession();
        try {
            CampaignMapper mapper = (CampaignMapper) session.getMapper(mapperClass);
            mapper.removeMapping(model);
            session.commit();
        } finally {
            session.close();
        }
    }
    
    
}
