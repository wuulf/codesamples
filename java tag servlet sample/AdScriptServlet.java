package com.improvead.service.web.servlet;

import com.improvead.model.Format;
import com.improvead.model.Site;
import com.improvead.service.FormatService;
import com.improvead.service.SiteService;
import com.improvead.service.TagService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import ro.isdc.wro.config.Context;
import ro.isdc.wro.config.jmx.WroConfiguration;
import ro.isdc.wro.extensions.processor.js.UglifyJsProcessor;
import ro.isdc.wro.model.group.processor.Injector;
import ro.isdc.wro.model.group.processor.InjectorBuilder;
import ro.isdc.wro.model.resource.Resource;
import ro.isdc.wro.model.resource.ResourceType;

/**
 *
 * Provides impressions for publishers
 * @author alexrodin
 */
@WebServlet(name = "AdScriptServlet", urlPatterns = {"/adscript/*"})
public class AdScriptServlet extends HttpServlet {
    
    private static final String ADSCRIPT_PATH = "/var/www/html/adscript/";

    //http://www.minifyjs.com/javascript-obfuscator/
    private static final String JS_FILE = "core.js";
    private static final String JS_FILE_ASYNC = "core_async.js";
    private static final int TAG_MAP_RELOAD_DELAY_MINUTES = 5;
    // private static final String IFRAME_FILE = "iaiframe.html";
    //  private Map<Integer, String> iaIframeMap = new HashMap<>();
    private Map<String, String> iaScriptMap = new HashMap<>();
    private Map<Integer, Site> siteMap = new HashMap();
    private String formatJSONData;
    private Configuration cfg;
    private long lastReload = System.currentTimeMillis();

    public AdScriptServlet() {
        
    }
    
    private void writeFile(String path, String content) {
        try (PrintStream out = new PrintStream(new FileOutputStream(path))) {
            out.print(content);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
   
    //TODO - unnecessary - could be cached
    private void loadFormatJSON() {
        //TODO - unnecessary - could be cached
        List<Format> formats = FormatService.getAll();
        StringBuilder formatJSON = new StringBuilder();
        formatJSON.append("{");
        for (Format format : formats) {
            if (format.getNaturalId().equals("responsive")) {
                continue;
            }
            formatJSON.append("'").append(format.getNaturalId())
                    .append("':{x:").append(format.getWidth())
                    .append(",y:").append(format.getHeight())
                    .append(",dpidmap:").append(StringUtils.isEmpty(format.getDemandPartnerIdMap())?"{}":format.getDemandPartnerIdMap())
                    .append("},");
        }
        formatJSON.deleteCharAt(formatJSON.length() - 1);
        formatJSON.append("}");
        formatJSONData = formatJSON.toString();
    }

    @Override
    public void init() throws ServletException {
        super.init(); //To change body of generated methods, choose Tools | Templates.
        cfg = new Configuration();
        cfg.setServletContextForTemplateLoading(getServletContext(), "/");
        
        reloadSiteTagMap();
    }
                      
    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("application/javascript;charset=UTF-8");
        
        if ((lastReload + (1000 * 60 * TAG_MAP_RELOAD_DELAY_MINUTES)) < System.currentTimeMillis()) {
            reloadChangedSiteTagMap();
        }
        
        try {  
            String[] pathElements = StringUtils.split(request.getRequestURI(), '/');
            if (pathElements.length < 1) {
                return;
            }
            String siteId = pathElements[pathElements.length - 1].substring(0, pathElements[pathElements.length - 1].indexOf("."));
            if (iaScriptMap.containsKey(siteId)) {
                response.getWriter().print(iaScriptMap.get(siteId));
            } else {
                response.getWriter().print("Invalid IA script requoest");
                System.out.println("LOG that SiteID did not exist: " + siteId + " uri was: " + request.getRequestURI());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        /*String userAgent = request.getHeader("user-agent");
        
        RequestLogRecord.Builder builder = new RequestLogRecord.Builder();
        builder.setIp(request.getRemoteAddr());
        builder.setLocale(request.getLocale().getCountry());
        builder.setSessionId(request.getSession().getId());
        builder.setUserAgent(userAgent);
        if (userAgent != null) {
            builder.setIsMobile(RequestAnalyser.isMobile(userAgent));
            builder.setPlatformName(RequestAnalyser.getPlatformName(userAgent));
        }
        builder.setSrvIp(request.getLocalAddr());
        builder.setDateCreated(new Date());
        RequestLogRecord record = builder.build();
        RequestLogService.insert(record);*/
    }
        
    private String generateSiteCodeFromTemplate(Site site) throws Exception, TemplateException, NumberFormatException {
        Map<String, Object> data = new HashMap<>();
        data.put("siteConfig", TagService.getSiteJSONConfig(site));
        data.put("demandPartnerConfig", TagService.getDemandPartnerTagTemplateMapJSON());
        data.put("formatJSONData", formatJSONData);
        data.put("siteId", site.getId());
        Template template; 
        if (site.getTagLoadingMode() == Site.TagLoadingMode.ASYNC) {
            template = cfg.getTemplate(JS_FILE_ASYNC);
        } else {
            template = cfg.getTemplate(JS_FILE);
        }
        Writer out = new StringWriter();
        template.process(data, out);  
        return minifyScript(out.toString());
    }

    private void reloadSiteTagMap() {
        System.out.println("*** Reloading all sites map...");
        lastReload = System.currentTimeMillis();
        loadFormatJSON();
        List<Site> list = SiteService.getAll();
        
        Map<String, String> tmp_iaScriptMap = new HashMap<>();
        
        for (Site s : list) {
            if (s.getGenerator().equals("default")) {
                try {
                    //Load All sites to hashmap on start
                    siteMap.put(s.getId(), s);
                    String script = generateSiteCodeFromTemplate(s);
                    tmp_iaScriptMap.put(s.getId().toString(), script);                

                    writeFile(ADSCRIPT_PATH+"/"+s.getId().toString()+".js", script);                
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        
        synchronized (iaScriptMap) {
            this.iaScriptMap = tmp_iaScriptMap;
        }
    }
    
    private void reloadChangedSiteTagMap() {
        
        lastReload = System.currentTimeMillis();
        
        List<Site> list = SiteService.getAll();
        
        Map<String, String> tmp_iaScriptMap;
        
        tmp_iaScriptMap = new HashMap<>();
        
        System.out.println("**** Reloading changed sites map ****");
        
        Site cachedSite;
        
        for (Site s : list) {
            if (s.getGenerator().equals("default")) {
                //get cached Site data
                cachedSite = siteMap.get(s.getId());

                if (cachedSite == null || !s.getUpdatedAt().equals(cachedSite.getUpdatedAt())) {
                    try {
                        if (cachedSite != null) {
                            cachedSite.setUpdatedAt(s.getUpdatedAt());
                        } else {
                            System.out.println("New site added ..." + s.getId());
                            siteMap.put(s.getId(), s);
                        }

                        String script = generateSiteCodeFromTemplate(s);
                        tmp_iaScriptMap.put(s.getId().toString(), script);

                        writeFile(ADSCRIPT_PATH+"/"+s.getId().toString()+".js", script);                

                        //s.setTagMapReloadAt(new Date());
                        //SiteService.update(s);

                    } catch (Exception ex) {
                        //ex.printStackTrace();
                    }
                }
            }
        }
        
        synchronized (iaScriptMap) {
            for (Map.Entry<String, String> entry : tmp_iaScriptMap.entrySet()) {
                this.iaScriptMap.put(entry.getKey(), entry.getValue());
                System.out.println("Reloaded map for " + entry.getKey());
            }
        }
    }

    private String minifyScript(String script) {
        // if(true) return script;
        WroConfiguration config = new WroConfiguration();
        Context.set(Context.standaloneContext(), config);
        try {
            //Create injector which will inject all dependencies of the processor
            Injector injector = new InjectorBuilder().build();
            UglifyJsProcessor p = new UglifyJsProcessor();
            injector.inject(p);
            Resource resource = Resource.create("tmp.js", ResourceType.JS);
            StringReader reader = new StringReader(script);
            StringWriter writer = new StringWriter();
            p.process(resource, reader, writer);
            return writer.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
            return script;
        } finally {
            Context.unset();
        }
    }

}
