/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mgm.tagscriptgenerator.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author alexrodin
 */
public class DPMapRecordModel {
    private int id;
    private String key;
    private boolean last = false;
    
    private DPMapRecordModel() {};

    public DPMapRecordModel(int id, String key, boolean last) {
        this.id = id;
        this.key = key;
        this.last = last;
    }        
    
    public static DPMapRecordModel modelFromDBRecord(ResultSet record) {
        DPMapRecordModel model = new DPMapRecordModel();
        try {
            model.id = record.getInt("id");
            model.key = record.getString("key");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return model;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }
}
