package com.mgm.tagscriptgenerator;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.mgm.core.data.activemodel.v2.ClusterTier;
import com.mgm.core.data.activemodel.v2.TemplateVarValue;
import com.mgm.tagscriptgenerator.db.DBAPI;
import com.mgm.tagscriptgenerator.model.DPConfigModel;
import com.mgm.tagscriptgenerator.model.DPMapRecordModel;
import com.mgm.tagscriptgenerator.model.FormatModel;
import com.mgm.tagscriptgenerator.model.SiteConfigModel;
import com.mgm.tagscriptgenerator.model.SiteModel;
import com.mgm.tagscriptgenerator.model.TemplateModel;
import com.mgm.tagscriptgenerator.model.TierConfigModel;
import com.mgm.tagscriptgenerator.model.VarModel;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.apache.commons.io.FileUtils;
import ro.isdc.wro.config.Context;
import ro.isdc.wro.config.jmx.WroConfiguration;
import ro.isdc.wro.extensions.processor.js.UglifyJsProcessor;
import ro.isdc.wro.model.group.processor.Injector;
import ro.isdc.wro.model.group.processor.InjectorBuilder;
import ro.isdc.wro.model.resource.Resource;
import ro.isdc.wro.model.resource.ResourceType;

/**
 *
 * @author alexrodin
 */
public class ScriptWriter {
    
    public static String FILE_PATH = "/var/www/html/tags";
    
    public static void createScript(SiteModel siteModel) {
        ScriptWriter.createScript(siteModel, false, true);
    }

    public static String createScript(SiteModel siteModel, boolean minify, boolean toFile) {
        SiteConfigModel configModel = generateSiteConfigModel(siteModel.getId());
         //TODO - Not that cool this is, not that cool
        configModel.setOldId(siteModel.getOldId());
        configModel.setDomain(siteModel.getDomain());
        configModel.setAsyncmode(siteModel.getAsyncmode());
        configModel.setSendWfStat(false);
        configModel.setWfStatURL("#");
        configModel.setOriginAgnostic(false);
        
        try {
            
            return writeScript(String.valueOf(siteModel.getOldId()) + ".js",
                    configModel,
                    siteModel.getMustacheTemplate(), 
                    minify, 
                    toFile);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static String createDynamicScript(SiteModel siteModel, boolean minify, boolean toFile) {
        
        String ret = "";
        
        List<ClusterTier> clusterTierList = ClusterTier.find("site_id=?", siteModel.getId());
        
        for (ClusterTier clusterTier : clusterTierList) {

            Integer tierId = clusterTier.getInteger("tier_id");
            
            List<VarModel> vars = new ArrayList<>();
            TemplateVarValue.find("site_id=? AND tier_id=?", siteModel.getId(), tierId).forEach(v->{
                vars.add(new VarModel(v.getInteger("id"),
                        siteModel.getId(),
                        v.getInteger("template_id"),
                        v.getString("var"),
                        v.getString("value"),
                        v.getString("format")));
            });
            
            String tConfig = createTierConfig(clusterTier, siteModel, vars);

            String content = (minify) ? minifyScript(tConfig) : tConfig;
            
            if (toFile) {                    
                String fileName = String.format("%d_tier%d.js", siteModel.getOldId(), tierId);
                System.out.println("Saving to file: "+fileName);
                
                try {
                    FileUtils.writeStringToFile(new File(FILE_PATH, fileName), content);
                } catch (IOException ex) {
                    Logger.getLogger(ScriptWriter.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            } else {
                ret += content;
            }
        }
        
        return ret;
    }
    
    public static String createTierConfig(ClusterTier clusterTier, SiteModel siteModel, List<VarModel> vars) {
        String ret = "";

        Integer demand_partner_id = clusterTier.getInteger("demand_partner_id");
        Integer tier_id = clusterTier.getInteger("tier_id");
        Integer cluster_id = clusterTier.getInteger("cluster_id");

        Map<String, List<VarModel>> byFormat = vars.stream().collect(Collectors.groupingBy(VarModel::getFormat));

        TierConfigModel tierConfigModel = new TierConfigModel();
        List<FormatModel> formatModelList = new ArrayList<>();
        for (Map.Entry<String, List<VarModel>> entry : byFormat.entrySet()) {

            // Mustache hack
            List<VarModel> value = entry.getValue();
            int len = value.size();
            if (len > 0) {
                value.get(len - 1).setLast(true);
            }

            formatModelList.add(new FormatModel(entry.getKey(), value));
        }

        // Mustache hack
        if (formatModelList.size() > 0) {
            formatModelList.get(formatModelList.size()-1).setLast(true);
        }

        tierConfigModel.setFormats(formatModelList);
        tierConfigModel.setDpId(String.format("%d", demand_partner_id));

        try (StringWriter writer = new StringWriter()) {

            MustacheFactory mf = new DefaultMustacheFactory();
            Mustache mustache = mf.compile("tier_config.moustache");
            mustache.execute(writer, tierConfigModel).flush();

            return writer.toString();

        } catch ( IOException e ) {
            e.printStackTrace();
        }
            
        return "";
    }
    
    public static SiteConfigModel generateSiteConfigModel(int siteId) {
        Connection c = null;
        SiteConfigModel siteModel = new SiteConfigModel();
        try {
            List<DPMapRecordModel> dps = DBAPI.getSiteDps(siteId);
            siteModel.setId(siteId);
            siteModel.setDPMap(dps);
            for (DPMapRecordModel dp : dps) {
                TemplateModel tpl = DBAPI.getTemplate(dp.getId());
                //System.out.println(tpl.getTagTemplate());
                List<VarModel> variables = DBAPI.getVariables(siteId, tpl.getId());
                DPConfigModel dpModel = DPConfigModel.buildModel(dp.getId(), tpl, variables);
                siteModel.addDps(dpModel);
            }
            siteModel.markLast();
            siteModel.setFirst();
        } catch (Exception e) {
           e.printStackTrace();
           System.err.println(e.getClass().getName()+ " : " + e.getMessage());
           System.exit(0);
        }
        return siteModel;
    }
    
    public static String writeScript(String fileName, SiteConfigModel model, String template,
            boolean minify, boolean toFile) throws IOException {
        
        try (StringWriter writer = new StringWriter()) {
            
            MustacheFactory mf = new DefaultMustacheFactory();
            Mustache mustache = mf.compile(template);
            mustache.execute(writer, model).flush();
            
            String content = (minify) ? minifyScript(writer.toString()) : writer.toString();
            
            if (toFile) {
                FileUtils.writeStringToFile(new File(FILE_PATH, fileName), content);
            } else {
                return content;
            }
            
        } catch ( IOException e ) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    private static String minifyScript(String script) {
        
        WroConfiguration config = new WroConfiguration();
        Context.set(Context.standaloneContext(), config);
        try {
            //Create injector which will inject all dependencies of the processor
            Injector injector = new InjectorBuilder().build();
            UglifyJsProcessor p = new UglifyJsProcessor();
            injector.inject(p);
            Resource resource = Resource.create("tmp.js", ResourceType.JS);
            StringReader reader = new StringReader(script);
            StringWriter writer = new StringWriter();
            p.process(resource, reader, writer);
            return writer.toString();
        } catch (IOException ex) {
            ex.printStackTrace();
            return script;
        } finally {
            Context.unset();
        }
    }
}
