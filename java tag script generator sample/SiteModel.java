/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mgm.tagscriptgenerator.model;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author alexrodin
 */
public class SiteModel {
    private int id;
    private int oldId;
    private String domain;
    private String generator;
    private boolean asyncmode;

    public static final String GENERATOR_DEFAULT = "default";
    public static final String GENERATOR_NOTDEFAULT = "not-default";
    public static final String GENERATOR_DYNAMIC = "dynamic";

    public SiteModel () {}
    
    public static SiteModel modelFromDBRecord(ResultSet record) {
        SiteModel model = new SiteModel();
        try {
            model.id = record.getInt("id");
            model.oldId = record.getInt("v1_id");
            model.domain = record.getString("domain_name");
            model.generator = record.getString("generator");
            model.asyncmode = record.getBoolean("asyncmode");
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return model;
    }
    
    public String getMustacheTemplate() {
        switch (this.generator) {
            case GENERATOR_DYNAMIC:
                return "site_config_dynamic.moustache";
            default:
                return "site_config.moustache";
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOldId() {
        return oldId;
    }

    public void setOldId(int oldId) {
        this.oldId = oldId;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getGenerator() {
        return generator;
    }

    public void setGenerator(String generator) {
        this.generator = generator;
    }
    
    public void setAsyncmode(boolean asyncmode) {
        this.asyncmode = asyncmode;
    }

    public boolean getAsyncmode() {
        return asyncmode;
    }
    
}
