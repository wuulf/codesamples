/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mgm.tagscriptgenerator.service;

import com.mgm.core.env.Config;
import com.mgm.core.env.Environment;
import com.mgm.core.env.Service;
import com.mgm.tagscriptgenerator.ScriptWriter;
import com.mgm.tagscriptgenerator.db.DBAPI;
import com.mgm.tagscriptgenerator.model.SiteModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.javalite.activejdbc.Base;

/**
 *
 * @author alexrodin
 */
public class GeneratorService {
    
    public static void main(String[] args) throws IOException {
        //TODO move options outta here!
        Options options = new Options();
        Option siteIdOption = Option.builder("s")
            .required(false)
            .hasArgs()
            .desc("site [id_1<space>id_2...] to generate script for.")
            .build()
        ;
        options.addOption(siteIdOption);
        
        Option folderOption = Option.builder("f")
            .required(true)
            .hasArg()
            .desc("folder to save scripts to")
            .build()
        ;
        options.addOption(folderOption);
        
        Option minifyOption = Option.builder("m")
                .required(false)
                .desc("Generate minified javascript")
                .build();
        options.addOption(minifyOption);
        
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp( "scrgen", options );
            System.exit(1);
        }
        
        ScriptWriter.FILE_PATH = cmd.getOptionValue("f");
        boolean minify = cmd.hasOption("m");
       
        try {
            DBAPI.init();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println("Unable to init DBPAPI");
            System.exit(1);
        }

        
        // *************************************************************
        // Generate sites main JS file
        // *************************************************************
        List<SiteModel> sites = new ArrayList();
        if (cmd.hasOption("s")) {
            String[] siteIds = cmd.getOptionValues("s");
            for (String id : siteIds) {
                sites.addAll(DBAPI.getSiteByOldID(Integer.valueOf(id)));
            }
        } else {
            sites = DBAPI.getSites();
        }

        sites.stream().forEach((site) -> {
            System.out.println("Site: "+site.getId()+" "+site.getDomain()+" ---> "+site.getOldId()+".js");
            ScriptWriter.createScript(site, minify, true);
        });
        
        // *************************************************************
        // Generate tiers config
        // This will generate ONLY variables override files siteid_tierid.js
        // *************************************************************
                        
        Config.INSTANCE.setEnv(Environment.stringToEnv(System.getProperty("env")));
        Base.open(Service.INSTANCE.getPgpool());
        
        sites = new ArrayList();
        if (cmd.hasOption("s")) {
            String[] siteIds = cmd.getOptionValues("s");
            for (String id : siteIds) {
                sites.addAll(DBAPI.getSiteByOldID(Integer.valueOf(id)));
            }
        } else {
            sites = DBAPI.getSitesDynamic();
        }
        
        System.out.println("--------------------");
        
        sites.stream().forEach((site) -> {            
            System.out.println("Dynamic Site: "+site.getId()+" "+site.getDomain());
            ScriptWriter.createDynamicScript(site, minify, true);
        });
        
        Base.close();
    }
}
