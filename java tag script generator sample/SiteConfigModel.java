
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mgm.tagscriptgenerator.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alexrodin
 */

public class SiteConfigModel {
    
    private int oldId;
    private int id;
    private List<DPConfigModel> dps;
    private List<DPMapRecordModel> dpMap;
    private int first = -1;
    private String domain;
    private boolean asyncmode;
    private boolean sendWfStat = false;
    private boolean originAgnostic = false;
    private String wfStatURL = "#"; 

    public boolean isAsyncmode() {
        return asyncmode;
    }

    public void setAsyncmode(boolean asyncmode) {
        this.asyncmode = asyncmode;
    }
    
    public SiteConfigModel() {
        dps = new ArrayList();
    };
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<DPConfigModel> getDps() {
        return dps;
    }

    public void setDps(List<DPConfigModel> dps) {
        this.dps = dps;
    }
    
    public void addDps(DPConfigModel dp) {
        int len = dps.size(); 
        if (len > 0) {
            dps.get(len - 1).setNext(dp.getDpId());
        }
        dps.add(dp);
    }

    public List<DPMapRecordModel> getDpMap() {
        return dpMap;
    }

    public int getFirst() {
        return first;
    }

    public void setDPMap(List<DPMapRecordModel> dps) {
        dpMap = dps;
        int len = dpMap.size();
        if (len > 0) {
            dpMap.get(len-1).setLast(true);
        }
    }
    
    public void setFirst() {
        int len = dps.size();
        if (len > 0) {
            first = dps.get(0).getDpId();
        }
    }
    
    //TODO - MOUSTACHE HACK TO SET LAST ELEMENT MARK
    public void markLast() {
        int len = dps.size(); 
        if (len > 0) {
            dps.get(len - 1).setLast(true);
        }
    }

    public int getOldId() {
        return oldId;
    }

    public void setOldId(int oldId) {
        this.oldId = oldId;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public boolean isSendWfStat() {
        return sendWfStat;
    }

    public void setSendWfStat(boolean sendWfStat) {
        this.sendWfStat = sendWfStat;
    }

    public String getWfStatURL() {
        return wfStatURL;
    }

    public void setWfStatURL(String wfStatURL) {
        this.wfStatURL = wfStatURL;
    }

    public boolean isOriginAgnostic() {
        return originAgnostic;
    }

    public void setOriginAgnostic(boolean originAgnostic) {
        this.originAgnostic = originAgnostic;
    }
    
    
}
