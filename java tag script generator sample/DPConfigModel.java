package com.mgm.tagscriptgenerator.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author alexrodin
 */
public class DPConfigModel {
    private int dpId;
    private TemplateModel tpl;
    private List<FormatModel> formats;
    private int next = -1;
    private boolean last = false;
    
    private DPConfigModel() {
        formats = new ArrayList();
    };
    
    public static DPConfigModel buildModel(int dpId, TemplateModel tpl, List<VarModel> vars) {
        DPConfigModel model = new DPConfigModel();
        model.tpl = tpl;
        model.dpId = dpId;
        Map<String, List<VarModel>> byFormat = vars.stream().collect(Collectors.groupingBy(VarModel::getFormat));
        for (Map.Entry<String, List<VarModel>> entry : byFormat.entrySet()) {
            String formatName = entry.getKey();
            List<VarModel> varList = entry.getValue();
            
            // Do not generate template for empty formats
            if (varList.stream().anyMatch(t->t.getVarValue().equals(""))) {
                continue;
            }
            
            //TODO - MOUSTACHE HACK TO SET LAST ELEMENT MARK
            int len = varList.size();
            if (len > 0) {
                varList.get(len - 1).setLast(true);
            }
            //END OF MOUSTACHE HACK
            model.formats.add(new FormatModel(formatName, varList));
        }
        
        //TODO - MOUSTACHE HACK TO SET LAST ELEMENT MARK
        int len = model.formats.size();
        if (len > 0) {
            model.formats.get(len - 1).last = true;
        }
        //END OF MOUSTACHE HACK
        return model;
    }

    public int getDpId() {
        return dpId;
    }

    public void setDpId(int dpId) {
        this.dpId = dpId;
    }

    public TemplateModel getTpl() {
        return tpl;
    }

    public void setTpl(TemplateModel tpl) {
        this.tpl = tpl;
    }

    public List<FormatModel> getFormats() {
        return formats;
    }

    public void setFormats(List<FormatModel> formats) {
        this.formats = formats;
    }

    public int getNext() {
        return next;
    }

    public void setNext(int next) {
        this.next = next;
    }

    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    @Override
    public String toString() {
        return "DPConfigModel{" + "dpId=" + dpId + ", tpl=" + tpl + ", formats=" + formats + ", next=" + next + ", last=" + last + '}';
    }
}
