package com.rodinalex.andgames.r13.fsm;

import com.rodinalex.andgames.fw.fsm.Puppet;
import com.rodinalex.andgames.fw.fsm.PuppetState;
import com.rodinalex.andgames.fw.math.Vector2f;
import com.rodinalex.andgames.r13.Fighter_Old;

public class PuppetFighter  extends Puppet<Fighter_Old> {

	public Vector2f position;
	public int direction;
	public Fighter_Old opponent;
	
	public PuppetFighter(Fighter_Old fighter) {
		super(fighter);
		position = new Vector2f();
	}

	public PuppetFighter (
		Fighter_Old fighter,
		PuppetState<Fighter_Old> currentState,
		PuppetState<Fighter_Old> previousState,
		PuppetState<Fighter_Old> globalState
	) {
		super(fighter, currentState, previousState, globalState);
		position = new Vector2f();
	}
	
	public void setPosition(float x, float y) { 
		position.set(x, y);
	}
	
	public void setDirection(int direction) {
		this.direction = direction;
	}
	
	public void setOpponent(Fighter_Old opponent) {
		this.opponent = opponent;
	}
	
}