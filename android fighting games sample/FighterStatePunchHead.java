package com.rodinalex.andgames.r13.fsm;

import org.json.JSONObject;

import com.rodinalex.andgames.fw.fsm.Puppet;
import com.rodinalex.andgames.fw.messaging.Telegram;
import com.rodinalex.andgames.r13.Fighter_Old;
import com.rodinalex.andgames.r13.screen.ScreenCombat;

public class FighterStatePunchHead extends FighterState_Old {

	private float messageTime = 0.0f;
	
	public FighterStatePunchHead(JSONObject config, PuppeteerFighter puppeteer) {
		super(config, puppeteer);
		messageTime = duration / 2.0f;
	}

	@Override
	public void enter(Puppet<Fighter_Old> puppet) {
		super.enter(puppet);
	}
	
	@Override
	public void update(Puppet<Fighter_Old> puppet, float deltaTime) {
		puppet.timeInState += deltaTime;

		if (Math.abs(puppet.timeInState - messageTime) < 0.1f) {
			Telegram msg = ScreenCombat.msg;
			msg.receiver = ((PuppetFighter)puppet).opponent.id;
			msg.sender = puppet.entity.id;
			msg.msgType = Fighter_Old.MSG_HIT_HEAD;
			ScreenCombat.messageDispatcher.registerMessage(msg);
		}
		
		if (puppet.timeInState >= duration) {
			puppeteer.changeState(puppet, puppeteer.idle);
		}
	}
	
	@Override
	public boolean handleMessage(Puppet<Fighter_Old> puppet, Telegram msg) {
		
		if (msg.msgType == Fighter_Old.MSG_HIT_HEAD) {
			puppeteer.changeState(puppet, puppeteer.hitHead);
			return true;
		} 
		
		return false;
	}
	
}
