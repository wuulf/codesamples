package com.rodinalex.andgames.warwarium.screen;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.rodinalex.andgames.fw.AndInput.TouchEvent;
import com.rodinalex.andgames.fw.GLGame;
import com.rodinalex.andgames.fw.collisions.AABB2D;
import com.rodinalex.andgames.fw.collisions.CollisionDetector2D;
import com.rodinalex.andgames.fw.gl.Camera2D;
import com.rodinalex.andgames.fw.gl.GLScreen;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.gl.Texture;
import com.rodinalex.andgames.fw.gl.TextureRegion;
import com.rodinalex.andgames.fw.math.Vector2f;

public class Screen_MainMenu extends GLScreen {

	Camera2D guiCam;
    SpriteBatcher batcher;
    Vector2f touchPoint;
    
    Texture screenTexture;
    TextureRegion bgRegion;
    TextureRegion playButtonRegion_normal;
    TextureRegion playButtonRegion_pressed;
    TextureRegion demoButtonRegion_normal;
    TextureRegion demoButtonRegion_pressed;
    
    AABB2D playButtonBox;
    AABB2D demoButtonBox;
    
    boolean playButtonPressed = false;
    boolean demoButtonPressed = false;
	
	public Screen_MainMenu(GLGame game) {
		super(game);
		guiCam = new Camera2D(graphics, 480, 320);
        batcher = new SpriteBatcher(graphics, 10);
        playButtonBox = new AABB2D(176 - 48, 200 - 16, 96, 32);
        demoButtonBox = new AABB2D(176 - 48, 160 - 16, 96, 32);
        touchPoint = new Vector2f();
	}
	
	@Override
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
        int len = touchEvents.size();
        for (int i = 0; i < len; i++) {
        	TouchEvent event = touchEvents.get(i); 
        	if(event.type == TouchEvent.kTouchDown) {
                touchPoint.set(event.x, event.y);
                guiCam.touchToWorld(touchPoint);
                
                if(CollisionDetector2D.intersectAABBPoint(playButtonBox, touchPoint)) {
                    playButtonPressed = true;
                	return;
                }
                
                if(CollisionDetector2D.intersectAABBPoint(demoButtonBox, touchPoint)) {
                    demoButtonPressed = true;
                	return;
                }
            }
            
            if(event.type == TouchEvent.kTouchUp) {
                
            	touchPoint.set(event.x, event.y);
                guiCam.touchToWorld(touchPoint);
                
                if (playButtonPressed) {
                	playButtonPressed = false;
                	game.setScreen(new Screen_Combat(game, 0));
                	//game.setScreen(new Screen_WorldMap(game));
                	return;
                }
                
                if (demoButtonPressed) {
                	demoButtonPressed = false;
                	game.setScreen(new Screen_Combat(game, 1));
                	return;
                }
                
            }
        }
	}
	
	@Override
	public void render() {
		GL10 gl = graphics.getGL();        
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        guiCam.setViewportAndMatrices();
        
        gl.glEnable(GL10.GL_TEXTURE_2D);
        
        gl.glEnable(GL10.GL_BLEND);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);      
        
        batcher.beginBatch(screenTexture);
        batcher.drawSprite(240, 160, 480, 320, bgRegion);
        
        if (!playButtonPressed) {
        	batcher.drawSprite(176, 200, 96, 32, playButtonRegion_normal);
		} else {
			batcher.drawSprite(176, 200, 96, 32, playButtonRegion_pressed);
		}
        
        if (!demoButtonPressed) {
        	batcher.drawSprite(176, 160, 96, 32, demoButtonRegion_normal);
		} else {
			batcher.drawSprite(176, 160, 96, 32, demoButtonRegion_pressed);
		}
        
        batcher.endBatch();
        
        gl.glDisable(GL10.GL_BLEND);
	}

	@Override
	public void pause() {
		screenTexture.release();
	}

	@Override
	public void resume() {
		screenTexture = Texture.textureForGameWithFileName(game, "screen/main_menu_screen_00.png");
        bgRegion = new TextureRegion(screenTexture, 0, 0, 960, 640);
        playButtonRegion_normal = new TextureRegion(screenTexture, 0, 640, 192, 64);
        playButtonRegion_pressed = new TextureRegion(screenTexture, 192, 640, 192, 64);
        demoButtonRegion_normal = new TextureRegion(screenTexture, 0, 704, 192, 64);
        demoButtonRegion_pressed = new TextureRegion(screenTexture, 192, 704, 192, 64);
    }
    
	@Override
	public void release() {
		screenTexture.release();
	}

}
