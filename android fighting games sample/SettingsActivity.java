package com.rodinalex.andgames.warwarium;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.rodinalex.andgames.fw.AndFileIO;

public class SettingsActivity extends Activity {
	
	AndFileIO fileIO;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		fileIO = new AndFileIO(getAssets());
		loadSettings();
	}

	public void saveChangesButtonDidPress(View v) {
		//TODO - save settings
	}
	
	public void returnToLauncherButtonDidPress(View v) {
		Intent intent = new Intent(this, LauncherActivity.class);
		startActivity(intent);
	}
	
	public void loadSettings() {
    	StringBuilder jsonString = new StringBuilder();
		BufferedReader br = null;
		try {
			br = new BufferedReader(
				new InputStreamReader(
					fileIO.readAssetWithName("config/swordsman.cfg")
				)
			);
			String line;
		    while ((line = br.readLine()) != null) {
		    	jsonString.append(line);
		    	jsonString.append('\n');
		    }
		} catch (IOException e) {
			throw new RuntimeException("Can't load config settings");
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		EditText textView = (EditText)findViewById(R.id.settingsView);
		textView.setText(jsonString);
    }
}
