package com.rodinalex.andgames.warwarium;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rodinalex.andgames.fw.GLGame;
import com.rodinalex.andgames.warwarium.combat.CombatMove;

public class Swordsman extends Fighter {
    
	public String configFile;
	
	public JSONObject config;
	
	//TODO move array creation to init
	public ArrayList<CombatMove> attackMoves; 
	public ArrayList<CombatMove> defenseMoves; 
	public ArrayList<CombatMove> hitMoves;
	
	public Swordsman(float x, float y, int side, String configFile, GLGame game) {
		super(x, y, side);
		this.game = game;
		this.configFile = configFile;
		this.attackMoves = new ArrayList<CombatMove>();
		this.defenseMoves = new ArrayList<CombatMove>();
		this.hitMoves = new ArrayList<CombatMove>();
	}
	
	@Override
	public void init() {
		config = Assets.getJsonConfig(game, configFile);
		try {
			damageMultiplier = config.getInt("damageMultiplier");
			width = (float)config.getDouble("width");
			height = (float)config.getDouble("height");
			//Init combat moves
			JSONArray srcMoves = config.getJSONArray("moves"); 
			attackMoves.clear();
			defenseMoves.clear();
			hitMoves.clear();
			int len = srcMoves.length();
			for (int i = 0; i < len; i++) {
				CombatMove move = new CombatMove(this, srcMoves.getJSONObject(i));
				if (move.type == 1) {
					attackMoves.add(move);
				} else if (move.type == 2) {
					defenseMoves.add(move);
				} else if (move.type == 3) {
					hitMoves.add(move);
				}
			}
			currentMove = defaultMove;
		} catch (Exception e) {
			throw new RuntimeException("Invalid config for warrior '" +  configFile + "'");
		}
	}
	
	@Override
	public void release() {
		for (int i = 0; i < attackMoves.size(); i++) {
			attackMoves.get(i).release();
		}
		for (int j = 0; j < defenseMoves.size(); j++) {
			defenseMoves.get(j).release();
		}
		for (int k = 0; k < hitMoves.size(); k++) {
			hitMoves.get(k).release();
		}
		attackMoves.clear();
		defenseMoves.clear();
		hitMoves.clear();
	}
}
