package com.rodinalex.andgames.warwarium.ui;

import javax.microedition.khronos.opengles.GL10;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rodinalex.andgames.fw.collisions.AABB2D;
import com.rodinalex.andgames.fw.gl.GLScreen;
import com.rodinalex.andgames.fw.gl.Texture;
import com.rodinalex.andgames.fw.gl.Vertices;
import com.rodinalex.andgames.warwarium.Fighter;

public class HealthBar extends UIItem {

	public Vertices vertices;
	
	public Fighter fighter;
	
	public boolean reverse = false;
	
	public HealthBar(GLScreen screen) {
		this.screen = screen;
	}; 
	
	public static HealthBar createHealthBar(GLScreen screen, Texture texture, Fighter fighter, JSONObject config) {
		HealthBar bar = new HealthBar(screen);
		
		try {
		
			JSONArray rs = config.getJSONArray("regionScreen");
			bar.box = new AABB2D(rs.getInt(0), rs.getInt(1), rs.getInt(2), rs.getInt(3));
			bar.reverse = config.optBoolean("reverse", false);
			
		} catch (Exception e) {
			throw new RuntimeException("HealthBar creation failed : invalid health bar config");
		}
		
		bar.fighter = fighter;
		// init vertices
		bar.vertices = new Vertices(screen.graphics, 4, 6, false, false);            
	        
        bar.vertices.setVertices(new float[] { 
        	 0.0f,  0.0f,
        	 1.0f,  0.0f,
        	 1.0f,  1.0f,
             0.0f,  1.0f 
        }, 0, 8);
        
        bar.vertices.setIndices(new short[] {0, 1, 2, 2, 3, 0}, 0, 6);
		
        return bar;
	}
	
	@Override
	public void update(float deltaTime) {
		// TODO Auto-generated method stub
	}

	@Override
	public void render() {
		
		GL10 gl = screen.graphics.getGL();
		
		float healthWidth = fighter.health * box.width;
		float healthHeight = box.height;
		
		float dx = box.min.x;
		float dy = box.min.y;
		
		if (reverse) {
			dx += box.width - healthWidth;
		}
		
		gl.glPushMatrix();
			
			gl.glTranslatef(dx, dy, 0);
			vertices.bind();
			
	        gl.glPushMatrix();
		        gl.glScalef(healthWidth, healthHeight, 1.0f);
		        gl.glColor4f(0.8f,0,0,0.8f);
		        vertices.draw(GL10.GL_TRIANGLES, 0, 6);
	        gl.glPopMatrix();
	        
	        gl.glColor4f(1, 1, 1, 1);
	        vertices.unbind();
	        
	    gl.glPopMatrix();
	}

}
