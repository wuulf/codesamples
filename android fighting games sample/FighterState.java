package com.rodinalex.andgames.r13.fsm;

import org.json.JSONException;
import org.json.JSONObject;

import com.rodinalex.andgames.fw.animation.Animation;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.messaging.Telegram;
import com.rodinalex.andgames.r13.Fighter;

public class FighterState {
	
	public String name;
	public String next;
	protected Animation animation;
	public float duration;
	protected float frameDuration;
	protected FSMFighter fsm;

	public FighterState(JSONObject config, FSMFighter fsm) {
		try {
			name = config.getString("name");
			next = config.optString("next", null);
			animation = new Animation(config.getJSONObject("animation"));
			duration = (float)config.getDouble("duration");
			frameDuration = duration/animation.getNumberOfFrames();
			this.fsm = fsm;
		} catch (JSONException e) {
			throw new RuntimeException("INVALID CONFIG FOR PUPPET STATE!");
		}
	}
	
	public void render(Fighter fighter, SpriteBatcher batcher) {
		
		int frameIndex = animation.getFrameIndex(
			fighter.timeInState, 
			frameDuration, 
			animation.looping
		);
		
		int frameNumber = animation.getFrameNumber(frameIndex);

		batcher.drawSprite(
			fighter.position.x,
			fighter.position.y,
			fighter.direction * 8,
			4,
			fsm.frames[frameNumber]
		);
	}

	public void enter(Fighter fighter) {
		fighter.timeInState = 0.0f;
	}

	public void update(Fighter fighter, float deltaTime) {
		fighter.timeInState += deltaTime;
		if (next != null && fighter.timeInState >= duration) {
			fsm.changeState(fighter, fsm.states.get(next));
		}
	}

	public void exit(Fighter puppet) {

	}

	public boolean handleMessage(Fighter puppet, Telegram msg) {
		return false;
	}
}