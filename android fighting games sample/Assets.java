package com.rodinalex.andgames.warwarium;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import com.rodinalex.andgames.fw.GLGame;
import com.rodinalex.andgames.fw.gl.Font;
import com.rodinalex.andgames.fw.gl.Texture;

public class Assets {
    
	public static Texture fontTexture;      
	public static Font font;
	public static Map<String, Texture> textures = new HashMap<String, Texture>();
	public static JSONObject defaultConfig = new JSONObject();
	
    public static void load(GLGame game) {
    	defaultConfig = getJsonConfig(game, "config/default.cfg");
    	fontTexture = Texture.textureForGameWithFileName(game, "misc/font_0.png");
    	font = new Font(fontTexture, 0, 32, 16, 16, 16);
    }       
    
    public static void reload() {
    	fontTexture.reload();
    }
    
    public static JSONObject getJsonConfig(GLGame game, String path) {
    	JSONObject config = null;
    	StringBuilder jsonString = new StringBuilder();
		BufferedReader br = null;
		try {
			br = new BufferedReader(
				new InputStreamReader(
					game.getFileIO().readAssetWithName(path)
				)
			);
			String line;
		    while ((line = br.readLine()) != null) {
		    	jsonString.append(line);
		    	jsonString.append('\n');
		    }
		} catch (IOException e) {
			throw new RuntimeException("Can't load config '" + path + "'");
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		try {
			config = new JSONObject(jsonString.toString());
		} catch (Exception e) {
			throw new RuntimeException("Can't convert '" + path + "' to JSON");
		}
		
		return config;
    }
    
    public static Texture getTexture(GLGame game, String fileName) {
    	Texture tex = textures.get(fileName);
    	if (tex == null) {
    		tex = loadTexture(game, fileName);
    	} 
    	return tex;
    }
    
    public static Texture loadTexture(GLGame game, String fileName) {
    	Texture tex = null;
    	try {
    		tex = Texture.textureForGameWithFileName(game, fileName);
    		textures.put(fileName, tex);
    	} catch (Exception e) {
    		throw new RuntimeException("Can't load texture '" + fileName + "'");
    	}
    	return tex;
    }
    
    public static void releaseTexture(String fileName) {
    	Texture tex = textures.get(fileName);
    	if (tex != null) {
    		tex.release();
    	}
    	textures.remove(fileName);
    }
}