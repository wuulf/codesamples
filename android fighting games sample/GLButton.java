package com.rodinalex.andgames.warwarium.ui;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rodinalex.andgames.fw.collisions.AABB2D;
import com.rodinalex.andgames.fw.collisions.CollisionDetector2D;
import com.rodinalex.andgames.fw.gl.GLScreen;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.gl.Texture;
import com.rodinalex.andgames.fw.gl.TextureRegion;

public class GLButton extends UIItem {
	
	public boolean buttonDown = false;
	
	public boolean buttonUp = false;
	
	public TextureRegion regionNormal;
	
	public TextureRegion regionPressed;
	
	public GLButton(GLScreen screen) {
		this.screen = screen;
	}; 
	
	public static GLButton createButton(GLScreen screen, Texture texture, JSONObject config) {
		GLButton button = new GLButton(screen);
		try {
			
			JSONArray rn = config.getJSONArray("regionNormal");
			button.regionNormal = new TextureRegion(texture, rn.getInt(0), rn.getInt(1), rn.getInt(2), rn.getInt(3));
			
			JSONArray rp = config.getJSONArray("regionPressed");
			button.regionPressed = new TextureRegion(texture, rp.getInt(0), rp.getInt(1), rp.getInt(2), rp.getInt(3));
			
			JSONArray rs = config.getJSONArray("regionScreen");
			button.box = new AABB2D(rs.getInt(0), rs.getInt(1), rs.getInt(2), rs.getInt(3));
			
			button.buttonDown = false;
			
			button.buttonUp = false;
			
		} catch (Exception e) {
			throw new RuntimeException("Invalid button config");
		}
		return button;
	};
	
	@Override
	public void update(float deltaTime) {
		buttonDown = CollisionDetector2D.intersectAABBPoint(box, screen.touchDown);
		buttonUp = CollisionDetector2D.intersectAABBPoint(box, screen.touchUp);
	}
	
	@Override
	public void render(SpriteBatcher batcher) {
		if (buttonDown) {
			batcher.drawSprite(box.center.x, box.center.y, box.width, box.height, regionPressed);
		} else {
			batcher.drawSprite(box.center.x, box.center.y, box.width, box.height, regionNormal);
		}
	}
}
