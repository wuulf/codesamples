package com.rodinalex.andgames.warwarium.combat;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.gl.Texture;
import com.rodinalex.andgames.fw.gl.TextureRegion;
import com.rodinalex.andgames.warwarium.Assets;
import com.rodinalex.andgames.warwarium.Fighter;

/*
 * move_regions :
 * 
 * 0 - top
 * 1 - middle 
 * 2 - bottom
 * 
 * move_types :
 *
 * 0 - idle
 * 1 - attack
 * 2 - defense
 * 3 - hit
 * 
 */

public class CombatMove {
	
	//Game props
	public int currentFrameIndex = 0;
	public int currentFrameNumber;
	public float timeSinceMoveStarted = 0;
	public Fighter fighter;
	public MoveState moveState;
	
	//Config props
	public String textureName;
	public Texture texture;
	public ArrayList<TextureRegion> frames;
	public int[] animation;
	
	public String name;
	public int looping;
	public int type;
	public int region;
	public float damage;
	public float staminaRequired;
	public float staminaModifierPerFrame;
	
	public float frameDuration;
	public int actionFrameIndex;
	public boolean defaultMove = false;
	
	public CombatMove(Fighter fighter, JSONObject config) {
		try {
			this.name = config.getString("name");
			this.frameDuration = (float)config.getDouble("frameDuration");
			this.fighter = fighter;
			this.looping = config.getInt("looping");
			this.region = config.getInt("region");
			this.type = config.getInt("type");
			this.staminaRequired = (float)config.optDouble("staminaRequired", 0);
			this.staminaModifierPerFrame = (float)config.optDouble("staminaModifierPerFrame", 0);
			this.damage = (float)config.optDouble("damage", 0);
			this.defaultMove = config.optBoolean("defaultMove", false);
			this.actionFrameIndex = config.optInt("actionFrameIndex", -1);
			if (this.defaultMove) {
				this.fighter.defaultMove = this;
			}
			
			//Get Texture  && Frames
			
			this.textureName = config.getString("texture");
			this.texture = Assets.getTexture(fighter.game, textureName);
			JSONArray srcFrames = config.getJSONArray("frames");
			this.frames = new ArrayList<TextureRegion>();
			for (int i = 0; i < srcFrames.length(); i++) {
				JSONArray f = srcFrames.getJSONArray(i);
				frames.add(new TextureRegion(
					texture, 
					f.getInt(0), 
					f.getInt(1), 
					f.getInt(2), 
					f.getInt(3)
				));
			}
			
			//Get animation indexes
			JSONArray srcAnimation = config.getJSONArray("animation");
			this.animation = new int[srcAnimation.length()];
			for (int i = 0; i < animation.length; i++) {
				this.animation[i] = srcAnimation.getInt(i);
			}
		
		} catch (Exception e) {
			throw new RuntimeException("Can't initialize fighter : Invalid moves config");
		}
	}
	
	public static enum MoveState {
		ms_notStarted,
		ms_started,
		ms_action,
		ms_neutral,
		ms_progress,
		ms_completed,
		ms_finished
	}
	
	public void beginMove() {
		timeSinceMoveStarted = 0;
		fighter.stamina -= staminaRequired;
		moveState = MoveState.ms_started;
		currentFrameIndex = 0;
	}
	
	public void update(float deltaTime) {

		timeSinceMoveStarted += deltaTime;
				
		int frameIndex = getFrameIndex(timeSinceMoveStarted, this.looping);

		//check if frame changed and modify stamina accordingly
		if (frameIndex != currentFrameIndex) {
			fighter.stamina += staminaModifierPerFrame;
		}
		
		if (fighter.stamina >= Fighter.kMaxStamina) {
			fighter.stamina = Fighter.kMaxStamina;
		}
		
		if (fighter.stamina <= 0) {
			fighter.stamina = 0;
		}
		
		currentFrameIndex = frameIndex;
		
		//if move nonlooping and we are past frames.length - end move
		if (frameIndex > animation.length - 1) {
			endMove();
		} else {
			//otherwise - get current frame
			currentFrameNumber = animation[frameIndex];
			//check if it is action frame
			if (actionFrameIndex == frameIndex) {
				moveState = MoveState.ms_action;
			} else {
				moveState = MoveState.ms_progress;
			}
		}
	}
	
	public void endMove() {
		moveState = MoveState.ms_finished;
	}
	
	public void render(SpriteBatcher batcher) {
		batcher.beginBatch(texture);
		batcher.drawSprite(
			fighter.position.x,
			fighter.position.y,
			fighter.side * fighter.width,
			fighter.height,
			frames.get(currentFrameNumber)
		);
		batcher.endBatch();
	}
	
	public int getFrameIndex(float stateTime, int mode) {
		
		int frameIndex = (int)(stateTime / frameDuration);
        
		if (mode == 0) {
        	return frameIndex;         
        } else {
        	return frameIndex % animation.length;
        }  
    
	}
	
	public void release() {
		Assets.releaseTexture(textureName);
		texture = null;
	}
}
