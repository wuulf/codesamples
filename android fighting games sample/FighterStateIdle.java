package com.rodinalex.andgames.r13.fsm;

import java.util.Random;

import org.json.JSONObject;

import com.rodinalex.andgames.fw.fsm.Puppet;
import com.rodinalex.andgames.fw.messaging.Telegram;
import com.rodinalex.andgames.r13.Fighter_Old;

public class FighterStateIdle extends FighterState_Old {

	private int chanceToBlock = 24;
	
	public FighterStateIdle(JSONObject config, PuppeteerFighter puppeteer) {
		super(config, puppeteer);
	}
	
	@Override
	public void update(Puppet<Fighter_Old> puppet, float deltaTime) {
		puppet.timeInState += deltaTime;
		
		PuppetFighter pup = (PuppetFighter)puppet;
		PuppetFighter pupFoe = (PuppetFighter)pup.opponent.puppet;
		
		int rand = new Random().nextInt();
		
		if (pupFoe.currentState.equals(puppeteer.punchHead) &&
			pupFoe.timeInState < puppeteer.punchHead.duration/3.0f) {
			
			if (rand % chanceToBlock == 0) {
				puppeteer.changeState(puppet, puppeteer.blockHead);
			}
			
		} else {
			if (rand % 45 == 0) {
				puppeteer.changeState(puppet, puppeteer.punchHead);
			}
		}
	}
	
	@Override
	public boolean handleMessage(Puppet<Fighter_Old> puppet, Telegram msg) {
		if (msg.msgType == Fighter_Old.MSG_HIT_HEAD) {
			puppeteer.changeState(puppet, puppeteer.hitHead);
			return true;
		} 
		return false;
	}
}