package com.rodinalex.andgames.warwarium;

import com.rodinalex.andgames.fw.GLGame;
import com.rodinalex.andgames.fw.GameObject;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.warwarium.combat.CombatMove;
import com.rodinalex.andgames.warwarium.i.CombatIntelligence;

public abstract class Fighter extends GameObject {
	
	//public static final float kFighterWidth  = 96;
    //public static final float kFighterHeight = 96;
    
    public static final float kMaxHealth = 1.0f;
    public static final float kMaxStamina = 1.0f;
    
    public GLGame game;
    
    public int side;
    public CombatMove currentMove;
    public CombatMove defaultMove;
    
    public int fighterState = 0;
    public float health;
    public float stamina;
    public float width;
    public float height;
    
    public int renderingOrder = 0;
    public int damageMultiplier = 1;
    public int score = 0;
    
    public double blockChance = 1; 
	public double attackChance = 1;

	public float blockChanceThreshold = 0.3f;
	public float attackChanceThreshold = 0.3f;
	public float kMaxChance = 0.9f;
	public float kChanceUpdateTimeout = 0.5f;
    
    public CombatIntelligence intelligence;
    
    public boolean withinStrikeDistance = true;
    public boolean alive = true;
    
	public Fighter(float x, float y, int side) {
		super(x, y);
		this.health = kMaxHealth;
		this.stamina = kMaxStamina;
		this.side = side; 
	}
	
	public abstract void init();
	public abstract void release();
	
	public void update(float deltaTime) {
		intelligence.update(deltaTime);
		if (health <= 0) {
			alive = false;
			health = -1.0f;
		}
	}
	
	public void updateChanceThresholds(float val) {
		blockChanceThreshold += val;
		attackChanceThreshold += val;
		
		if (blockChanceThreshold >= kMaxChance) {
			blockChanceThreshold = kMaxChance;
		}
		
		if (attackChanceThreshold >= kMaxChance) {
			attackChanceThreshold = kMaxChance;
		}
	}
	
	public void render(SpriteBatcher batcher) {
		currentMove.render(batcher);
	}
}
