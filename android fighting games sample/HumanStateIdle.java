package com.rodinalex.andgames.r13.fsm;

import com.rodinalex.andgames.fw.fsm.Puppet;
import com.rodinalex.andgames.fw.messaging.Telegram;
import com.rodinalex.andgames.r13.Fighter_Old;

public class HumanStateIdle extends FighterState_Old {

	public HumanStateIdle(FighterState_Old clone, PuppeteerHuman puppeteer) {
		super(clone, puppeteer);
	}
	
	@Override
	public void update(Puppet<Fighter_Old> puppet, float deltaTime) {
		puppet.timeInState += deltaTime;
	}
	
	@Override
	public boolean handleMessage(Puppet<Fighter_Old> puppet, Telegram msg) {
		
		switch (msg.msgType) {
			
			case Fighter_Old.MSG_HIT_HEAD : {
				puppeteer.changeState(puppet, puppeteer.hitHead);
				return true;
			}
			
			case Fighter_Old.MSG_DO_PUNCH_HEAD : {
				puppeteer.changeState(puppet, puppeteer.punchHead);
				return true;
			}
			
			case Fighter_Old.MSG_DO_BLOCK_HEAD : {
				puppeteer.changeState(puppet, puppeteer.blockHead);
				return true;
			}
		}
		
		return false;
	}
}