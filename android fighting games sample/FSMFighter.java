package com.rodinalex.andgames.r13.fsm;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.gl.Texture;
import com.rodinalex.andgames.fw.gl.TextureRegion;
import com.rodinalex.andgames.fw.messaging.Telegram;
import com.rodinalex.andgames.r13.Fighter;
import com.rodinalex.andgames.r13.R13Game;

public class FSMFighter {
	
	public static String kConfigFile = "dsc/fighter.des";
	
	public Texture texture;
	
	public TextureRegion[] frames;
	
	public HashMap<String, FighterState> states;
	
	public FSMFighter(R13Game game) {
		try {
			//load config
			JSONObject config = game.getAssetMaster().jsonFromFile(kConfigFile);
			
			//load sprites
			texture = game.getAssetMaster().loadTextureFromFile(config.getString("texturePath"));
			JSONArray framesJSON = config.getJSONArray("frames");
			int numberOfFrames = framesJSON.length();
			frames = new TextureRegion[numberOfFrames];
			JSONArray frame = null;
			
			for (int i = 0; i < numberOfFrames; i++) {
				frame = framesJSON.getJSONArray(i);
				frames[i] = new TextureRegion(
					texture,
					frame.getInt(0),
					frame.getInt(1),
					frame.getInt(2),
					frame.getInt(3)
				);
			}
			
			//load states
			states = new HashMap<String, FighterState>();
			JSONArray statesCfg = config.getJSONArray("states");
			int len = statesCfg.length();
			FighterState state = null;
			for (int i = 0; i < len; i++) {
				state = new FighterState(statesCfg.getJSONObject(i), this);
				states.put(state.name, state);
			}
			
		} catch (Exception e) {
			throw new RuntimeException("CAN'T INITIALIZE FIGHTER FSM");
		}
	}
	public void setCurrentState(Fighter fighter, FighterState state) {
		fighter.currentState = state;
	}
	
	public void setGlobalState(Fighter fighter, FighterState state) {
		fighter.globalState = state;
	}
	
	public void setPreviousState(Fighter fighter, FighterState state) {
		fighter.previousState = state;
	}
	
	public void update(Fighter fighter, float deltaTime) {
		
		if (fighter.globalState != null) {
			fighter.globalState.update(fighter, deltaTime);
		}
		
		if (fighter.currentState != null) {
			fighter.currentState.update(fighter, deltaTime);
		}
		
	}
	
	public void render(Fighter fighter, SpriteBatcher batcher) {
		
		if (fighter.globalState != null) {
			fighter.globalState.render(fighter, batcher);
		}
		
		if (fighter.currentState != null) {
			fighter.currentState.render(fighter, batcher);
		}
	}
	
	public boolean handleMessage(Fighter fighter, final Telegram msg) {
		
		if (fighter.currentState != null && fighter.currentState.handleMessage(fighter, msg)) {
			return true;
		}
		
		if (fighter.globalState != null && fighter.globalState.handleMessage(fighter, msg)) {
			return true;
		}
		
		return false;
	}

	public void changeState(Fighter fighter, FighterState newState) {
		fighter.previousState = fighter.currentState;
		fighter.currentState.exit(fighter);
		fighter.currentState = newState;
		fighter.currentState.enter(fighter);
	}
	
	public void revertToPreviousState(Fighter fighter) {
		changeState(fighter, fighter.previousState);
	}
	
	public boolean isInState(Fighter fighter, final FighterState state) {
		if (state.equals(fighter.currentState)) {
			return true;
		}
		return false;
	}

	public FighterState getCurrentState(Fighter fighter) {
		return fighter.currentState;
	}
	
	public FighterState getGlobalState(Fighter fighter) {
		return fighter.globalState;
	}
	
	public FighterState getPreviousState(Fighter fighter) {
		return fighter.previousState;
	}
}
