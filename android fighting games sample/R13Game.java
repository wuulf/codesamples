
package com.rodinalex.andgames.r13;

import android.content.Context;
import android.content.IntentFilter;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.rodinalex.andgames.fw.GLGame;
import com.rodinalex.andgames.fw.gl.GLScreen;
import com.rodinalex.andgames.r13.konst.Constants;
import com.rodinalex.andgames.r13.konst.Messages;
import com.rodinalex.andgames.r13.network.WFDReceiver;
import com.rodinalex.andgames.r13.network.WFDService;
import com.rodinalex.andgames.r13.screen.ScreenCombat;
import com.rodinalex.andgames.r13.screen.ScreenMultiPlayer;
import com.rodinalex.andgames.r13.screen.ScreenMainMenu;

import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class R13Game extends GLGame{

	public static R13Game instance;

	public static final String TAG = "R13";

	boolean firstTimeCreate = true;

	private WifiP2pManager wifiManager;

	private WifiP2pManager.Channel  wifiChannel;

	private WFDReceiver wifiDirectReceiver;

	private IntentFilter wifiIntentFilter;

	private WifiP2pManager.PeerListListener peerListListener;

	private ArrayList<WifiP2pDevice> deviceList;

	public WifiP2pDevice selectedDevice;

    private WFDService wfdService;

    private static Handler wfdHandler;

	public int gameMode = 0; //0 - single player, 1 - multiplayer

	/**
	 * String buffer for outgoing messages
	 */
	private StringBuffer outBuffer;
	
	@Override
	public GLScreen getStartScreen() {
		return new ScreenMainMenu(this);
	}
	
	@Override
	public void init() {
		multiTouchOn = false;
		super.init();
		setAssetMaster(new R13AM(this));
		instance = this;
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {         
	    super.onSurfaceCreated(gl, config);
	    if(firstTimeCreate) {
	    	getAssetMaster().load();
	        firstTimeCreate = false;            
	    } else {
	        getAssetMaster().reload();
	    }
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setWiFiDirect();
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onPause() {
		super.onPause();
		unregisterReceiver(wifiDirectReceiver);
	}

	@Override
	public void onResume() {
		super.onResume();
		registerReceiver(wifiDirectReceiver, wifiIntentFilter);
	}

	public void setWiFiDirect() {

		wifiIntentFilter = new IntentFilter();
		wifiIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
		wifiIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
		wifiIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
		wifiIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

		wifiManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
		wifiChannel = wifiManager.initialize(this, getMainLooper(), null);
		wfdService = new WFDService(this, getWfdHandler());
		wifiDirectReceiver = new WFDReceiver(wifiManager, wifiChannel, wfdService, this);

		peerListListener = new WifiP2pManager.PeerListListener() {
			@Override
			public void onPeersAvailable(WifiP2pDeviceList wifiP2pDeviceList) {
				displayPeers(wifiP2pDeviceList);
			}
		};

		deviceList = new ArrayList<WifiP2pDevice>();

		selectedDevice = null;
	}

    public Handler getWfdHandler() {
        if (wfdHandler == null) {
			wfdHandler = new Handler() {
				@Override
				public void handleMessage(Message msg) {
					switch (msg.what) {
						case Messages.MESSAGE_STATE_CHANGE:
							switch (msg.arg1) {
								case WFDService.STATE_CONNECTED:
									break;
								case WFDService.STATE_CONNECTING:
									break;
								case WFDService.STATE_LISTEN:
								case WFDService.STATE_NONE:
									break;
							}
							break;
						case Messages.MESSAGE_WRITE:
							byte[] writeBuf = (byte[]) msg.obj;
							// construct a string from the buffer
							String mes = "";
							for (int i = 0; i < writeBuf.length; i++) {
								mes += writeBuf[i];
							}
							//String writeMessage = new String(writeBuf);
							Log.d(TAG, "write: " + mes);
							break;
						case Messages.MESSAGE_READ:
							byte[] readBuf = (byte[]) msg.obj;
							String m1 = "";
							for (int i = 0; i < readBuf.length; i++) {
								m1 += readBuf[i];
							}
							String msgString = "read: " + m1;
							Log.d(TAG, msgString);
							handleRemoteMessage(readBuf);
							break;
					}
				}
			};
		}
		return wfdHandler;
    }

	public void scanForPeers() {

		deviceList.clear();
		selectedDevice = null;

		wifiManager.discoverPeers(wifiChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.v(R13Game.TAG, "requesting peers");
                wifiManager.requestPeers(wifiChannel, peerListListener);
            }

            @Override
            public void onFailure(int reasonCode) {
                Log.v(TAG, "peers scan failure");
            }
        });
	}

	public void displayPeers(WifiP2pDeviceList peerList) {
		deviceList.clear();
		deviceList.addAll(peerList.getDeviceList());
		if (getCurrentScreen() instanceof ScreenMultiPlayer) {
			((ScreenMultiPlayer)getCurrentScreen()).displayPeers(deviceList);
		}
	}

	public void sendMessage(String message) {
		wfdService.write(message.getBytes());
	}

	public void sendMessage(byte[] msg) {
		wfdService.write(msg);
	}

	public void connect() {
		if (selectedDevice != null) {
			WifiP2pConfig config = new WifiP2pConfig();
			config.deviceAddress = selectedDevice.deviceAddress;
			wifiManager.connect(wifiChannel, config, wifiDirectReceiver);
		}
	}

	public void host() {
		wfdService.host(this, null);
	}

	public void join() {
		wfdService.join(selectedDevice);
	}

	public void setDeviceSelected(int position) {
		selectedDevice = deviceList.get(position);
	}

	public void onWifiModeSet(int mode) {
		if (getCurrentScreen() instanceof ScreenMultiPlayer) {
			((ScreenMultiPlayer) getCurrentScreen()).onWifiModeSet(mode);
		}
	}

	public void onDeviceConnectionSucceed() {
		if (getCurrentScreen() instanceof ScreenMultiPlayer) {
			((ScreenMultiPlayer) getCurrentScreen()).onDeviceConnected();
		}
	}

	public void onDeviceConnectionFailure(int reason) {
		String msg = "failed to connect to " + selectedDevice.deviceName + ", reason : " + reason;
		Log.v(TAG, msg);
		if (getCurrentScreen() instanceof ScreenMultiPlayer) {
			((ScreenMultiPlayer) getCurrentScreen()).onDeviceConnectionFailure(msg);
		}
	}

	public void onSocketConnectionReady() {
		Log.v(TAG, "Socket connection ready");
		if (getCurrentScreen() instanceof ScreenMultiPlayer) {
			((ScreenMultiPlayer) getCurrentScreen()).onReadyToStart();
		}
	}

	//TODO Refactor to proper message handling
	public void handleRemoteMessage(byte[] msg) {
		if (msg.length == 2) {
			if (msg[0] == Messages.COMBAT_MESSAGE) {
				if (getCurrentScreen() instanceof ScreenCombat) {
					((ScreenCombat) getCurrentScreen()).handleMessage(msg[1]);
				}
			}
		}
	}
}