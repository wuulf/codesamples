package com.rodinalex.andgames.r13.fsm;

import org.json.JSONObject;

import com.rodinalex.andgames.fw.fsm.Puppet;
import com.rodinalex.andgames.r13.Fighter_Old;

public class FighterStateHitHead extends FighterState_Old {

	public FighterStateHitHead(JSONObject config, PuppeteerFighter puppeteer) {
		super(config, puppeteer);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void enter(Puppet<Fighter_Old> puppet) {
		super.enter(puppet);
		puppet.entity.health -= 10.0f;
		if (puppet.entity.health < 0.1f) {
			puppet.entity.health = puppet.entity.maxHealth;
		}
	}
	
	@Override
	public void update(Puppet<Fighter_Old> puppet, float deltaTime) {
		puppet.timeInState += deltaTime;

		if (puppet.timeInState >= duration) {
			puppeteer.changeState(puppet, puppeteer.idle);
		}
	}

}
