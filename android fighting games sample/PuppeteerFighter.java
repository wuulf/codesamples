package com.rodinalex.andgames.r13.fsm;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rodinalex.andgames.fw.fsm.Puppeteer;
import com.rodinalex.andgames.fw.gl.Texture;
import com.rodinalex.andgames.fw.gl.TextureRegion;
import com.rodinalex.andgames.r13.Fighter_Old;
import com.rodinalex.andgames.r13.R13Game;

public class PuppeteerFighter extends Puppeteer<Fighter_Old> {
	public static String kConfigFile = "dsc/fighter.des";
	public Texture texture;
	
	public TextureRegion[] frames;
	
	public FighterState_Old idle;
	public FighterState_Old punchHead;
	public FighterState_Old blockHead;
	public FighterState_Old hitHead;
	
	public PuppeteerFighter(PuppeteerFighter clone) {
		texture = clone.texture;
		frames = clone.frames;
	}
	
	public PuppeteerFighter(R13Game game) {
		try {
			//load config
			JSONObject config = game.getAssetMaster().jsonFromFile(kConfigFile);
			
			//load sprites
			texture = game.getAssetMaster().loadTextureFromFile(config.getString("texturePath"));
			JSONArray framesJSON = config.getJSONArray("frames");
			int numberOfFrames = framesJSON.length();
			frames = new TextureRegion[numberOfFrames];
			JSONArray frame = null;
			
			for (int i = 0; i < numberOfFrames; i++) {
				frame = framesJSON.getJSONArray(i);
				frames[i] = new TextureRegion(
					texture,
					frame.getInt(0),
					frame.getInt(1),
					frame.getInt(2),
					frame.getInt(3)
				);
			}
			
			//load states
			JSONObject states = config.getJSONObject("states");
			idle = new FighterStateIdle(states.getJSONObject("idle"), this);
			punchHead = new FighterStatePunchHead(states.getJSONObject("punch_head"), this);
			blockHead = new FighterStateBlockHead(states.getJSONObject("block_head"), this);
			hitHead = new FighterStateHitHead(states.getJSONObject("hit_head"), this);
		} catch (Exception e) {
			throw new RuntimeException("CAN'T INITIALIZE FIGHTER PUPPETEER");
		}
	}
}
