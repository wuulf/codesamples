//
//  UI_Button.js
//  Framework
//
//  Created by Alex Rodin on 26.08.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

(function() {

UI_Button = function(config) {
	UI_Button._superClass.constructor.call(this, config);
	this.delegate = null;
	if (config && config.bubbleEvent) {
		this.bubbleEvent = config.bubbleEvent;
	};
};

CoreFW.extend(UI_Button, UI_View);

UI_Button.prototype.init = function() {
	UI_Button._superClass.init.call(this);
	var self = this;
	this.addEvent(this.element, 'click', function(e) {
		if (!self.bubbleEvent) {
			e.preventDefault();
		}
		self.buttonDidPress(e);
	});
};
    
UI_Button.prototype.buttonDidPress = function(e) {
	if (this.delegate) {
        this.delegate.buttonDidPress(this, e);
    } else {
		console.log("button pressed");
	}
};

UI_Button.prototype.setTitle = function(title) {
	this.element.innerHTML = title;
};

})();