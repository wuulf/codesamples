//
//  UI_PagesView.js
//  Framework
//
//  Created by Alex Rodin on 04.09.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

(function() {
	
UI_PagesView = function(config) {
	UI_PagesView._superClass.constructor.call(this);
	this.pages = [];
	this.pageViews = [];
	this.topPageIndex = 0;
};

CoreFW.extend(UI_PagesView, UI_View);

UI_PagesView.prototype.init = function() {

	UI_PagesView._superClass.init.call(this);
	this.initPages();	
};

UI_PagesView.prototype.initPages = function() {
	var els = this.getChildrenByClassName(this.element, "pages_view_page");
	this.pages = els;
	for (var i = 1, len = els.length; i < len; i++) {
		this.hidePage(i);
	}
};

UI_PagesView.prototype.getPageView = function(index) {
	return 	this.pageViews[index];
};

UI_PagesView.prototype.setViewForPage = function(view, index) {
	this.pageViews[index] = view;
};

UI_PagesView.prototype.showPage = function(index) {
	if (index === this.topPageIndex) {
		return;
	} else {
		this.hidePage(this.topPageIndex);
		if (this.pageViews[index]) {
			this.pageViews[index].show();
		} else {
			this.pages[index].style.display = "block";
		}
		this.topPageIndex = index;
	}
};

UI_PagesView.prototype.hidePage = function(index) {
	if (this.pageViews[index]) {
		this.pageViews[index].hide();
	} else {
		this.pages[index].style.display = "none";
	}
};


})();
