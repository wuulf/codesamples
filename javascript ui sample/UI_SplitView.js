//
//  UI_SplitView.js
//  Framework
//
//  Created by Alex Rodin on 04.09.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

(function() {
	
UI_SplitView = function(config) {
	UI_SplitView._superClass.constructor.call(this);
};

CoreFW.extend(UI_SplitView, UI_View);

UI_SplitView.prototype.init = function() {
	UI_SplitView._superClass.init.call(this);
	this.masterEl = this.getChildByClassName(this.element, "split_view_master");
	this.detailsEl = this.getChildByClassName(this.element, "split_view_details");
};

UI_SplitView.prototype.setMasterView = function(view) {
	this.masterView = view;
};

UI_SplitView.prototype.setDetailsView = function(view) {
	this.detailsView = view;
};

})();
