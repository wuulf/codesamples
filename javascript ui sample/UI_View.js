//
//  UI_View.js
//  Framework
//
//  Created by Alex Rodin on 23.08.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

(function() {
	
UI_View = function() {
	this.element = null;
};
	
UI_View.prototype = {
	
	/* Default initializer */
	init : function() {},
	
	/* Designated initializer */
	initWithElement : function(element) {
		this.element = document.getElementById(element);
		this.init();
	},
	
	/* get first child having clsName */
	getChildByClassName : function(element, clsName) {
		var els = element.getElementsByTagName('*');
	    for (i in els) {
	        if((' ' + els[i].className + ' ').indexOf(' ' + clsName + ' ') > -1) {
	        	return els[i];
	        }
	    }
	    return null;
	},
	
	/* get children having clsName */
	getChildrenByClassName : function(element, clsName) {
		var els = element.getElementsByTagName('*');
	    var result = [];
	    for (i in els) {
	        if((' ' + els[i].className + ' ').indexOf(' ' + clsName + ' ') > -1) {
	        	result.push(els[i]);
	        }
	    }
	    return result;
	},
	
	findElementParentByClassName : function(element, cls) {
	
		var result = null;
		var self = this;
		
		var search = function(el, cls) {
			if (self.hasClass(el, cls)) {
				result = el;
				return;
			} else if (el.parentNode && el.parentNode !== self.element) {
				search(el.parentNode, cls);
			} else {
				return;
			}
		};
		
		search(element, cls);
		return result;
	},
	
	/* cross-browser addEvent method */
	addEvent : function (el, eType, callback) {
	    if (!el) {
	   		throw "No el found for " + eType + callback.toSource();
	    }
	   
	    if (el.addEventListener) {
	        el.addEventListener(eType, callback, false);
	    } else if (el.attachEvent) {
	        el.attachEvent('on' + eType, callback);
	    } else {
	        el['on' + eType] = callback;
	    }
	},
	
	hide : function() {
		this.element.style.display = "none";
		this.hidden = true;
	},
	
	show : function() {
		this.element.style.display = "block";		
		this.hidden = false;
	},
	
	hasClass : function (el, cls) {
		return (' ' + el.className + ' ').indexOf(' ' + cls + ' ') > -1;
	}
};

})();
