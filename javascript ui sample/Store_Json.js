//
//  Store_Json.js
//  Bringo
//
//  Created by Alex Rodin on 11.09.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

(function(){

Store_Json = function(config) {
	this.items = [];
};

Store_Json.prototype.init = function() {
	this.clear();
};
	
Store_Json.prototype.clear = function() {
	this.items.length = 0;
};

Store_Json.prototype.loadData = function(data) {
	for (var i = 0, len = data.length; i < len; i++) {
		this.items.push(data[i]);
	}
};

Store_Json.prototype.getItemIndex = function(item) {
	return this.items.indexOf(item);
};

Store_Json.prototype.getItemAtIndex = function(ind) {
	return this.items[ind];
};

Store_Json.prototype.allItems = function() {
	return this.items.slice();
};

Store_Json.prototype.count = function() {
	return this.items.length;
};

})();


