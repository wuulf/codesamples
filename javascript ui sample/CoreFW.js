//
//  CoreFW.js
//  Framework
//
//  Created by Alex Rodin on 23.08.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

(function(){

CoreFW = {};

//  Extend method : subClass inherits from superClass
//  subClass now has property _superClass that points to superClass
CoreFW.extend = function(subClass, superClass) {

	var F = function() {};
	F.prototype = superClass.prototype;
	subClass.prototype = new F();
	subClass.prototype.constructor = subClass;
	
	subClass._superClass = superClass.prototype;
	
	if (superClass.prototype.constructor == Object.prototype.constructor) {
		superClass.prototype.constructor = superClass;
	} 
};

// Clone method
CoreFW.clone = function(obj) {
	function F() {};
	F.prototype = obj;
	return new F;
};
	
// Augment method
CoreFW.augment = function(tgtClass, srcClass) {
	if (arguments[2]) { //only give certain methods
		for (var i = 2, len = arguments.length; i < len; i++ ) {
			tgtClass.prototype[arguments[i]] = srcClass.prototype[arguments[i]];
		}
	} else {  // give all methods
		for (methodName in srcClass.prototype) {
			if (!tgtClass.prototype[methodName]) {
				tgtClass.prototype[methodName] = srcClass.prototype[methodName];
			}
		}
	} 
};

CoreFW.trim = function(str) {
	return str.replace(/^\s+|\s+$/g, '');
};

CoreFW.showErrorMessage = function(tgtField, message) {
	if (!this.visibleErrors) {
		this.errorFields = [];
		this.visibleErrors = [];
	}
	
	this.hideErrorMessage(tgtField);
	
	var el = document.createElement('div');
	el.className = "common_error_message";
	el.style.top = tgtField.offsetTop + tgtField.offsetHeight + "px";
	el.style.left = tgtField.offsetLeft + "px";
	el.innerHTML = '*' + message;
	
	this.errorFields.push(tgtField);
	this.visibleErrors.push(el);
	
	tgtField.parentNode.appendChild(el);
};

CoreFW.hideErrorMessage = function(tgtField) {
	
	if (!this.errorFields) {
		return;
	}
	
	var ind = this.errorFields.indexOf(tgtField);
	if (ind !== -1) {
		this.errorFields[ind].parentNode.removeChild(this.visibleErrors[ind]);
		this.errorFields[ind] = null;
		this.visibleErrors[ind] = null;
	}
};

CoreFW.addEvent = function (el, eType, callback) {
    if (!el) {
   		throw "No el found for " + eType + callback.toSource();
    }
   
    if (el.addEventListener) {
        el.addEventListener(eType, callback, false);
    } else if (el.attachEvent) {
        el.attachEvent('on' + eType, callback);
    } else {
        el['on' + eType] = callback;
    }
};

CoreFW.loadScript = function(scriptSource, callback) {
	var sc = document.createElement('script');
    sc.type = 'text/javascript';
    sc.src = scriptSource;
    document.head.appendChild(sc);
    this.addEvent(sc, 'load', callback, false);	
};

CoreFW.loadScripts = function(scriptList, outerCallback) {
	var ind = 0;
	var scriptList = scriptList;
	var self = this;
	var len = scriptList.length;
	
	var callback = function() {
		ind++;
		if (ind < len) {
			self.loadScript(scriptList[ind], callback);
		} else {
			outerCallback();
		}
	};
	
	this.loadScript(scriptList[ind], callback);
};


CoreFW.setCookie = function(name, value, days) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime() + (days * 24 * 60 * 60 * 1000);
		expires = "; expires=" + date.toGMTString();
	}
	document.cookie = name + "=" + escape(value) + "; expires=" + expires +  "; path=/";
};
		
CoreFW.getCookie = function(name){
	var searchName = name + "=";
	var cookies = document.cookie.split(';');
	for (var i = 0; i < cookies.length; i++) {
		var c = cookies[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1, c.length);
		if (c.indexOf(searchName) == 0)
			return c.substring(searchName.length, c.length);
	}
	return null;
};
		
CoreFW.eraseCookie = function(name) {
	this.setCookie(name, "", -1);
};

/* USES JQUERY */
CoreFW.numberInputHandler = function(event) {
    // Allow: backspace, delete, tab, escape, and enter
    if (event.keyCode == 46 || 
    	event.keyCode == 8 || 
    	event.keyCode == 9 || 
    	event.keyCode == 27 || 
    	event.keyCode == 13 || 
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) || 
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    } else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault(); 
        }   
	}
};

CoreFW.showMessage = function(msg, timeout) {
	var mb = document.getElementById("message_box");
	if (!mb) {
		mb = document.createElement('div');
		mb.className = "message_box";
		mb.id = "message_box";
		document.body.appendChild(mb);
	}
	
	mb.innerHTML = msg;
	mb.style.display = "block";
	setTimeout(function() {
		CoreFW.hideMessage();
	}, timeout ? timeout*1000 : 5000);
};

CoreFW.hideMessage = function() {
	var mb = document.getElementById("message_box");
	if (mb) {
		mb.innerHTML = "";
		mb.style.display = "none";
	}
};

})();
