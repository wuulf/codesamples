//
//  UI_DropDown.js
//  Bringo
//
//  Created by Alex Rodin on 29.08.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

(function(){

UI_DropDown = function(config) {
	UI_DropDown._superClass.constructor.call(this);
	Mixin_VerticalExpander.configureMixin(this, config);
};

CoreFW.extend(UI_DropDown, UI_View);

CoreFW.augment(UI_DropDown, Mixin_VerticalExpander);

UI_DropDown.prototype.init = function() {
	UI_DropDown._superClass.init.call(this);

	/* Overwrite maxHeight && currentHeight */
	/*this.maxHeight = this.element.offsetHeight;
	this.currentHeight = this.maxHeight;
	!this.expanded && this.collapse();*/
};

})();


