/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * @author Rodin Alex
 * @date 17.03.2017
 */

//TODO - minify && obfuscate

(function() {

var MAX_NUM = Number.MAX_VALUE;
var MIN_NUM = Number.MIN_VALUE;
var MIN_Y = 0;
var MAX_Y = 0;
var DEFAULT_WIDTH = 100;
var DEFAULT_HEIGHT = 100;
var VALUE_LINE_DASH = [5,2];
var DEFAULT_LINE_WIDTH = 1;
var HALF_PX = 0.5; //for proper determining canvas line width;
var DEFAULT_COL_HEIGHT = 1;
var COL_GAP = 2;
var MIN_DELTA = 3; //Minimal dx or dy to react to

var X_OFFSET_LEFT = 60;
var X_OFFSET_RIGHT = 10;
var Y_OFFSET_TOP = 25;
var Y_OFFSET_BOTTOM = 25;
var DEFAULT_TEXT_ALIGN = "left";
var DEFAULT_TEXT_MARGIN_BOTTOM = 4;
var DEFAULT_MINMAX_TEXT_MARGIN_LEFT = 4;
var RANGE_BUTTON_TEXT = "Часы";
var FOOTER_MARGIN_BOTTOM = 10;
var DEBUG = true;


// ********
// ERRORS
// ********

var ERROR_INVALID_DELEGATE = "ColChartWidget.setDelegate(delegate): delegate should implement colChartWidget_columnDidClick(column) method";

var testData = { valueField : "val", labelField : "label", items : []};
for (var i = 0; i < 10; i++) {
    testData.items.push({ val : Math.random() * 500 + 500, label : Math.floor(Math.random() * 10)});
}

var TEXT_STYLES = {
    RANGE_BUTTON : {
        fillColor : "#348ED3",
        font : "12px Verdana",
        textAlign : "left"
    },
    DEFAULT_TEXT : {
        fillColor : "#8D8D8D",
        font : "12px Verdana",
        textAlign : "left"
    },
    DEFAULT_LABEL_TEXT : {
        fillColor : "#5D5D5D",
        font : "12px Verdana",
        textAlign : "center"
    }
};

var LINE_STYLES = {
    DEFAULT_LINE : {
        color : "#000000",
        width : DEFAULT_LINE_WIDTH
    },
    MIN_VALUE_LINE : {
        color : "#3ADE20",
        dash : VALUE_LINE_DASH,
        width : DEFAULT_LINE_WIDTH
    },
    MAX_VALUE_LINE : {
        color : "#E50000",
        dash : VALUE_LINE_DASH,
        width : DEFAULT_LINE_WIDTH
    }
};

var RECT_STYLES = {
    DEFAULT_RECT : {
        fillColor : "#000000"
    },
    VALUE_RECT : {
        fillColor : "#FBFBFB"
    },
    HIGHLIGHTED_VALUE_RECT : {
        fillColor : "#FFD400"
    },
    BACKGROUND_RECT : {
        fillColor : "#D8D8D8"
    }
};

var Painter = function(ctx) {
    this.ctx = ctx;
};

Painter.prototype = {
    drawLine : function(x1, y1, x2, y2, aStyle) {
        var ctx = this.ctx;
        var style = aStyle || LINE_STYLES.DEFAULT_LINE;
        ctx.save();
        ctx.beginPath();
        ctx.lineWidth = style.width;
        ctx.strokeStyle = style.color;
        ctx.moveTo(x1, y1 + HALF_PX);
        ctx.lineTo(x2, y2 + HALF_PX);
        style.dash && ctx.setLineDash(style.dash);
        ctx.stroke();
        ctx.restore();
    },
    
    drawRect : function(x1, y1, x2, y2, aStyle) {
        var ctx = this.ctx;
        var style = aStyle || RECT_STYLES.DEFAULT_REC;
        ctx.save();
        ctx.fillStyle = style.fillColor;
        style.strokeStyle && (ctx.strokeStyle = style.strokeColor);
        ctx.fillRect(x1, y1, x2, y2);
        ctx.restore();
    },
    drawText : function(x, y, text, aStyle) {
        var ctx = this.ctx;
        var style = aStyle || TEXT_STYLES.DEFAULT_TEXT;
        ctx.save();
        ctx.textAlign= style.textAlign || DEFAULT_TEXT_ALIGN;
        ctx.fillStyle = style.fillColor;
        ctx.font = style.font;
        ctx.fillText(text, x, y);
        ctx.restore();
    }
};

var ColChartWidget = function() {
    this.columns = [];
    this.canvas = null;
    this.canvasEl = null;
    this.width = DEFAULT_WIDTH;
    this.height = DEFAULT_HEIGHT;
    this.minValue = MAX_NUM;
    this.maxValue = MIN_NUM;
    this.minValueY = 0;
    this.maxValueY = 0;
    this.minY = MIN_Y;
    this.maxY = MAX_Y;
    this.painter = null;
    this.colWidth = 1;
    this.selectedColumnIndex = -1;
    this.highlightedColumnIndex = -1;
    this.lastEventCoords = [0,0];
};

ColChartWidget.prototype = {
    
    initWithContainer : function(container) {
        var canvas = $("<canvas></canvas>").appendTo(container);
        this.canvas = canvas;
        this.canvasEl = canvas.get(0);
        this.painter = new Painter(this.canvasEl.getContext("2d"));
        this.attachListeners();
        return this;
    },
    
    setDelegate: function(delegate) {
        if (typeof(delegate.colChartWidget_columnDidClick) === 'function') {
            this.delegate = delegate;
        } else {
            DEBUG && console.error(ERROR_INVALID_DELEGATE);
        }
        return this;
    },
   
    attachListeners : function() {
        var self = this;
        this.canvas.mousemove(function(e) {
            var target = $(e.target);
            var offset = $(this).offset();
            
            var oldXY = self.lastEventCoords;
            
            var newXY = [
                e.pageX - offset.left,
                e.pageY - offset.top
            ];
            
            var dx = Math.abs(oldXY[0] - newXY[0]);
            var dy = Math.abs(oldXY[1] - newXY[1]);
            var delta = Math.max(dx, dy);
            
            if (delta > MIN_DELTA) {
                self.lastEventCoords = newXY;
                self.draw();
            }
        });
        this.canvas.click(function(e) {
            e.preventDefault();
            var index = self.highlightedColumnIndex;
            DEBUG && console.log("canvas did click", index);
            if (
                index >= 0 
                && self.delegate 
            ) {
                self.delegate.colChartWidget_columnDidClick(self.columns[index]);
            }
        });
    },
   
    setDimensions : function(width, height) {
        this.width = width;
        this.height = height;
        this.graphHeight = this.height - Y_OFFSET_BOTTOM - Y_OFFSET_TOP;
        this.maxValueY = Y_OFFSET_TOP;
        this.canvas.attr("width", width);
        this.canvas.attr("height", height);
        return this;
    },
    
    draw : function() {
        var p = this.painter;
        var w = this.width;
        var h = this.height;
        p.drawRect(0, 0, w, h, RECT_STYLES.BACKGROUND_RECT);
        this.drawColumns();
        this.drawMinMax();
        p.drawLine(0, h - Y_OFFSET_BOTTOM, w, h - Y_OFFSET_BOTTOM, LINE_STYLES.DEFAULT_LINE);
        p.drawText(
            DEFAULT_MINMAX_TEXT_MARGIN_LEFT, 
            h - FOOTER_MARGIN_BOTTOM, 
            RANGE_BUTTON_TEXT, 
            TEXT_STYLES.RANGE_BUTTON
        );
        return this;
    },
    
    drawMinMax : function() {
        var p = this.painter;
        var w = this.width;
        var h = this.height;
        p.drawLine(0, this.maxValueY, w, this.maxValueY, LINE_STYLES.MAX_VALUE_LINE);
        p.drawText(
            DEFAULT_MINMAX_TEXT_MARGIN_LEFT, 
            this.maxValueY - DEFAULT_TEXT_MARGIN_BOTTOM, 
            this.maxValue,
            TEXT_STYLES.DEFAULT_TEXT
        );
        p.drawLine(0, this.minValueY, w, this.minValueY, LINE_STYLES.MIN_VALUE_LINE);
        p.drawText(
            DEFAULT_MINMAX_TEXT_MARGIN_LEFT, 
            this.minValueY - DEFAULT_TEXT_MARGIN_BOTTOM, 
            this.minValue,
            TEXT_STYLES.DEFAULT_TEXT
        );
    },
    
    drawColumns : function() {
        this.highlightedColumnIndex = -1;
        for (var i = 0, len = this.columns.length; i < len; i++) {
            var col = this.columns[i];
            var x = i * this.colWidth + X_OFFSET_LEFT;
            var h = col.val/(this.maxValue || 1) * this.graphHeight;
            var y = this.height - Y_OFFSET_BOTTOM - h;
            var w = this.colWidth - COL_GAP;
            var style = RECT_STYLES.VALUE_RECT;
            col._screenData = [x, y, w, h];
            if (this.isMouseOverColumn(col)) {
                style = RECT_STYLES.HIGHLIGHTED_VALUE_RECT;
                this.highlightedColumnIndex = i;
                this.painter.drawText(
                    x + w/2, 
                    this.maxValueY - DEFAULT_TEXT_MARGIN_BOTTOM, 
                    col.val, 
                    TEXT_STYLES.DEFAULT_LABEL_TEXT
                );
            }
            this.painter.drawRect(x, y, w, h, style);
            this.painter.drawText(
                x + w/2, 
                this.height - FOOTER_MARGIN_BOTTOM, 
                col.label, 
                TEXT_STYLES.DEFAULT_LABEL_TEXT
            );
        }
    },
    
    isMouseOverColumn : function(column) {
        var x = column._screenData[0];
        var y = column._screenData[1];
        var w = column._screenData[2];
        var h = column._screenData[3];
        var mX = this.lastEventCoords[0];
        var mY = this.lastEventCoords[1];
        if (mX < x || mY < y || mX > x + w || mY > y + h) {
            return false;
        }
        return true;
    },
    
    /*
     * @argument {object} data <{ valueField : <fieldName>, labelField: <fieldName>, items : [{item}] }>
     */
    
    setData : function(data) {
        var size = data.items.length;
        for (var i = 0; i < size; i++) {
            var rec = data.items[i];
            this.columns.push({ 
                val : rec[data.valueField],
                label : rec[data.labelField],
                _screenData : null,
                raw : rec
            });
        }
        this.preprocessColumns();
        return this;
    },
    
    preprocessColumns : function() {
        var len = this.columns.length;
        for (var i = 0; i < len; i++) {
            var col = this.columns[i];
            var val = Math.floor(col.val);
            col.val = val;
            this.minValue = Math.min(val, this.minValue);
            this.maxValue = Math.max(val, this.maxValue);
            this.minValueY = this.maxValueY + (1 - this.minValue/(this.maxValue || 1)) * this.graphHeight;
        }
        this.colWidth = (this.width - X_OFFSET_LEFT - X_OFFSET_RIGHT) / (len || 1);
        return this;
    },
    
    sortData : function(key) {
        this.columns = this.columns.sort(function(c1, c2) {
            return c1.raw[key] < c2.raw[key] ? -1 : (c1.raw[key] > c2.raw[key] ? 1 : 0);
        });
        return this;
    }
};

/*
$(document).ready(function(){
    
    var delegate = {
        colChartWidget_columnDidClick2 : function(column) {
           DEBUG && console.log("column did click", column);
        }
    };
    
    var chart = new ColChartWidget()
        .initWithContainer("#chart_container")
        .setDimensions(480, 240)
        .setData(testData)
        .sortData("label")
        .setDelegate(delegate)
        .draw()
    ;
});
*/

})();
