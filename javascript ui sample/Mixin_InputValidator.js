//
//  Mixin_InputValidator.js
//  Framework
//
//  Created by Alex Rodin on 23.08.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

(function() {

var kDefaultAnimationFrameMS = 16;

Mixin_InputValidator = function() {};

/* Required method to call before augmenting obj with mixin */
Mixin_InputValidator.configureMixin = function(obj, config) {
	obj.validationConfig = config;
};

Mixin_InputValidator.prototype = {
	
	validate : function(vConfig) {
		var tgts = vConfig || this.validationConfig;
		var len = tgts.length;
		var res = true;
		for (var i = 0; i < len; i++) {
			if (!this.validateField(tgts[i].field, tgts[i].validator, vConfig[i].fieldName)) {
				res = false;
			}
		}
		return res;
	},
	
	validateField : function(field, validator, fieldName) {
		var result = Misc_Validation[validator](field.value, fieldName);
		if (!result.valid) {
			var cls = field.className;
			cls = cls.replace(" invalid_field", "");
			field.className = cls;
			field.className += " invalid_field";
			CoreFW.showErrorMessage(field, result.message);
			return false;
		} else {
			var cls = field.className;
			cls = cls.replace(" invalid_field", "");
			field.className = cls;
			CoreFW.hideErrorMessage(field);
			return true;
		}
	}
};
    
})();
