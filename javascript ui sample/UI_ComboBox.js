//
//  UI_ComboBox.js
//  Bringo
//
//  Created by Alex Rodin on 29.08.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

(function(){

UI_ComboBox = function(config) {
	UI_ComboBox._superClass.constructor.call(this);
	this.contentStore = config.store;
	this.contentView = config.view;
	this.maxHeight = config.maxHeight;
};

CoreFW.extend(UI_ComboBox, UI_View);

UI_ComboBox.prototype.init = function() {
	UI_ComboBox._superClass.init.call(this);
	
	var valueElement = this.getChildByClassName(this.element, "combobox_value");
	this.valueElement = valueElement;
	
	var expandButton = new UI_Button();
	expandButton.element = this.getChildByClassName(this.element, "combobox_expand_button");
	expandButton.init();
	expandButton.delegate = this;
	this.expandButton = expandButton;

	var dropDown = new UI_DropDown({
		expanded : false,
		minHeight : 0,
		maxHeight : this.maxHeight
	});
	
	var self = this;
	
	this.addEvent(document, 'mousedown', function(e) {
		if (self.dropDown.expanded) {
			var pos = self.element.compareDocumentPosition(e.target);
			if (!(pos == 20)) {
				self.collapseDropDown();
			}
		}
	});
	
	this.addEvent(this.valueElement, 'keyup', function(e) {
		self.search();		
	});
	
	dropDown.element = this.getChildByClassName(this.element, "combobox_dropdown");
	dropDown.init();
	//dropDown.maxHeight = 130;
	this.dropDown = dropDown;
	
	var view = this.contentView;
	view.element = dropDown.element;
	view.init();
	view.delegate = this;
	view.element.style.width = this.element.offsetWidth + "px";
	
	this.clear();
};
	
UI_ComboBox.prototype.clear = function() {
	this.setValue("");
};

UI_ComboBox.prototype.search = function() {
	var value = this.valueElement.value;
	var data = this.contentStore.search(value);
	this.searchData = data;
	if (!this.dropDown.expanded) {
		this.expandDropDown();
	}
	this.contentView.setData(data);
};

UI_ComboBox.prototype.expandDropDown = function() {
	this.dropDown.expand(true);
};

UI_ComboBox.prototype.collapseDropDown = function() {
	this.dropDown.collapse(true);
};

UI_ComboBox.prototype.buttonDidPress = function(button, e) {
	if (button == this.expandButton) {
		if (this.dropDown.expanded) {
			this.collapseDropDown();
		} else {
			this.expandDropDown();
			this.contentView.setData(this.contentStore.allItems());
		}
	}
};

UI_ComboBox.prototype.cellDidClick = function(cell, e) {
	var ind = this.contentView.getCellIndex(cell);
	if (ind !== -1) {
		var item = this.contentStore.getItemAtIndex(ind);
		this.setValue(item, ind);
		
		this.collapseDropDown();
	}
};

UI_ComboBox.prototype.setValue = function(item, index) {
	if (this.valueElement.tagName == "INPUT") {
		this.valueElement.value = item ? item.text : "";
	} else {
		if (!this.defaultValueCssClass) {
			this.defaultValueCssClass = this.valueElement.className;
		}
		this.valueElement.innerHTML = item ? item.text : "";
		this.valueElement.className = item.cssClass + " " + this.defaultValueCssClass;
	}
	
	if (index >= 0) {
		this.selectedItemIndex = index;	
		if (this.delegate) {
			this.delegate.comboBoxValueChanged(this);	
		};
	};
};

UI_ComboBox.prototype.getValue = function() {
	if (this.valueElement.tagName == "INPUT") {
		return this.valueElement.value;
	} else {
		return this.valueElement.innerHTML;
	}
};

})();


