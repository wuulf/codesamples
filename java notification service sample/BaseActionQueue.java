package com.bringo247.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;

import com.bringo247.model.ExecutionModel;

public abstract class BaseActionQueue {

	private final BlockingQueue< ExecutionModel > queue;
	
	public BaseActionQueue() {
		queue = new DelayQueue<ExecutionModel>();
	}
	
	public void add(final ExecutionModel action) {
        if( !queue.contains( action )) {
        	queue.offer(action);
        }
	}
	
	public ExecutionModel poll() {
		return queue.poll();
	}
	
	public int size() {
		return queue.size();
	}
}
