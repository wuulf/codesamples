package com.bringo247.thread;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.bringo247.misc.Const;
import com.bringo247.queue.LogQueue;

public class LogWriter extends Thread {
	
	public static final String LOG_FILE_NAME = "bringo_app_execution.log";
	private LogQueue log;
	
	public LogWriter(String name) {
		super(name);
		log = LogQueue.getInstance();
	}
	
	public void run() {
        while (true) {
            try {
            	writeToFile();
                Thread.sleep(Const.LOG_WRITER_DELAY);
            } catch (IOException ioe) {
            	ioe.printStackTrace();
        	} catch (InterruptedException ie) {
                ie.printStackTrace();
                return;
            } 
        }
    }
	
	private void writeToFile() throws IOException {
    	BufferedWriter writer = null;
        try {
        	String filePath = System.getProperty("catalina.base") + "/logs";
            File file = new File(filePath, LOG_FILE_NAME);
            writer = new BufferedWriter(new FileWriter(file, true));
            String msg = null;
            while ((msg = log.poll()) != null) {
            	writer.write(msg);
            	writer.newLine();
            }
        } catch ( IOException e ) {
            e.printStackTrace();
        } finally {
            if ( writer != null )  {
            	writer.close();
            }
        }
	}
}
