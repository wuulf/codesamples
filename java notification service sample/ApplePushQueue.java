package com.bringo247.queue;

public class ApplePushQueue extends BaseActionQueue {
	
	private static ApplePushQueue _instance;
	
	private ApplePushQueue() {
		super();
	}
	
	public static ApplePushQueue getInstance() {
		if (_instance == null) {
			synchronized (ApplePushQueue.class) {
				if (_instance == null) {
					_instance = new ApplePushQueue();
				}
			}
		}
		return _instance;
	}
}
