package com.bringo247.thread;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.TimeZone;

import com.bringo247.misc.Const;
import com.bringo247.model.ActionModel;
import com.bringo247.queue.DeliveryStatusPushQueue;
import com.bringo247.queue.ResultQueue;

public class DeliveryStatusPusher extends Thread {

	private DeliveryStatusPushQueue queue;
	private ResultQueue resultQueue;
	private String apiUrl;
	
	public DeliveryStatusPusher(String name, String url) {
		super(name);
		apiUrl = url;
		queue = DeliveryStatusPushQueue.getInstance();
		resultQueue = ResultQueue.getInstance();
		apiUrl = Const.DLV_STATUS_URL;
	}
	
	public void run() {
        while (true) {
            try {
            	ActionModel action = null; 
            	while ((action = ((ActionModel)queue.poll())) != null) {
            		execute(action);
            	}
                Thread.sleep(Const.DELIVERY_STATUS_PUSH_DELAY);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
                return; //stop execution
            } 
        }
    }
	
	public void execute(ActionModel action) {
		HttpURLConnection connection = null;
		String serviceMessage = "";
		try {
			if (action.message_payload != null) {
				URL url = new URL(apiUrl);
				connection = (HttpURLConnection)url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type","application/json");
				OutputStream os = connection.getOutputStream();
				os.write(action.message_payload.getBytes());
				os.flush();
				int code = connection.getResponseCode();
				if (code >= 200 && code < 300) {
					action.action_status = Const.ACTION_STATUS_COMPLETED;
					action.message_status = Const.MSG_STATUS_DONE;
					serviceMessage = "Action completed. HTTP_CODE: " + code;
				} else {
					action.action_status = Const.ACTION_STATUS_FAILED;
					action.message_status = Const.MSG_STATUS_FAILED;
					
					String resBody = ""; 
					
					BufferedReader in = new BufferedReader(new InputStreamReader(connection.getErrorStream()));
					String inputLine;
					while ((inputLine = in.readLine()) != null) {
						resBody += inputLine;
					}
					in.close();
					
					serviceMessage = "Action failed. HTTP_CODE: "
						+ code + ". MESSAGE: " + connection.getResponseMessage()
						+ ". RESPONSE: " + resBody;
					
					Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
					int nextAttemptDelay = (int)(
						Const.BASE_ATTEMPT_DELAY * 
						Math.pow((double)2, (double)action.attempt_number)
					);
					Timestamp ts = new Timestamp(cal.getTimeInMillis() + nextAttemptDelay);
					action.next_try_time = ts;
					action.attempt_number++;
				}
			} else {
				action.action_status = Const.ACTION_STATUS_FAILED;
				action.message_status = Const.MSG_STATUS_FAILED;
				serviceMessage = "Action failed. Payload is null.";
			}
		} catch (ProtocolException pe) {
			pe.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Protocol exeption: " + pe.getMessage();
		} catch (MalformedURLException mfe) {
			mfe.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Invalid URL: " + mfe.getMessage();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. IO exeption: " + ioe.getMessage();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
			resultQueue.add(action);
			action.error_message = serviceMessage;
			ActionModel.log(getName(), action, serviceMessage);
		}
	}
}
