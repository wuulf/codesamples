package com.bringo247.model;

import java.util.Date;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class LogMsgModel {
	
	public String jsonString;
	
	public LogMsgModel() {}
	
	public static LogMsgModel modelWithMap(Map<String, String> map) {
		LogMsgModel model = new LogMsgModel();
		try {
			model.jsonString = mapToJsonString(map);
		} catch (JSONException e) {
			Date d = new Date();
			model.jsonString = 
				"{ time : " + d.toString() + 
				", actor: \"LogMsgModel\", error : \"Failure to create json from map\"}"
			;
		}
		return model;
	}
	
	public static String mapToJsonString(Map<String, String> map) throws JSONException {
		JSONObject json = new JSONObject(map);
		return json.toString();
	}
}
