package com.bringo247.queue;

public class SmsQueue extends BaseActionQueue {
	
	private static SmsQueue _instance;
	
	private SmsQueue() {
		super();
	}
	
	public static SmsQueue getInstance() {
		if (_instance == null) {
			synchronized (SmsQueue.class) {
				if (_instance == null) {
					_instance = new SmsQueue();
				}
			}
		}
		return _instance;
	}
}
