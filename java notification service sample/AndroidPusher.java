package com.bringo247.thread;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.bringo247.misc.Const;
import com.bringo247.model.ActionModel;
import com.bringo247.queue.AndroidPushQueue;
import com.bringo247.queue.ResultQueue;


public class AndroidPusher extends Thread {
	
	private String gcmKey;
	private AndroidPushQueue queue;
	private ResultQueue resultQueue;
	
	public AndroidPusher(String name) {
		super(name);
		queue = AndroidPushQueue.getInstance();
		resultQueue = ResultQueue.getInstance();
		gcmKey = "key=" + Const.GCM_KEY;
	}

	public void run() {
        while (true) {
        	try {
        		ActionModel action = null; 
        		while ((action = ((ActionModel)queue.poll())) != null) {
        			execute(action, false);
        		}
  				Thread.sleep(Const.ANDROID_PUSH_DELAY);
			} catch (InterruptedException ie) {
				ie.printStackTrace();
				return; //stop execution
			} 
        }
    }
	
	private void execute(ActionModel action, boolean dryRun) {
		
		HttpURLConnection connection = null;
		String serviceMessage = "";

		try {
			int valid = actionValid(action);
			if (valid == 1) {
				
				ArrayList<String> regIds = new ArrayList<String>();
				regIds.add(action.addr);
				URL url = new URL(Const.GOOGLE_URL);
				connection = (HttpURLConnection)url.openConnection();
				connection.setDoOutput(true);
				connection.setRequestMethod("POST");
				connection.setRequestProperty("Content-Type","application/json");
				connection.setRequestProperty("Authorization", gcmKey);

				JSONObject data = new JSONObject();
				data.put("registration_ids", regIds);
				data.put("data", new JSONObject(action.message_payload));
				data.put("dry_run", dryRun);
			
				OutputStream os = connection.getOutputStream();
				os.write(data.toString().getBytes());
				os.flush();
				
				int code = connection.getResponseCode();
				
				if (code >= 200 && code < 300) {
					//TODO - for now assume, that push is delivered
					action.action_status = Const.ACTION_STATUS_COMPLETED;
					action.message_status = Const.MSG_STATUS_DONE;
					serviceMessage = "Action completed. HTTP_CODE: " + code;
					
					/*BufferedReader br = new BufferedReader(
						new InputStreamReader(connection.getInputStream())
					);
	                StringBuilder sb = new StringBuilder();
	                String line;
	                while ((line = br.readLine()) != null) {
	                    sb.append(line+"\n");
	                }
		            br.close();
		            JSONObject resData = new JSONObject(sb.toString());
					System.out.println(
						"Successes: " + resData.getInt("success") + 
						", Failures: " + resData.getInt("failure")
					);*/
				} else {
					action.action_status = Const.ACTION_STATUS_FAILED;
					action.message_status = Const.MSG_STATUS_FAILED;
					serviceMessage = "Action failed. HTTP_CODE: " + code;
				}
			} else if (valid == -1) {
				serviceMessage = "Action failed. Address Token is null.";
			} else if (valid == -2) {
				serviceMessage = "Action failed. Message Payload is null.";
			}
		} catch (JSONException jse) {
			jse.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Invalid payload JSON:" + jse.getMessage();
		} catch (ProtocolException pe) {
			pe.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Protocol exeption: " + pe.getMessage();
		} catch (MalformedURLException mfe) {
			mfe.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Invalid URL: " + mfe.getMessage();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. IO exeption: " + ioe.getMessage();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
			resultQueue.add(action);
			ActionModel.log(getName(), action, serviceMessage);
		}
	}
		
	
	//TODO - refactor to proper messaging
    private int actionValid(ActionModel action) {
    	if (action.addr == null) {
    		action.action_status = Const.ACTION_STATUS_FAILED;
    		action.message_status = Const.MSG_STATUS_FAILED;
    		return -1;
    	}
    	
    	if (action.message_payload == null) {
    		action.action_status = Const.ACTION_STATUS_FAILED;
    		action.message_status = Const.MSG_STATUS_FAILED;
    		return -2;
    	}
    	
    	return 1;
    }
}
