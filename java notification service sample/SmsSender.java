package com.bringo247.thread;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.bringo247.misc.Const;
import com.bringo247.model.ActionModel;
import com.bringo247.model.LogMsgModel;
import com.bringo247.queue.LogQueue;
import com.bringo247.queue.ResultQueue;
import com.bringo247.queue.SmsQueue;


public class SmsSender extends Thread {

	private String smsChannelUrl;
	private SmsQueue queue;
	private ResultQueue resultQueue;
	private LogQueue logQueue;
	
	public SmsSender(String name, String smsChannelUrl) {
		super(name);
		this.smsChannelUrl = smsChannelUrl;
		queue = SmsQueue.getInstance();
		resultQueue = ResultQueue.getInstance();
		logQueue = LogQueue.getInstance();
	}
	
	public void run() {
        while (true) {
            try {
            	ActionModel action = null; 
            	while ((action = ((ActionModel)queue.poll())) != null) {
            		execute(action);
            	}
                Thread.sleep(Const.SMS_SENDER_DELAY);
            } catch (InterruptedException ie) {
                ie.printStackTrace();
                return; //stop execution
            } 
        }
    }
	
	public void execute(ActionModel action) {
		HttpURLConnection connection = null;
		String serviceMessage = "";
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Date now = Calendar.getInstance().getTime();        
		String dateString = df.format(now);
		
		try {
			if (action.message_payload != null) {
				JSONObject obj = new JSONObject(action.message_payload);
				String to = URLEncoder.encode(obj.getString("to"), "UTF-8");
				String text = URLEncoder.encode(obj.getString("text"), "UTF-8");
				String msg = String.format("to=%s&text=%s&coding=2&charset=utf-8", to, text);
				URL url = new URL(smsChannelUrl + msg);
				connection = (HttpURLConnection)url.openConnection();
				connection.setRequestMethod("GET");
				connection.connect();
				int code = connection.getResponseCode();
				if (code >= 200 && code < 300) {
					action.action_status = Const.ACTION_STATUS_COMPLETED;
					action.message_status = Const.MSG_STATUS_DONE;
					serviceMessage = "Action completed. HTTP_CODE: " + code;
				} else {
					action.action_status = Const.ACTION_STATUS_FAILED;
					action.message_status = Const.MSG_STATUS_FAILED;
					serviceMessage = "Action failed. HTTP_CODE: " + code;
				}
			} else {
				action.action_status = Const.ACTION_STATUS_FAILED;
				action.message_status = Const.MSG_STATUS_FAILED;
				serviceMessage = "Action failed. Payload is null.";
			}
		} catch (JSONException jse) {
			jse.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Invalid payload JSON:" + jse.getMessage();
		} catch (ProtocolException pe) {
			pe.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Protocol exeption: " + pe.getMessage();
		} catch (MalformedURLException mfe) {
			mfe.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Invalid URL: " + mfe.getMessage();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. IO exeption: " + ioe.getMessage();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
			resultQueue.add(action);
			logAction(action, dateString, serviceMessage);
		}
	}
	
	private void logAction(ActionModel action, String dateString, String message) {
    	HashMap<String, String> map = new HashMap<String, String>();
    	map.put("time", dateString);
    	map.put("actor", getName());
    	map.put("action_id", String.valueOf(action.id));
    	map.put("action_type", String.valueOf(action.action_type));
    	map.put("message_id", String.valueOf(action.message_id));
    	map.put("message_payload", action.message_payload);
    	map.put("action_status", String.valueOf(action.action_status));
    	map.put("service_message", message);
    	logQueue.add(LogMsgModel.modelWithMap(map));
    }
}
