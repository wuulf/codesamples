package com.bringo247.model;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class ExecutionModel implements Delayed {
	
	public final long id;
	private final long createTime;
	public final long delay;
	public final long message_id;
	
	public ExecutionModel( long id, long message_id, long delay) {
		this.id = id;
		this.message_id = message_id;
		this.createTime = System.currentTimeMillis();
		this.delay = delay;
	}

	@Override
	public int compareTo(Delayed obj) {

		if (obj == this) {
            return 0;
        }
		
		long delta = 0;
		
        if (obj instanceof ExecutionModel) {
        	delta = delay - ((ExecutionModel)obj).delay;
            return ((delta == 0) ? 0 : ((delta < 0) ? -1 : 1));
        }
        
        delta = (getDelay(TimeUnit.MILLISECONDS ) - obj.getDelay(TimeUnit.MILLISECONDS));
	    return ((delta == 0) ? 0 : ((delta < 0) ? -1 : 1 ));
	}

	@Override
	public long getDelay(TimeUnit unit) {
		long now = System.currentTimeMillis();
		return unit.convert(delay - (now - createTime), TimeUnit.MILLISECONDS);
	}
	
	
	@Override
    public int hashCode() {
		return new HashCodeBuilder(29, 67).
            append(id).
            append(message_id).
            toHashCode()
        ;
		
	}

    @Override
    public boolean equals(Object obj) {
    	
    	if( this == obj ) {
            return true;
        }
        
        if( !( obj instanceof ExecutionModel ) ) {
            return false;
        }
    		 
        final ExecutionModel other = (ExecutionModel)obj;
        
        return new EqualsBuilder().
            append(id, other.id).
            append(message_id, other.message_id).
            isEquals()
        ;
    }
}
