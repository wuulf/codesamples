package com.bringo247.thread;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.TreeSet;

import com.bringo247.misc.Const;
import com.bringo247.model.ActionModel;
import com.bringo247.queue.ResultQueue;

public class ResultWriter extends Thread {

	private Connection connection;
	private ResultQueue queue;
	private static String updateMessageString = "UPDATE ntf.msgq SET status_id = ? WHERE id = ?";
	private static String updateActionString = ""
			+ "UPDATE actions.action_msg "
			+ "SET status = ?, next_try_time = ?, attempt_number = ?, error_message = ? "
			+ "WHERE id = ?"
	;
	
	public ResultWriter(String name, Connection connection) {
		super(name);
		this.connection = connection;
		queue = ResultQueue.getInstance();
	}
	
	public void run() {
        while (true) {
            try {
            	writeToDB();
                Thread.sleep(Const.RESULT_WRITER_DELAY);
            } catch (SQLException se) {
            	 try {
                 	connection.close();
                 }catch (SQLException sqle) {
                 	sqle.printStackTrace();
                 	return; //stop execution
                 }
                 return; //stop execution
            } catch (InterruptedException ie) {
                ie.printStackTrace();
                try {
                	connection.close();
                }catch (SQLException sqle) {
                	sqle.printStackTrace();
                	return; //stop execution
                }
                return; //stop execution
            } 
        }
    }
	
	private void writeToDB() throws SQLException{
		ActionModel action = null;
		PreparedStatement psMessages = null;
		PreparedStatement psActions = null;
		TreeSet<Long> actionIds = new TreeSet<Long>();
		int psMessagesBatchSize = 0;
		int psActionsBatchSize = 0;
    	try {
    		connection.setAutoCommit(false);
    		psMessages = connection.prepareStatement(updateMessageString);
    		psActions = connection.prepareStatement(updateActionString);
    		while ((action = (ActionModel)(queue.poll())) != null) {
	    		if (action.message_id != 0) {
	    			psMessages.setInt(1, action.message_status);
	    			psMessages.setLong(2, action.message_id);
	    			psMessages.addBatch();
	    			psMessagesBatchSize++;
	    		}
	    		if (!actionIds.contains(action.id)) {
	    			actionIds.add(action.id);
	    			psActions.setInt(1, action.action_status);
	    			psActions.setTimestamp(2, action.next_try_time);
	    			psActions.setInt(3, action.attempt_number);
	    			psActions.setString(4,  action.error_message);
	    			psActions.setLong(5, action.id);
	    			psActions.addBatch();
	    			psActionsBatchSize++;
	    		}
	    		ActionModel.log(getName(), action, "Writing action && message statuses to DB");
    		}
    		if (psMessagesBatchSize > 0) {
    			psMessages.executeBatch();
    		}
    		if (psActionsBatchSize > 0) {
    			psActions.executeBatch();
    		}
    		connection.commit();
    		psMessages.close();
    		psActions.close();
    	} catch (SQLException e) {
    		e.printStackTrace();
    		throw e;
    		//e.getNextException().printStackTrace();
    	} finally {
    		if (psMessages != null) {
    			try {
    				psMessages.close();
    			} catch (SQLException sqle) {
    				sqle.printStackTrace();
    			}
    		}
    		if (psActions != null) {
    			try {
    				psActions.close();
    			} catch (SQLException sqle) {
    				sqle.printStackTrace();
    			}
    		}
    	}
    }
}
