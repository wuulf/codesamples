package com.bringo247.queue;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.bringo247.model.LogMsgModel;

public class LogQueue {
	
	private final BlockingQueue< String > queue;
	
	private static LogQueue _instance;
	
	private LogQueue() {
		this.queue = new LinkedBlockingQueue<String>();
	}
	
	public static LogQueue getInstance() {
		if (_instance == null) {
			synchronized (LogQueue.class) {
				if (_instance == null) {
					_instance = new LogQueue();
				}
			}
		}
		return _instance;
	}
	
	public void add(final LogMsgModel model) {
        if( !queue.contains(model.jsonString)) {
        	queue.offer(model.jsonString);
        }
	}
	
	public String poll() {
		return queue.poll();
	}
	
	public int size() {
		return queue.size();
	}
}
