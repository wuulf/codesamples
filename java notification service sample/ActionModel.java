package com.bringo247.model;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import com.bringo247.queue.LogQueue;

public class ActionModel extends ExecutionModel {
	
	/** Action Attributes **/
	public int action_status;
	public int action_type;
	
	/** Message Attributes **/
	public int chan_id;
	public int message_status;
	public String addr;
	public String message_payload;
	
	/** Aux attributes **/
	public int error_code;
	public String error_message;
	public int attempt_number;
	public Object auxInfo;
	public Timestamp next_try_time;
	
	public ActionModel(long id, long message_id, int delay) {
		super(id, message_id, delay);
	}
	
	public ActionModel(
		long id,
		int action_status,
		int action_type,
		int chan_id,
		long message_id,
		int message_status,
		String addr,
		String message_payload,
		int attempt_number,
		long delay
	) {
		super(id, message_id, delay);
		this.action_type = action_type;
		this.action_status = action_status;
		this.chan_id = chan_id;
		this.message_status = message_status;
		this.addr = addr;
		this.message_payload = message_payload;
		this.attempt_number = attempt_number;
	}
	
	public static void log(String actor, ActionModel action, String message) {
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		Date now = Calendar.getInstance().getTime();        
		String dateString = df.format(now);
		
		HashMap<String, String> map = new HashMap<String, String>();
    	map.put("time", dateString);
    	map.put("actor", actor);
    	map.put("chan_id", String.valueOf(action.chan_id));
    	map.put("action_id", String.valueOf(action.id));
    	map.put("action_type", String.valueOf(action.action_type));
    	map.put("message_id", String.valueOf(action.message_id));
    	map.put("message_payload", action.message_payload);
    	map.put("action_status", String.valueOf(action.action_status));
    	map.put("service_message", message);
    	
    	LogQueue.getInstance().add(LogMsgModel.modelWithMap(map));
	}
}
