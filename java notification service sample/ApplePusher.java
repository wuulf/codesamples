package com.bringo247.thread;

import java.io.IOException;
import java.io.InputStream;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotifications;

import javax.servlet.ServletContext;

import org.json.JSONException;

import com.bringo247.misc.Const;
import com.bringo247.model.ActionModel;
import com.bringo247.queue.ApplePushQueue;
import com.bringo247.queue.ResultQueue;


public class ApplePusher extends Thread {
	
	private ServletContext context;
	private String certFile;
	
	private ResultQueue resultQueue;
	private ApplePushQueue queue;
	
	public ApplePusher(String name, ServletContext context, String certFile) {
		super(name);
		this.context = context;
		this.certFile = certFile;
		queue = ApplePushQueue.getInstance();
		resultQueue = ResultQueue.getInstance();
	}
	
	public void run() {
        while (true) {
        	try {
        		ActionModel action = null; 
        		while ((action = ((ActionModel)queue.poll())) != null) {
        			execute(action);
        		}
  				Thread.sleep(Const.APPLE_PUSH_DELAY);
			} catch (InterruptedException ie) {
				ie.printStackTrace();
				return; //stop execution
			} 
        }
    }
	
	public void execute(ActionModel action) {
		
		InputStream resourceContent = null;
		String serviceMessage = "";

		try {
			int valid = actionValid(action);
			
			if (valid == 1) {
				resourceContent = context.getResourceAsStream(certFile);
				PushNotificationPayload payload = PushNotificationPayload.fromJSON(action.message_payload);
				PushedNotifications notes = Push.payload(
					payload, 
					resourceContent, 
					Const.APPLE_CERT_PASS, 
					false, 
					action.addr
				);
				if (notes.getSuccessfulNotifications().size() > 0) {
					action.action_status = Const.ACTION_STATUS_COMPLETED;
					action.message_status = Const.MSG_STATUS_DONE;
					serviceMessage = "Action completed.";
				} else {
					action.action_status = Const.ACTION_STATUS_COMPLETED;
					action.message_status = Const.MSG_STATUS_DONE;
					serviceMessage = "Action failed. Apple refused.";
				}
			} else if (valid == -1) {
				serviceMessage = "Action failed. Address Token is null.";
			} else if (valid == -2) {
				serviceMessage = "Action failed. Message Payload is null.";
			}
		} catch (JSONException jse) {
			jse.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Invalid payload JSON:" + jse.getMessage();
		} catch (KeystoreException kse) {
			kse.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Keystore exeption: " + kse.getMessage();
		} catch (CommunicationException ce) {
			ce.printStackTrace();
			action.action_status = Const.ACTION_STATUS_FAILED;
			action.message_status = Const.MSG_STATUS_FAILED;
			serviceMessage = "Action failed. Communication exeption: " + ce.getMessage();
		} finally {
			try {
				resourceContent.close();
			} catch (IOException e) {
				e.printStackTrace();
				action.action_status = Const.ACTION_STATUS_FAILED;
				action.message_status = Const.MSG_STATUS_FAILED;
				serviceMessage = "Action failed. IO exeption: " + e.getMessage();
			}
			resultQueue.add(action);
			ActionModel.log(getName(), action, serviceMessage);
		}
	}
	
	//TODO - refactor to proper messaging
    private int actionValid(ActionModel action) {
    	if (action.addr == null) {
    		action.action_status = Const.ACTION_STATUS_FAILED;
    		action.message_status = Const.MSG_STATUS_FAILED;
    		return -1;
    	}
    	if (action.message_payload == null) {
    		action.action_status = Const.ACTION_STATUS_FAILED;
    		action.message_status = Const.MSG_STATUS_FAILED;
    		return -2;
    	}
    	return 1;
    }
}
