package com.bringo247.queue;

public class DeliveryStatusPushQueue extends BaseActionQueue {
	
	private static DeliveryStatusPushQueue _instance;
	
	private DeliveryStatusPushQueue() {
		super();
	}
	
	public static DeliveryStatusPushQueue getInstance() {
		if (_instance == null) {
			synchronized (DeliveryStatusPushQueue.class) {
				if (_instance == null) {
					_instance = new DeliveryStatusPushQueue();
				}
			}
		}
		return _instance;
	}

}
