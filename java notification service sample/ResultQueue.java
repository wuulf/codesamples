package com.bringo247.queue;

public class ResultQueue extends BaseActionQueue {

	private static ResultQueue _instance;
	
	private ResultQueue() {
		super();
	}
	
	public static ResultQueue getInstance() {
		if (_instance == null) {
			synchronized (ResultQueue.class) {
				if (_instance == null) {
					_instance = new ResultQueue();
				}
			}
		}
		return _instance;
	}
}
