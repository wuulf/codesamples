package com.bringo247.queue;


public class AndroidPushQueue extends BaseActionQueue {

	private static AndroidPushQueue _instance;
	
	private AndroidPushQueue() {
		super();
	}
	
	public static AndroidPushQueue getInstance() {
		if (_instance == null) {
			synchronized (AndroidPushQueue.class) {
				if (_instance == null) {
					_instance = new AndroidPushQueue();
				}
			}
		}
		return _instance;
	}
}
