package com.bringo247.thread;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;

import org.postgresql.PGConnection;
import org.postgresql.PGNotification;

import com.bringo247.misc.Const;
import com.bringo247.misc.Stats;
import com.bringo247.model.ActionModel;
import com.bringo247.queue.AndroidPushQueue;
import com.bringo247.queue.ApplePushQueue;
import com.bringo247.queue.BaseActionQueue;
import com.bringo247.queue.DeliveryStatusPushQueue;
import com.bringo247.queue.ResultQueue;
import com.bringo247.queue.SmsQueue;

public class ActionReader extends Thread {

	public static final boolean[] allowed_actions = {
		false, //Better not to use index 0 for action types
		true   //1 - send message action
	};
	
	public static final boolean[] allowed_channels = {
		false, //Better not to use index 0 for channel types
		true,  //1 sms
		false, //2 email
		true,  //3 anroid push
		true,  //4 ios push
		true   //5 delivery status partner push
	};

	private Connection connection;
	private PGConnection pgConnection;
	private long lastUpdateTime = 0;
	private HashMap<Integer, BaseActionQueue> queues;
	private ResultQueue resultQueue;
	
	private ArrayList<ActionModel> actionBuffer;
	private LinkedHashSet<Long> actionIds;
	
    public ActionReader(String name, Connection connection) throws SQLException {
    	super(name);
    	actionBuffer = new ArrayList<ActionModel>();
    	queues = new HashMap<Integer, BaseActionQueue>();
    	actionIds = new LinkedHashSet<Long>();
        this.connection = connection;
        this.pgConnection = (PGConnection)connection;
        
        Statement s = connection.createStatement();
        s.execute("LISTEN actionmsg");
        s.close();
        lastUpdateTime = System.currentTimeMillis();
        //Channel queues
        queues.put(1, SmsQueue.getInstance()); //for sms 
        queues.put(2, null); //for email
        queues.put(3, AndroidPushQueue.getInstance()); //for android pushes
        queues.put(4, ApplePushQueue.getInstance()); //for ios pushes
        queues.put(5, DeliveryStatusPushQueue.getInstance()); //for delivery status push
        //Result queue
        resultQueue = ResultQueue.getInstance();
    }
    
    public void run() {
        while (true) {
            try {
                PGNotification notifications[] = pgConnection.getNotifications();
                if (notifications != null && notifications.length > 0) {
                	getActions();
                } else {
                	long delta = System.currentTimeMillis() - lastUpdateTime;
                	if (delta > Const.ACTION_READER_DELAY_AUTO_UPDATE) {
                		getActions();
                		lastUpdateTime = System.currentTimeMillis();
                	}
                }
                Thread.sleep(Const.ACTION_READER_DELAY_BASE);
            } catch (SQLException sqle) {
                sqle.printStackTrace();
                return; //stop execution
            } catch (InterruptedException ie) {
                ie.printStackTrace();
                try {
                	connection.close();
                } catch (SQLException sqle) {
                	sqle.printStackTrace();
                	return; //stop execution
                } 
                return; //stop execution
            }
        }
    }
    
    private void setStatus(Date date) throws SQLException{
    	String dbConnectionStatus = connection.isClosed() ? "closed" : "open";
    	Stats.getInstance().setStatus(getName() + ":Postgre Connection", dbConnectionStatus, date);
    }
    
    private void getActions() throws SQLException {
    	actionBuffer.clear();
    	actionIds.clear();
    	
    	Date now = new Date();
    	
		Timestamp ts = new Timestamp(now.getTime());
    	
    	PreparedStatement ps = connection.prepareStatement(
			"SELECT * FROM actions.new_actions_v" +
			" WHERE next_try_time <= ? AND attempt_number < ?"
		);
    	
    	ps.setTimestamp(1, ts);
		ps.setInt(2, Const.ACTION_MAX_ATTEMPTS);
		
		boolean result = ps.execute();
		
		if (result) {
			ResultSet rs = ps.getResultSet();
	    	ActionModel model = null;
			
	    	while(rs.next()) {
	    		model = new ActionModel(rs.getLong("action_id"), rs.getLong("message_id"), 0);
	    		model.action_status = rs.getInt("action_status");
	    		model.action_type = rs.getInt("action_type");
	    		model.chan_id = rs.getInt("channel_id");
	    		model.message_status = rs.getInt("message_status");
	    		model.addr = rs.getString("addr");
	    		model.message_payload = rs.getString("message_payload");
	    		model.attempt_number = rs.getInt("attempt_number");
	    		actionBuffer.add(model);
	    		actionIds.add(model.id);
	    		ActionModel.log(getName(), model, "Action Read Successfully");
	    	}
		}
		ps.close();
		
		if (lockActions()) {
			queueActions();
		}
    	
		setStatus(now);
    }
    
    private boolean lockActions() {
    	try {
    		PreparedStatement ps = connection.prepareStatement(
				"UPDATE actions.action_msg SET status = ? WHERE id = ANY(?)"
			);
    		Array arr = connection.createArrayOf("bigint", actionIds.toArray());
	    	ps.setInt(1, 50);
	    	ps.setArray(2, arr);
	    	ps.execute();
	    	ps.close();
    	} catch (SQLException e) {
    		e.printStackTrace();
    		return false;
    	}
    	
    	return true;
    }
    
    private void queueActions() {
    	int len = actionBuffer.size();
    	for (int i = 0; i < len; i++){
    		ActionModel action = actionBuffer.get(i);
    		BaseActionQueue q = null;
    		if (actionValid(action) && (q = queues.get(action.chan_id)) != null) {
				//Only process if action supported by service && we have corresponding queue
    			q.add(action);
    			ActionModel.log(getName(), action, Const.MSG_ACTION_TAKEN_FOR_EXECUTION);
    		} else {
    			//If action not supported - put it at once to result queue and log
    			action.action_status = Const.ACTION_STATUS_NOT_SUPPORTED;
    			action.message_status = Const.MSG_STATUS_FAILED;
    			resultQueue.add(action);
    			ActionModel.log(getName(), action, Const.MSG_ACTION_NOT_SUPPORTED);
    		}
    	}
    }
    
    private boolean actionValid(ActionModel model) {
    	return isActionTypeSupported(model.action_type) 
			&& isChannelSupported(model.chan_id);
    }
    
    //TODO - refactor to enum
    private boolean isActionTypeSupported(int actionType) {
    	if (actionType > allowed_actions.length - 1 || actionType < 0) {
    		return false;
    	}
    	return allowed_actions[actionType];
    }
    
    private boolean isChannelSupported(int channelId) {
    	if (channelId > allowed_channels.length - 1 || channelId < 0) {
    		return false;
    	}
    	return allowed_channels[channelId];
    }
}
