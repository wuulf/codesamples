package com.mgm.bean.view;

import com.mgm.core.data.model.AccountModel;
import com.mgm.core.data.model.RoleModel;
import com.mgm.core.data.model.UserModel;
import com.mgm.core.service.AccountService;
import com.mgm.core.service.UserService;
import com.mgm.utils.Msg;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.commons.lang3.RandomStringUtils;

/**
 *
 * @author alexrodin
 */
@ManagedBean
@ViewScoped
public class UserProfileEditView {
    private boolean isNew = false;
    private static final int DEFAULT_ROLE_ID = 3; //omni
    private static final int TEMP_NEW_USER_ID = -1;
    private static final int DEFAULT_PASSWORD_LENGTH = 12;
    private UserModel user;
    private String generatedPassword;
    private String repeatPassword;
    private List<AccountModel> accounts;
    private List<RoleModel> roles;
    
    /**
     * Creates a new instance of UserProfileEditView
     */
    public UserProfileEditView() {
    }
    
    @PostConstruct
    public void init() {
        
        generatePassword(null);
        Map<String, String> paramMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String idParam = paramMap.get("id");
        roles = UserService.getRoles();
        
        
        if (idParam == null) {
            isNew = true;
            user = new UserModel();
            user.setRoleId(DEFAULT_ROLE_ID);
            user.setRole(UserService.getRoleById(DEFAULT_ROLE_ID));
            user.setId(TEMP_NEW_USER_ID);
        } else {
            user = UserService.getById(Integer.valueOf(idParam));
            if (user.getAccountId() != null) {
                user.setAccount(AccountService.getById(user.getAccountId()));
            }
            if (user.getRoleId() != null) {
                user.setRole(UserService.getRoleById(user.getRoleId()));
            }
        }
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }
    
    public List<AccountModel> completeAccount(String query) {
        accounts = AccountService.getAll();
        List<AccountModel> accountsFiltered = new ArrayList();
        for (int i = 0; i < accounts.size(); i++) {
            AccountModel account = accounts.get(i);
            if(account.getAccountName().toLowerCase().contains(query)) {
                accountsFiltered.add(account);
            }
        }
        return accountsFiltered;
    }

    public void generatePassword(ActionEvent event) {
        //System.out.println("generating new password...");
        generatedPassword = RandomStringUtils.randomAlphanumeric(DEFAULT_PASSWORD_LENGTH);
        //user.setUserPassword(generatedPassword);
        //repeatPassword = generatedPassword;
        //System.out.print(generatedPassword);
    }
    
    public String getGeneratedPassword() {
        return generatedPassword;
    }

    public void setGeneratedPassword(String generatedPassword) {
        this.generatedPassword = generatedPassword;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public boolean isIsNew() {
        return isNew;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    public List<AccountModel> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountModel> accounts) {
        this.accounts = accounts;
    }

    public List<RoleModel> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleModel> roles) {
        this.roles = roles;
    }
    
    public void save() {
        if (UserService.exists(user) && isNew) {
            Msg.addMessageError("Unable to create user!", "User with email \"" + user.getUserEmail() +  "\" already exists!");
            return;
        }
        
        if (user.getAccount() != null) {
            user.setAccountId(user.getAccount().getId());
        } else {
            user.setAccountId(-1);
        }
        
        user.setRoleId(user.getRole().getId());
        UserService.upsertUser(user);
        onSaveSuccess();
    }
    
    public String onSaveSuccess() {
        Msg.addMessage("Success!", "User " + user.getUserEmail() + " has been saved");
        return null;
    }
    
    //TODO - hardcoded system roles
    public void onRoleChanged() {
        int id = user.getRole().getId();
        user.setRoleId(user.getRole().getId());
        if (id == 4 || id == 1) {
            user.setAccount(null);
            user.setAccountId(-1);
        }
        System.out.println("role changed" + user.getRoleId());
    }
}
