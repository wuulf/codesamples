package com.improvead.bean.campaign;

import com.improvead.utils.DefaultDateIntervals;
import com.mgm.core.data.model.CountryModel;
import com.mgm.core.data.model.SiteModel;
import com.mgm.core.data.model.campaign.CampaignCountryModel;
import com.mgm.core.data.model.campaign.CampaignModel;
import com.mgm.core.data.model.campaign.CampaignSiteModel;
import com.mgm.core.data.model.campaign.CampaignStateModel;
import com.mgm.core.data.model.campaign.CampaignTagModel;
import com.mgm.core.data.model.campaign.TagOwnedModel;
import com.mgm.core.env.Service;
import com.mgm.core.service.AdSizeService;
import com.mgm.core.service.CampaignService;
import com.mgm.core.service.CountryService;
import com.mgm.core.service.SiteService;
import com.mgm.core.service.TagOwnedService;
import com.mgm.utils.DateUtils;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.metrics.MetricsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.sum.InternalSum;
import org.joda.time.DateTime;
import org.primefaces.model.DualListModel;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.LinearAxis;

/**
 *
 * @author alexrodin
 */
@ManagedBean
@ViewScoped
public class CampaignBean {

    private boolean isNew = false;
    private int campaignId;
    private CampaignModel campaign = new CampaignModel();
    private List<TagOwnedModel> tags;
    private List<TagOwnedModel> selectedTags = new ArrayList<>();
    private List<SiteModel> sites;
    private List<SiteModel> selectedSites = new ArrayList<>();
    private List<CampaignStateModel> states;
    private List<CountryModel> countries;
    private List<CountryModel> selectedCountries = new ArrayList<>();
    private Map<String, String> paramMap;
    private DualListModel<TagOwnedModel> tagList;
    private DualListModel<SiteModel> siteList;
    private DualListModel<CountryModel> countryList;
    
    // stats    
    private Date reportFrom;
    private Date reportTo;
    private BarChartModel reportChartData = new BarChartModel();
    private List<SelectItem> dateRanges;
    private DefaultDateIntervals selectedDateRange;
    private double reportSpentTotal;
    private double reportCpm;
    private double reportSiteCpm;
    private long reportImpsTotal;    
    private double reportSiteSpentTotal;
    
    /**
     * Creates a new instance of CampaignBean
     */
    public CampaignBean() {
        
    }
    
    @PostConstruct
    public void init() {
        sites = SiteService.getAll();
        states = CampaignService.getCampaignStates();
        countries = CountryService.getAll();
        tags = TagOwnedService.getAll();
        for (int i = 0, len = tags.size(); i < len; i++) {
            TagOwnedModel tag = tags.get(i);
            tag.setAdSizeModel(AdSizeService.getById(tag.getAdSizeId()));
        }
        
        paramMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String idParam = paramMap.get("id");
        if (idParam == null) {
            isNew = true;
        } else {
            campaignId = Integer.valueOf(idParam);
            loadCampaignParams(campaignId);
        }
        
        countryList = new DualListModel(countries, selectedCountries);
        tagList = new DualListModel(tags, selectedTags);
        siteList = new DualListModel(sites, selectedSites);
        
        tags.removeAll(selectedTags);
        sites.removeAll(selectedSites);
        countries.removeAll(selectedCountries);
        
        dateRanges = DateUtils.getDateIntervalList();
        selectedDateRange = DefaultDateIntervals.THIS_WEEK;
        
        reportFrom = selectedDateRange.getInterval().getStart().toDate();
        reportTo = selectedDateRange.getInterval().getEnd().toDate();
                
        if (!isNew) {
            loadDefaultGraph();
            loadCampaignReport();
        }
    }
    
    private void updateReportDateRange() {
        reportFrom = selectedDateRange.getInterval().getStart().toDate();
        reportTo = selectedDateRange.getInterval().getEnd().toDate();
    }
    
    private void loadCampaignParams(int campaignId) {
        campaign = CampaignService.getById(campaignId);
        selectedTags = CampaignService.getCampaignTags(campaign);
        selectedSites = CampaignService.getCampaignSites(campaign);
        selectedCountries = CampaignService.getCampaignCountries(campaign);
    }
    
    private void loadCampaignReport() {        
    
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        reportChartData = new BarChartModel();       
        reportChartData.setMouseoverHighlight(false);        
        reportChartData.setLegendPosition("ne");
        
        Axis yAxis = new LinearAxis("Sum");
        yAxis.setTickFormat("€ %#.1f");
        reportChartData.getAxes().put(AxisType.Y, yAxis);
         
        Axis y2Axis =  new LinearAxis("Impressions");
        reportChartData.getAxes().put(AxisType.Y2, y2Axis);
        
        ChartSeries chartSum = new ChartSeries("Sum");
        ChartSeries chartImps = new ChartSeries("Impressions");        
        
        chartImps.setYaxis(AxisType.Y2);
        
        if (selectedDateRange == null) {
            selectedDateRange = DefaultDateIntervals.THIS_WEEK;
        }
        
        reportFrom = selectedDateRange.getInterval().getStart().toDate();
        reportTo = selectedDateRange.getInterval().getEnd().toDate();
        
        RangeQueryBuilder range = QueryBuilders
            .rangeQuery("time")
            .from(sdf.format(reportFrom)).format("yyyy-MM-dd")
            .to(sdf.format(reportTo)).format("yyyy-MM-dd");
        
        MatchQueryBuilder campaignSearch = QueryBuilders
            .matchQuery("campaignid", this.campaign.getId());

        MetricsAggregationBuilder priceAgg = 
            AggregationBuilders.sum("agg").field("price");
        
        MetricsAggregationBuilder priceSiteAgg = 
            AggregationBuilders.sum("agg2").field("siteprice");

        // Aggregate by
        // Day
        //    -- Sum
        AggregationBuilder dayAgg =
        AggregationBuilders
            .dateHistogram("aggDay")
            .field("time")
            .interval(DateHistogramInterval.DAY)
            .subAggregation(priceAgg)
            .subAggregation(priceSiteAgg);
        
        BoolQueryBuilder qb = boolQuery()
            .must(range)
            .must(campaignSearch);

        SearchResponse sr = Service.INSTANCE.getElasticsearch()
            .prepareSearch("mgp_campaign-*")
            .setQuery(qb)
            .addAggregation(dayAgg)
            .setSize(0)
            .execute()
            .actionGet();      
        
        Histogram agg = sr.getAggregations().get("aggDay");
        reportSpentTotal = 0;
        reportCpm = 0;
        reportImpsTotal = 0;
        reportSiteSpentTotal = 0;
        
        reportChartData.clear();
        
        for (Histogram.Bucket entry : agg.getBuckets()) {

            DateTime key = (DateTime)entry.getKey();
            
            double price = ((InternalSum)entry
                    .getAggregations()
                    .get("agg"))
                    .getValue();
            
            double priceSite = ((InternalSum)entry
                    .getAggregations()
                    .get("agg2"))
                    .getValue();
            
            long imps = entry.getDocCount();
            
            chartSum.set(sdf.format(key.toDate()), price);
            chartImps.set(sdf.format(key.toDate()), imps);
            
            reportImpsTotal += imps;
            reportSpentTotal += price;
            reportSiteSpentTotal += priceSite;
        }
        
        if (reportImpsTotal > 0) {
            reportCpm = reportSpentTotal / (reportImpsTotal / 1000);
            reportSiteCpm = reportSiteSpentTotal / (reportImpsTotal / 1000);
        }
               
        if (chartSum.getData().isEmpty()) {
            chartSum.set(0, 0);
            chartImps.set(0, 0);
        }
        
        reportChartData.addSeries(chartSum);
        reportChartData.addSeries(chartImps);
            
        reportChartData.getAxis(AxisType.X).setTickAngle(-45);
    }
    
    private void loadDefaultGraph() {        
    
    }        
    
    public void setReportChartData(BarChartModel reportChartData) {
        this.reportChartData = reportChartData;
    }

    public BarChartModel getReportChartData() {
        return reportChartData;
    }

    public DefaultDateIntervals getSelectedDateRange() {
        return selectedDateRange;
    }

    public void setSelectedDateRange(DefaultDateIntervals selectedDateRange) {
        this.selectedDateRange = selectedDateRange;
    }

    public DualListModel<SiteModel> getSiteList() {
        return siteList;
    }

    public void setSiteList(DualListModel<SiteModel> siteList) {
        this.siteList = siteList;
    }

    public DualListModel<CountryModel> getCountryList() {
        return countryList;
    }

    public void setCountryList(DualListModel<CountryModel> countryList) {
        this.countryList = countryList;
    }

    public DualListModel<TagOwnedModel> getTagList() {
        return tagList;
    }

    public void setTagList(DualListModel<TagOwnedModel> tagList) {
        this.tagList = tagList;
    }
    
    public CampaignModel getCampaign() {
        return campaign;
    }

    public void setCampaign(CampaignModel campaign) {
        this.campaign = campaign;
    }
    
    public void saveMapping() {
        List<CampaignTagModel> campaignTags = new ArrayList();
        List<CampaignSiteModel> campaignSites = new ArrayList();
        List<CampaignCountryModel> campaignCountries = new ArrayList();
        
        for (CountryModel m : countryList.getTarget()) {
            campaignCountries.add(new CampaignCountryModel(m.getId(), campaign.getId()));
        }
        
        for (TagOwnedModel m : tagList.getTarget()) {
             campaignTags.add(new CampaignTagModel(m.getId(), campaign.getId()));
        }
        
        for (SiteModel m : siteList.getTarget()) {
            campaignSites.add(new CampaignSiteModel(m.getId(), campaign.getId()));
        }
        
        CampaignService.removeMapping(campaign);
        if (campaignCountries.size()> 0) {
            CampaignService.setCampaignCountries(campaignCountries);
        }
        if (campaignSites.size() > 0) {
            CampaignService.setCampaignSites(campaignSites);
        }
        if (campaignTags.size() > 0) {
            CampaignService.setCampaignTags(campaignTags);
        }
    }
    
    public void onSaveButtonPressed() {
        if (campaign.getId() != null) {
            CampaignService.update(campaign);
            saveMapping();
        } else {
            CampaignService.insert(campaign);
            saveMapping();
        }
        System.out.println("campaign created/updated " + campaign.getId());
        saveSuccess();
    }

    public List<TagOwnedModel> getTags() {
        return tags;
    }

    public void setTags(List<TagOwnedModel> tags) {
        this.tags = tags;
    }

    public List<SiteModel> getSites() {
        return sites;
    }

    public void setSites(List<SiteModel> sites) {
        this.sites = sites;
    }

    public List<TagOwnedModel> getSelectedTags() {
        return selectedTags;
    }

    public void setSelectedTags(List<TagOwnedModel> selectedTags) {
        this.selectedTags = selectedTags;
    }

    public List<SiteModel> getSelectedSites() {
        return selectedSites;
    }

    public void setSelectedSites(List<SiteModel> selectedSites) {
        this.selectedSites = selectedSites;
    }

    public List<CampaignStateModel> getStates() {
        return states;
    }

    public void setStates(List<CampaignStateModel> states) {
        this.states = states;
    }

    public List<CountryModel> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryModel> countries) {
        this.countries = countries;
    }

    public List<CountryModel> getSelectedCountries() {
        return selectedCountries;
    }

    public void setSelectedCountries(List<CountryModel> selectedCountries) {
        this.selectedCountries = selectedCountries;
    }

    public List<SelectItem> getDateRanges() {
        return dateRanges;
    }

    public void setDateRanges(List<SelectItem> dateRanges) {
        this.dateRanges = dateRanges;
    }        
    
    public double getReportSpentTotal() {
        return reportSpentTotal;
    }

    public double getReportCpm() {
        return reportCpm;
    }

    public long getReportImpsTotal() {
        return reportImpsTotal;
    }

    public double getReportSiteSpentTotal() {
        return reportSiteSpentTotal;
    }

    public void setReportSiteSpentTotal(double reportSiteSpentTotal) {
        this.reportSiteSpentTotal = reportSiteSpentTotal;
    }    

    public double getReportSiteCpm() {
        return reportSiteCpm;
    }

    public void setReportSiteCpm(double reportSiteCpm) {
        this.reportSiteCpm = reportSiteCpm;
    }    
    
    public void onDateRangeChange() {
        updateReportDateRange();
        loadCampaignReport();
    }

    public boolean isIsNew() {
        return isNew;
    }

    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }
    
    public String saveSuccess() {
        addMessage("Success!", "Campaign " + campaign.getTitle()+ " has been saved");
        /*try {
            Thread.sleep(2000);
            FacesContext.getCurrentInstance().getExternalContext().redirect("/admin/campaign/list");
        } catch (InterruptedException | IOException e) {
            throw new FacesException(e);
        }*/
        return null;
    }

    public String saveFailure() {
        addMessageError("Failure!",  "Campaign " + campaign.getTitle() + " could not be saved");
        return null;
    }
    
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void addMessageError(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary,  null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void addMessage(String summary, String info) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  info);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    public void addMessageError(String summary, String info) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary,  info);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
    
    
}