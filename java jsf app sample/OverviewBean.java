/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.improvead.bean;

import com.improvead.model.report.*;
import com.improvead.utils.DefaultDateIntervals;
import com.improvead.web.jsf.UserBean;
import com.mgm.core.data.activemodel.v2.Account;
import com.mgm.core.data.model.CurrencyListItemModel;
import com.mgm.core.data.model.SiteModel;
import com.mgm.core.data.model.UserModel;
import com.mgm.core.service.CurrencyService;
import com.mgm.core.service.SiteService;
import com.mgm.core.service.UserService;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;
import org.joda.time.Interval;
import org.ocpsoft.logging.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LegendPlacement;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.primefaces.model.chart.LinearAxis;

/**
 *
 * @author ar
 */
@ManagedBean
@ViewScoped
public class OverviewBean implements Serializable {

    @ManagedProperty(value = "#{userBean}")
    private UserBean userBean;
    private InventoryDataReport thisMonthReport;
    private InventoryDataReport lastMonthReport;
    private InventoryDataReport totalReport;
    private InventoryDataReport graphReport;
    private LineChartModel revenueGraphModel;
    private List<SelectItem> intervalSelectItems;
    private DefaultDateIntervals selectedDefaultDateInterval;
    private List<SiteModel> accountSites;
    private ArrayList<Integer> siteFilter;
    private List<CurrencyListItemModel> currencies;
    private CurrencyListItemModel selectedCurrency;
    
    /**
     * Creates a new instance of DashboardBean
     */
    public OverviewBean() {
        intervalSelectItems = new ArrayList<>();
        intervalSelectItems.add(new SelectItem(DefaultDateIntervals.TODAY, "Today"));
        intervalSelectItems.add(new SelectItem(DefaultDateIntervals.YESTERDAY, "Yesterday"));
        intervalSelectItems.add(new SelectItem(DefaultDateIntervals.THIS_WEEK, "This week"));
        intervalSelectItems.add(new SelectItem(DefaultDateIntervals.LAST_WEEK, "last week"));
        intervalSelectItems.add(new SelectItem(DefaultDateIntervals.THIS_MONTH, "This month"));
        intervalSelectItems.add(new SelectItem(DefaultDateIntervals.LAST_MONTH, "Last month"));
        intervalSelectItems.add(new SelectItem(DefaultDateIntervals.LAST_7_DAYS, "Last 7 days"));
        intervalSelectItems.add(new SelectItem(DefaultDateIntervals.LAST_30_DAYS, "Last 30 days"));
    }

    public DefaultDateIntervals getSelectedDefaultDateInterval() {
        return selectedDefaultDateInterval;
    }

    public void setSelectedDefaultDateInterval(DefaultDateIntervals selectedDefaultDateInterval) {
        this.selectedDefaultDateInterval = selectedDefaultDateInterval;
    }

    public void updateDefaultInterval() {
        if (selectedDefaultDateInterval != null) {
            this.getGraphReport().getFilter().setInterval(selectedDefaultDateInterval);
        }
    }

    public List<SelectItem> getIntervalSelectItems() {
        return intervalSelectItems;
    }

    @PostConstruct
    public void init() {
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.set(2016, 1, 1, 0, 0);
        Calendar cal2 = Calendar.getInstance();
        Interval totalInterval = new Interval(cal.getTime().getTime(), cal2.getTime().getTime());
        
        Integer userId = userBean.getActiveUser().getId();
        UserModel model = UserService.getByOldId(userId);
        Integer accountId = UserService.getAccountId(model);
        accountSites = SiteService.getAccountSites(accountId);
        siteFilter = new ArrayList(CollectionUtils.collect(accountSites, new Transformer() {
            @Override
            public Integer transform(Object o) {
                return Integer.valueOf(((SiteModel)o).getV1Id());
            }
        }));
        
        currencies = CurrencyService.getCurrentRates();
        
        Account accountModel = Account.findById(accountId);
        selectedCurrency = getCurrencyById(accountModel.getInteger("currency_id"));
        
        InventoryDataReportFilter thisMonthFilter = new InventoryDataReportFilter(
            siteFilter, 
            DefaultDateIntervals.getThisMonth()
        );
        
        InventoryDataReportFilter lastMonthFilter = new InventoryDataReportFilter(
            siteFilter, 
            DefaultDateIntervals.getLastMonth()
        );
         
        InventoryDataReportFilter totalFilter = new InventoryDataReportFilter(
            siteFilter, 
            totalInterval
        ); 
        
        InventoryDataReportFilter last30DaysFilter = new InventoryDataReportFilter(
            siteFilter, 
            DefaultDateIntervals.getLast30Days()
        ); 
        last30DaysFilter.setGroupBySite(false);
        
        thisMonthReport = new InventoryDataReport(thisMonthFilter);
        lastMonthReport = new InventoryDataReport(lastMonthFilter);
        totalReport = new InventoryDataReport(totalFilter);
        graphReport = new InventoryDataReport(last30DaysFilter);
        applyCurrencyRate(selectedCurrency.getRate());
    }

    private void applyCurrencyRate(float rate) {
        thisMonthReport.applyCurrencyRate(rate);
        lastMonthReport.applyCurrencyRate(rate);
        totalReport.applyCurrencyRate(rate);
        graphReport.applyCurrencyRate(rate);
        buildRevenueGraphModel();
    }
    
    public InventoryDataReport getLastMonthReport() {
        return lastMonthReport;
    }

    public InventoryDataReport getGraphReport() {
        return graphReport;
    }

    public InventoryDataReport getThisMonthReport() {
        return thisMonthReport;
    }
    
    
    public InventoryDataReport getTotalReport() {
        return totalReport;
    }

    public void setTotalReport(InventoryDataReport totalReport) {
        this.totalReport = totalReport;
    }

    public String getGraphReportPrettyFilterInterval() {

        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM d, yyyy", userBean.getLocale());
        StringBuilder prettyInterval = new StringBuilder();
        prettyInterval.append(dateFormat.format(this.graphReport.getFilter().getFrom())).append(" - ").append(
                dateFormat.format(this.graphReport.getFilter().getTo())
        );
        return prettyInterval.toString();
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public LineChartModel getRevenueGraphModel() {
        return revenueGraphModel;
    }

    public void reloadGraph() {
        graphReport.loadData();
        graphReport.applyCurrencyRate(selectedCurrency.getRate());
        buildRevenueGraphModel();
    }

    public void buildRevenueGraphModel() {
        Logger.getLogger(getClass()).info("REloading revenueGraphModel ");
        
        if (graphReport.getData().size() == 0) {
            revenueGraphModel = null;
            return;
        }

        revenueGraphModel = new LineChartModel();

        LineChartSeries revenueSeries = new LineChartSeries();
        revenueSeries.setLabel("Revenue");
        LineChartSeries eCPMSeries = new LineChartSeries();
        eCPMSeries.setShowMarker(false);

        eCPMSeries.setXaxis(null);
        eCPMSeries.setLabel("eCPM");
        eCPMSeries.setYaxis(AxisType.Y2);
        eCPMSeries.setShowMarker(true);

        /* LineChartSeries eCPMSeries = new LineChartSeries();
         eCPMSeries.setLabel("eCPM");
         eCPMSeries.setShowMarker(false);
         eCPMSeries.setYaxis(AxisType.Y3);*/
        revenueGraphModel.setShowDatatip(true);
        revenueGraphModel.setDatatipFormat("<span style=\\\"display:none;\\\">%d</span><span>%.2f</span>");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        int days = 0;

        //  int daysInInterval = Days.daysIn(graphReport.getInventoryDataReportFilter().getInterval()).getDays();
        String startDate = dateFormat.format(graphReport.getFilter().getInterval().getStart().minusDays(1).toDate());
        String endDate = dateFormat.format(graphReport.getFilter().getInterval().getEnd().plusDays(1).toDate());

        for (InventoryData entry : graphReport.getData()) {
            revenueSeries.set(dateFormat.format(entry.getDate()), entry.getRevenue() * userBean.getExchangeRate());
            eCPMSeries.set(dateFormat.format(entry.getDate()), entry.getECPM() * userBean.getExchangeRate());
            days++;
        }

        revenueGraphModel.setStacked(true);
        revenueGraphModel.setDatatipFormat("%2$s");
        revenueGraphModel.setSeriesColors("7DCF68, F79646");
        //revenueGraphModel.setSeriesColors("8CC63E, C32032");

        revenueGraphModel.addSeries(revenueSeries);
        // revenueGraphModel.addSeries(dummy);
        revenueGraphModel.addSeries(eCPMSeries);

        //revenueGraphModel.setTitle("Revenue & eCPM");
        eCPMSeries.setDisableStack(true);

        DateAxis axis = new DateAxis("");

        axis.setTickAngle(-70);
        axis.setMin(startDate);
        axis.setMax(endDate);
        axis.setTickFormat("%#d, %b");
        if (days > 15) {
            axis.setTickCount(15);
        } else {
            axis.setTickCount(days + 1);
        }
        revenueGraphModel.getAxes().put(AxisType.X, axis);

        Axis yAxis = revenueGraphModel.getAxis(AxisType.Y);
        yAxis.setLabel("Revenue");
        yAxis.setTickFormat("%#.1f");
        Axis y2Axis = new LinearAxis("eCPM");
        y2Axis.setTickFormat("%#.1f");

        revenueGraphModel.setLegendPosition("s");
        revenueGraphModel.setLegendCols(3);
        revenueGraphModel.setLegendPlacement(LegendPlacement.OUTSIDEGRID);
        revenueGraphModel.getAxes().put(AxisType.Y2, y2Axis);
    }
    
    private void setDefaultCurrency() {
        System.out.println("Setting default currency");
        for (CurrencyListItemModel m : currencies) {
            if (m.getId() == 1) {
                selectedCurrency = m;
                break;
            }
        }
    }

    public List<CurrencyListItemModel> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<CurrencyListItemModel> currencies) {
        this.currencies = currencies;
    }

    public CurrencyListItemModel getSelectedCurrency() {
        return selectedCurrency;
    }

    public void setSelectedCurrency(CurrencyListItemModel aCurrency) {
        System.out.println(this.selectedCurrency.getCode() + "->" + aCurrency.getCode());
        if (!this.selectedCurrency.equals(aCurrency) && this.selectedCurrency != null) {
            //reset old rate
            applyCurrencyRate(1.0f/this.selectedCurrency.getRate());
            //set new rate
            applyCurrencyRate(aCurrency.getRate());
        }
        this.selectedCurrency = aCurrency;
        RequestContext context = RequestContext.getCurrentInstance();
        context.update("form");
        System.out.println("running setter");
    }
    
    public void selectedCurrencyDidChange() {
        //RequestContext context = RequestContext.getCurrentInstance();
        //context.update("form");
    }
    
    private CurrencyListItemModel getCurrencyById(int id) {
        for (CurrencyListItemModel m : currencies) {
            if (m.getId() == id) {
                return m;
            }
        }
        return null;
    }
}
