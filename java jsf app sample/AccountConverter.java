package com.mgm.converter;

/**
 *
 * @author alexrodin
 */

import com.mgm.core.data.model.AccountModel;
import com.mgm.core.service.AccountService;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
 
@FacesConverter("accountConverter")
public class AccountConverter implements Converter {
 
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && value.trim().length() > 0) {
            try {
                int id = Integer.parseInt(value);
                return AccountService.getById(id);
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        }
        else {
            return null;
        }
    }
 
    
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
         if (object == null || object.equals("")) {
            return "";
        } else {
            return String.valueOf(((AccountModel) object).getId());
        }
    }   
}