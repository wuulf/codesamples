package com.mgm.bean.view;

import com.mgm.core.data.model.AccountModel;
import com.mgm.core.data.model.SiteInteractiveModel;
import com.mgm.core.data.model.SiteModel;
import com.mgm.core.service.AccountService;
import com.mgm.core.service.SiteService;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author alexrodin
 */
@ManagedBean
@ViewScoped
public class SiteListView {

    private String defaultSiteFilter = "Live";
    private List<SiteInteractiveModel> sites;
    private List<AccountModel> accounts;
    /**
     * Creates a new instance of SiteListView
     */
    public SiteListView() {
        
    }
    
    @PostConstruct
    public void init() {
        sites = SiteService.getSiteInteractiveList();
        accounts = AccountService.getAll();
        
        sites.stream().forEach((site) -> {
            site.setAccount(getSiteAccount(site));
        });
    }

    private AccountModel getSiteAccount(SiteInteractiveModel site) {
        for (AccountModel account : accounts) {
            if (account.getId() == site.getAccountId()) {
                return account;
            }
        }
        return null;
    }
    
    public List<SiteInteractiveModel> getSites() {
        return sites;
    }

    public void setSites(List<SiteInteractiveModel> sites) {
        this.sites = sites;
    }
    
    public List<AccountModel> completeAccount(String query) {
        List<AccountModel> accountsFiltered = new ArrayList();
        for (int i = 0; i < accounts.size(); i++) {
            AccountModel account = accounts.get(i);
            if(account.getAccountName().toLowerCase().contains(query)) {
                accountsFiltered.add(account);
            }
        }
        return accountsFiltered;
    }
    
    public void onRowEdit(RowEditEvent event) {
        SiteInteractiveModel siteModel = (SiteInteractiveModel)(event.getObject());
        FacesMessage msg = new FacesMessage("Site Edited", siteModel.getDomainName());
        if (siteModel.getAccount() != null) {
            siteModel.setAccountId(siteModel.getAccount().getId());
            siteModel.setAccountName(siteModel.getAccount().getAccountName());
            System.out.println("" + siteModel.getId() + ":" + siteModel.getAccount().getId());
            SiteService.bindSiteToAccount(siteModel);
        }
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
     
    public void onRowCancel(RowEditEvent event) {
        SiteInteractiveModel siteModel = (SiteInteractiveModel)(event.getObject());
        FacesMessage msg = new FacesMessage("Edit Cancelled", siteModel.getDomainName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public void addToNewSites(SiteInteractiveModel site) {
        SiteModel model = new SiteModel();
        model.setDomainName(site.getDomainName());
        model.setV1Id(site.getOldId());
        SiteService.insert(model);
        FacesMessage msg = new FacesMessage("Site Added", site.getDomainName());
        FacesContext.getCurrentInstance().addMessage(null, msg);
        site.setId(model.getId());
    }
    
    public String isDefaultSiteFilter() {
        return defaultSiteFilter;
    }

    public void setDefaultSiteFilter(String defaultSiteFilter) {
        this.defaultSiteFilter = defaultSiteFilter;
    }

    public List<AccountModel> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountModel> accounts) {
        this.accounts = accounts;
    }
}
