/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mgm.converter;

/**
 *
 * @author alexrodin
 */

import com.mgm.core.data.model.campaign.TagOwnedModel;
import com.mgm.core.service.TagOwnedService;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
 
@FacesConverter("tagOwnedConverter")
public class TagOwnedConverter implements Converter {
 
    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
        if(value != null && value.trim().length() > 0) {
            try {
                int id = Integer.parseInt(value);
                return TagOwnedService.getById(id);
            } catch(NumberFormatException e) {
                throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid theme."));
            }
        }
        else {
            return null;
        }
    }
 
    
    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object object) {
         if (object == null || object.equals("")) {
            return "";
        } else {
            return String.valueOf(((TagOwnedModel) object).getId());
        }
    }   
}
