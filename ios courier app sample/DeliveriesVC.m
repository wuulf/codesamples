//
//  DeliveriesVC.m
//  BringoCDS
//
//  Created by Alex Rodin on 31.07.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

#import "DeliveriesVC.h"
#import "DeliveryProposalsVC.h"
#import "DeliveryMapVC.h"
#import "DeliveryStore.h"
#import "ServerAPI.h"

@interface DeliveriesVC ()

@property (nonatomic) CGRect contentFrame;
@property (nonatomic, weak) UIView* currentContentView;
@property (nonatomic) BOOL listMode;

@end

@implementation DeliveriesVC

@synthesize contentFrame = _contentFrame;
@synthesize currentContentView = _currentContentView;
@synthesize listMode = _listMode;
@synthesize progressView = _progressView;

- (id)init
{
    self = [super init];
    if (self) {
         
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect b = self.view.frame;
    self.contentFrame = CGRectMake(0.0, 86.0, b.size.width, b.size.height - 86.0);
    [self.progressView setHidesWhenStopped:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self showDeliveryList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) displayContentController: (UIViewController*) content;
{
    [self addChildViewController:content];                
    content.view.frame = self.contentFrame;
    [self.view addSubview:self.currentContentView];
    [content didMoveToParentViewController:self];
}

- (IBAction)menuButtonDidPress:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)showDeliveryList
{
    DeliveryProposalsVC *dpvc = [[DeliveryProposalsVC alloc] init];
    self.currentContentView = dpvc.tableView;
    self.listMode = YES;
    [self displayContentController:dpvc];
}

-(void)showDeliveryMap
{
    DeliveryMapVC *dmvc = [[DeliveryMapVC alloc] init];
    self.currentContentView = dmvc.view;
    self.listMode = NO;
    [self displayContentController:dmvc];
}

-(void)showActiveDelivery
{
    [self performSegueWithIdentifier:@"FromProposalsToActive" sender:self];
}

- (IBAction)listButtonDidPress:(id)sender
{
    [self showDeliveryList];
}

- (IBAction)mapButtonDidPress:(id)sender
{
    [self showDeliveryMap];
}

- (IBAction)refreshButtonDidPress:(id)sender
{
    
    if (self.listMode) {
        [self.progressView startAnimating];
        DeliveryStore* store = [DeliveryStore sharedStore];
        [store loadMyDeliveries:^(void) {
            if ([store deliveryStarted]) {
                [store loadActiveDelivery:^(void){
                    [self.progressView stopAnimating];
                    [(UITableView *)self.currentContentView reloadData];
                }];
            } else {
                [store loadDeliveries:^(void) {
                    [self.progressView stopAnimating];
                    [(UITableView *)self.currentContentView reloadData];
                    
                }];
            }
        }];
    }
}

@end
