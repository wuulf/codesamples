//
//  DeliveryStore.m
//  BringoCDS
//
//  Created by Alex Rodin on 30.07.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

#import "DeliveryStore.h"
#import "DeliveryModel.h"
#import "ServerAPI.h"
#import "Definitions.h"
#import "Utils.h"
#import "AddressModel.h"

@implementation DeliveryStore

+(DeliveryStore *)sharedStore
{
    static DeliveryStore *_sharedStore = nil;
    if (!_sharedStore) {
        _sharedStore = [[super allocWithZone:nil] init];
    }
    return _sharedStore;
}

- (id)init
{
    self = [super init];
    if (self) {
        _allItems = [[NSMutableArray alloc] init];
        _acceptedDeliveries = [[NSMutableArray alloc] init];
    }
    return self;
}

+ (id)allocWithZone:(NSZone *)zone
{
    return [self sharedStore];
}

- (NSArray *)allItems
{
    return _allItems;
}

- (NSArray *)acceptedDeliveries
{
    return _acceptedDeliveries;
}

-(DeliveryModel *)currentDelivery
{
    return _currentDelivery;
}

-(BOOL)deliveryStarted
{
    return _deliveryStarted;
}

-(void)setDeliveryStarted:(BOOL)started
{
    _deliveryStarted = started;
}

-(void)setCurrentDelivery:(DeliveryModel *)dm
{
    _currentDelivery = dm;
}

-(DeliveryModel *)createItem
{
    DeliveryModel* it = [DeliveryModel randomItem];
    
    [_allItems addObject:it];
    
    return it;
}

-(NSNumber *)activeDeliveryId
{
    return _activeDeliveryId;
}

-(void)setActiveDeliveryId:(NSNumber *)dlvId
{
    _activeDeliveryId = dlvId;
}

-(NSNumber *)activeDeliveryStatus
{
    return _activeDeliveryStatus;
}

-(void)setActiveDeliveryStatus:(NSNumber *)dlvStatus
{
    _activeDeliveryStatus = dlvStatus;
}

-(void)addItem:(DeliveryModel *)deliveryModel
{
    [_allItems addObject:deliveryModel];
}

-(void)addAcceptedDelivery:(NSNumber *)dlvId
{
    if (![_acceptedDeliveries containsObject:dlvId]) {
        [_acceptedDeliveries addObject:dlvId];
    }
    
}

-(void)removeItems
{
    [_allItems removeAllObjects];
}

-(void)loadDeliveries:(void (^)(void))responseCallback
{
    [[ServerAPI sharedAPI] requestDeliveryList:^(NSDictionary *res) {
        NSString* errorString = [NSString stringWithFormat:@"%@", [res objectForKey:@"error"]];
        if ([errorString isEqualToString:@"0"]) {
            NSMutableArray *responseData = [res valueForKey:@"data"];
            [self removeItems];
            for (NSDictionary* dlvData in responseData) {
                DeliveryModel *dm = [DeliveryModel modelWithDictionary:dlvData];
                [self addItem:dm];
                if ([_acceptedDeliveries containsObject:dm.dlvId]) {
                    [dm setDlvStatus:[NSNumber numberWithInt:8]];
                }
            }
            responseCallback();
        } else {
            NSString* errorMsg = (NSString *)[res objectForKey:@"msg"];
            [Utils showPopUpMessage:errorMsg withTitle:@"Печалька:("];
        }
    }];
}

-(void)loadMyDeliveries:(void (^)(void))responseCallback
{
    [[ServerAPI sharedAPI] requestMyDeliveries:^(NSDictionary *res) {
        NSString* errorString = [NSString stringWithFormat:@"%@", [res objectForKey:@"error"]];
        if ([errorString isEqualToString:@"0"]) {
            NSLog(@"received: %@", res);
            NSMutableArray *responseData = [res valueForKey:@"data"];
            for (NSDictionary* dlvData in responseData) {
                NSLog(@"my delivery: %@", dlvData);
                NSNumber *isMine = [dlvData objectForKey:@"is_mine"];
                NSNumber *dlvId = [dlvData objectForKey:@"dlv_id"];
                if ([isMine integerValue] == 1) {
                    if (![self deliveryStarted]) {
                        [self setDeliveryStarted:YES];
                        [self setActiveDeliveryId:[dlvData objectForKey:@"dlv_id"]];
                        [Utils showPopUpMessage:[NSString stringWithFormat:@"Вы должны выполнить доставку номер %@", dlvId] withTitle:@"Вас выбрали!"];
                    }
                } else {
                    [_acceptedDeliveries removeObject:dlvId];
                }
            }
            responseCallback();
        } else {
            NSString* errorMsg = (NSString *)[res objectForKey:@"msg"];
            [Utils showPopUpMessage:errorMsg withTitle:@"Печалька:("];
        }
    }];

}

-(void)loadActiveDelivery:(void (^)(void))responseCallback
{
    [[ServerAPI sharedAPI] requestActiveDelivery:^(NSDictionary *res) {
        NSString* errorString = [NSString stringWithFormat:@"%@", [res objectForKey:@"error"]];
        if ([errorString isEqualToString:@"0"]) {
            NSMutableArray *responseData = [res valueForKey:@"data"];
            if ([responseData count] > 0) {
                [self removeItems];                
                DeliveryModel *dm = [DeliveryModel modelWithDictionary:[responseData objectAtIndex:0]];
                if ([dm.dlvClosed integerValue] == 1) {
                    [self setCurrentDelivery:nil];
                    [self setActiveDeliveryId:nil];
                    [self setDeliveryStarted:NO];
                    [[ServerAPI sharedAPI] requestSetCourierReady:^(NSDictionary *result) {
                        NSLog(@"courier is ready");
                    }];
                } else {
                    [self addItem:dm];
                    [self setCurrentDelivery:dm];
                }
            }
            responseCallback();
        } else {
            NSString* errorMsg = (NSString *)[res objectForKey:@"msg"];
            [Utils showPopUpMessage:errorMsg withTitle:@"Печалька:("];
        }
    }];
}

@end
