//
//  DeliveriesVC.h
//  BringoCDS
//
//  Created by Alex Rodin on 31.07.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeliveriesVC : UIViewController

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressView;

-(void)showActiveDelivery;

@end
