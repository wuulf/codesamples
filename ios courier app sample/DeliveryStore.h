//
//  DeliveryStore.h
//  BringoCDS
//
//  Created by Alex Rodin on 30.07.13.
//  Copyright (c) 2013 Alex Rodin. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DeliveryModel;

@interface DeliveryStore : NSObject
{
    NSMutableArray *_allItems;
    NSMutableArray *_acceptedDeliveries;
    DeliveryModel *_currentDelivery;
    BOOL _deliveryStarted;
    NSNumber *_activeDeliveryId;
    NSNumber *_activeDeliveryStatus;
}



+(DeliveryStore *)sharedStore;

-(NSArray *)allItems;

-(DeliveryModel *)createItem;

-(void)removeItems;

-(void)addItem:(DeliveryModel *)deliveryModel;

-(void)addAcceptedDelivery:(NSNumber *)dlvId;

-(NSArray *)acceptedDeliveries;



-(void)loadDeliveries:(void (^)(void))responseCallback;

-(void)loadMyDeliveries:(void (^)(void))responseCallback;

-(void)loadActiveDelivery:(void (^)(void))responseCallback;



-(NSNumber *)activeDeliveryId;

-(void)setActiveDeliveryId:(NSNumber *)dlvId;

-(NSNumber *)activeDeliveryStatus;

-(void)setActiveDeliveryStatus:(NSNumber *)dlvStatus;



-(DeliveryModel *)currentDelivery;

-(BOOL)deliveryStarted;

-(void)setCurrentDelivery:(DeliveryModel *)dm;

@end
