package com.rodinalex.andgames.r12.map;


public class R12_Door {
/*	
	int id;
	public float width;
	public float height;
	public float spriteWidth;
	public float spriteHeight;
	public float boundingRadius;
	
	public String stateOpen;
	public String stateClosing;
	public String stateClosed;
	public String stateOpening;
	
	public int orientation;
	
	public Vector2f position;
	
	public boolean closed = true;
	
	public boolean toBeRendered = false;
	
	private R12_World world;
	public Animator animator;
	public Puppet puppet;
	
	public R12_Door(R12_World world) {
		this.world = world;
		boundingRadius = 0.5f;
		position = new Vector2f(0.0f, 0.0f);
	};
	
	public void loadFromJson(JSONObject config) {
		try {
			position.x = (float)config.getInt("x") / (float)world.map.kTileWidth;
			position.y = (float)(world.map.sizeY * world.map.kTileHeight - config.getInt("y")) / (float)world.map.kTileHeight;
			orientation = config.getInt("name");
			if (orientation == 1) {
				spriteWidth = 12.0f/32.0f;
				spriteHeight = 48.0f/32.0f;
				width = 0.5f;
				height = 1.0f;
				changeState("door_v_closed");
			} else {
				spriteWidth = 48.0f/32.0f;
				spriteHeight = 12.0f/32.0f;
				width = 1.0f;
				height = 0.5f;
				changeState("door_h_closed");
			}
		} catch (Exception e) {
			throw new RuntimeException("Invalid wall config");
		}
	}
	
	public void openDoor() {
		if (currentState.name.equals("door_v_opened") || currentState.name.equals("door_h_opened")) {
			return;
		} else {
			changeState(orientation == 0 ? "door_h_opening" : "door_v_opening");
		}
	}
	
	public void closeDoor() {
		if (currentState.name.equals("door_v_closed") || currentState.name.equals("door_h_closed")) {
			return;
		} else {
			changeState(orientation == 0 ? "door_h_closing" : "door_v_closing");
		}
	}
	
	public void changeState(String stateName) {
		if (currentState == null || !stateName.equals(currentState.name)) {
			currentState = world.objectStateManager.getState(stateName);
			timeInState = 0.0f;
			currentFrameIndex = 0;
			if (currentState.name.equals("door_v_opened") || currentState.name.equals("door_h_opened")) {
				closed = false;
			} else {
				closed = true;
			}
		}
	}
	
	public void update(float deltaTime) {
		
		int len = world.bots.size();
		
		boolean blocked = false;
		
		for (int i = 0; i < len; i++) {
			R12_Bot bot = world.bots.get(i);
			if (world.entitiesCollideCircles(this, bot)) {
				blocked = true;
			}
		}
		
		if (blocked) {
			openDoor();
		} else {
			closeDoor();
		}
		
		if (currentState != null) {
			
			timeInState += deltaTime;
			currentFrameIndex = currentState.getFrameIndex(timeInState, currentState.looping);
			
			//If state ended
			if (currentState.looping == 0 && 
				currentFrameIndex > currentState.getNumberOfFrames() - 1
			) {
				setNextState(currentState.name);
			}
		}
	}
	
	//TODO refactor
	public void setNextState(String stateName) {
		
		if (stateName.equals("door_v_opening")) {
			changeState("door_v_opened");
			return;
		}
		
		if (stateName.equals("door_v_closing")) {
			changeState("door_v_closed");
			return;
		}
		
		if (stateName.equals("door_h_opening")) {
			changeState("door_h_opened");
			return;
		}
		
		if (stateName.equals("door_h_closing")) {
			changeState("door_h_closed");
			return;
		}
	}
	
	@Override
	public void render(SpriteBatcher batcher) {
		if (currentState != null && toBeRendered) {
			batcher.drawSprite(
				position.x,
				position.y,
				spriteWidth,
				spriteHeight,
				world.objectStateManager.getStateFrame(currentState.getFrameNumber(currentFrameIndex))
			);
		}
	}

	@Override
	public void getPosition(Vector2f vec) {
		vec.set(position);
	}

	@Override
	public float getWidth() {
		return width;
	}

	@Override
	public float getHeight() {
		return height;
	}

	@Override
	public float getSpriteWidth() {
		return spriteWidth;
	}

	@Override
	public float getSpriteHeight() {
		return spriteHeight;
	}

	@Override
	public boolean handleMessage(Telegram msg) {
		return false;
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public void setID(int id) {
		this.id = id;
	}

	@Override
	public float getBoundingRadius() {
		return boundingRadius;
	}

	@Override
	public void setAnimator(Animator animator) {
		this.animator = animator;
	}

	@Override
	public Animator getAnimator() {
		return this.animator;
	}

	@Override
	public boolean isToBeRendered() {
		return toBeRendered;
	}

	@Override
	public Puppet getPuppet() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPuppet(Puppet puppet) {
		// TODO Auto-generated method stub
		
	};*/
}
