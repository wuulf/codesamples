package com.rodinalex.andgames.r11;

import javax.microedition.khronos.opengles.GL10;

import com.rodinalex.andgames.fw.gl.Vertices;
import com.rodinalex.andgames.fw.math.Vector2f;
import com.rodinalex.andgames.fw.phys.Firework;
import com.rodinalex.andgames.fw.phys.FireworkRule;
import com.rodinalex.andgames.fw.phys.PointMass2D;

public class R11_FireworkFactory {
	
	static final int kMaxFireworks = 128;
	Firework[] fireworks;
	int nextFirework;
	FireworkRule[] rules;
	Vertices sparkVertices;
	
	public R11_FireworkFactory(R11_World world) {
		
		sparkVertices = new Vertices(world.getGraphics(), 4, 6, false, false);
        sparkVertices.setVertices(new float[] {
			0.0f, 0.0f,
			3.0f, 0.0f,
			3.0f, 3.0f,
			0.0f, 3.0f
		}, 0, 8);
        sparkVertices.setIndices(new short[] {0, 1, 2, 2, 3, 0 }, 0, 6); 
		
		rules = new FireworkRule[9];
		
		//Spark rules
		rules[0] = new FireworkRule();
		rules[1] = new FireworkRule();
		rules[2] = new FireworkRule();
		//Smoke rules
		rules[3] = new FireworkRule();
		rules[4] = new FireworkRule();
		rules[5] = new FireworkRule();
		
		//Blood rules
		rules[6] = new FireworkRule();
		rules[7] = new FireworkRule();
		rules[8] = new FireworkRule();	
				
		rules[0].init(2);
		rules[0].setParams(
			0,
			0.05f, 
			0.05f, 
			new Vector2f(0, 0), 
			new Vector2f(0, 0),
			0.99f,
			new float[]{1.0f, 1.0f, 0.0f, 1.0f}
		);
		
		rules[0].payloads[0].set(1, 4);
		rules[0].payloads[1].set(2, 4);
		
		rules[1].init(0);
		rules[1].setParams(
			1,
			0.4f, 
			0.8f, 
			new Vector2f(-50, -25), 
			new Vector2f(0, 25),
			0.9f,
			new float[]{1.0f, 0.8f, 0.0f, 1.0f}
		);
		
		rules[2].init(0);
		rules[2].setParams(
			2,
			0.4f, 
			0.8f, 
			new Vector2f(0, -25 ), 
			new Vector2f(50, 25),
			0.9f,
			new float[]{1.0f, 0.8f, 0.0f, 1.0f}
		);
		
		rules[3].init(2);
		rules[3].setParams(
			3,
			0.001f, 
			0.001f, 
			new Vector2f(0, 0), 
			new Vector2f(0, 0),
			0.01f,
			new float[]{0.7f, 0.7f, 0.7f, 0.3f}
		);
		
		rules[3].payloads[0].set(4, 16);
		rules[3].payloads[1].set(5, 16);
		
		rules[4].init(0);
		rules[4].setParams(
			4,
			0.3f, 
			0.6f, 
			new Vector2f(-50, -25), 
			new Vector2f(0, 25),
			0.05f,
			new float[]{0.7f, 0.7f, 0.7f, 0.3f}
		);
		
		rules[5].init(0);
		rules[5].setParams(
			5,
			0.3f, 
			0.6f, 
			new Vector2f(0, -25 ), 
			new Vector2f(50, 25),
			0.05f,
			new float[]{0.7f, 0.7f, 0.7f, 0.3f}
		);
		
		rules[6].init(2);
		rules[6].setParams(
			6,
			0.05f, 
			0.05f, 
			new Vector2f(0, 0), 
			new Vector2f(0, 0),
			0.99f,
			new float[]{0.8f, 0.0f, 0.0f, 1.0f}
		);
		
		rules[6].payloads[0].set(7, 5);
		rules[6].payloads[1].set(8, 5);
		
		rules[7].init(0);
		rules[7].setParams(
			7,
			0.4f, 
			0.8f, 
			new Vector2f(-50, -25), 
			new Vector2f(0, 25),
			0.9f,
			new float[]{0.8f, 0.0f, 0.0f, 1.0f}
		);
		
		rules[8].init(0);
		rules[8].setParams(
			8,
			0.4f, 
			0.8f, 
			new Vector2f(0, -25 ), 
			new Vector2f(50, 25),
			0.9f,
			new float[]{0.8f, 0.0f, 0.0f, 1.0f}
		);
		
		fireworks = new Firework[kMaxFireworks];
		nextFirework = 0;
	}
	
	public void update(float deltaTime) {
		 for (int i = 0; i < fireworks.length; i++) {
			Firework fw = fireworks[i];
			if (fw != null && fw.type != -1) {
				if (fw.update(deltaTime)) {
					FireworkRule fr = rules[fw.type];
					fw.type = -1;
					for (int j = 0; j < fr.payloadCount; j++) {
						FireworkRule.Payload p = fr.payloads[j];
						createExplosion(p.type, p.count, fw);
					}
				}
			}
		}
	}
	
	public void render(GL10 gl) {
		Vector2f pos;
		FireworkRule rule;
        for (int i = 0; i < fireworks.length;i++) {
        	Firework fw = fireworks[i];
        	if (fw != null && fw.type != -1) {
        		pos = fw.getPosition();
        		rule = rules[fw.type];
        		gl.glPushMatrix();
                gl.glTranslatef(pos.x, pos.y, 0);
                gl.glColor4f(rule.red, rule.green, rule.blue, rule.alpha);
                sparkVertices.bind();
                sparkVertices.draw(GL10.GL_TRIANGLES, 0, 6);
                sparkVertices.unbind();
                gl.glPopMatrix();
        	}
        }
	}
	
	public void createExplosion(int type, Vector2f position) {
		
		if (fireworks[nextFirework] == null) {
			fireworks[nextFirework] = new Firework(); 
		}
		
		Firework fw = fireworks[nextFirework];
		FireworkRule fr = rules[type];
		fr.createFirework(fw, position);
		
		if (nextFirework >= kMaxFireworks - 1) {
			nextFirework = 0;
		} else {
			nextFirework++;
		}
	}
	
	public void createExplosion(int type, PointMass2D parent) {
		
		if (fireworks[nextFirework] == null) {
			fireworks[nextFirework] = new Firework(); 
		}
		
		Firework fw = fireworks[nextFirework];
		fw.type = type;
		
		FireworkRule fr = rules[type];
		fr.createFirework(fw, parent);
		
		if (nextFirework >= kMaxFireworks - 1) {
			nextFirework = 0;
		} else {
			nextFirework++;
		}
	}
	
	public void createExplosion(int type, int numberOfShards, Firework parent) {
		for (int i = 0; i < numberOfShards; i++) {
			createExplosion(type, parent);
		}
	}
}
