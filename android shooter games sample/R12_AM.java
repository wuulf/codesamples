package com.rodinalex.andgames.r12;

import com.rodinalex.andgames.fw.AndSound;
import com.rodinalex.andgames.fw.GLGame;
import com.rodinalex.andgames.fw.game.AssetMaster;
import com.rodinalex.andgames.fw.gl.FontACF;

public class R12_AM extends AssetMaster {
	
	//TODO - refactor
	public static final String kPlayerTexture = "tex/Player_Bot.png";
	public static final String kMenuTexture = "tex/menu_hud_items.png";
	public static final String kTestTS = "tex/Concrete_01_all.png";
	public static final String kBotTexture = "tex/Texture_Bot.png";
	public static final String kItemsTexture = "tex/Items_00.png";
	public static final String kFontTexture = "font/krungthep_red.png";
	public static final String kFontDescriptor = "font/krungthep_red.fnt";
	public static final String kProjectileTexture = "tex/projectiles.png";
	
	public static FontACF font;
	
	public static AndSound laserShardSound;
	
	public R12_AM(GLGame game) {
		super(game);
	}
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
	}

	@Override
	public void load() {
		loadTextureFromFile(kPlayerTexture);
		loadTextureFromFile(kMenuTexture);
		loadTextureFromFile(kTestTS);
		loadTextureFromFile(kBotTexture);
		loadTextureFromFile(kItemsTexture);
		loadTextureFromFile(kFontTexture);
		loadTextureFromFile(kProjectileTexture);
		font = FontACF.loadFromFile(game.getFileIO(), kFontDescriptor, getTexture(kFontTexture));
		laserShardSound = game.getAudio().soundWithFileName("sound/laser_shot.ogg");
	}

	@Override
	public void reload() {
		getTexture(kMenuTexture).reload();
		getTexture(kTestTS).reload();
		getTexture(kBotTexture).reload();
		getTexture(kItemsTexture).reload();
		getTexture(kFontTexture).reload();
		getTexture(kProjectileTexture).reload();
		getTexture(kPlayerTexture).reload();
	}
	
	public void playSound(AndSound sound) {
		sound.playWithVolume(1);
	}
	
}
