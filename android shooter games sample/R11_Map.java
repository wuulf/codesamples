package com.rodinalex.andgames.r11;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.gl.Vertices;
import com.rodinalex.andgames.fw.math.MathUtil;
import com.rodinalex.andgames.fw.math.Vector2f;

public class R11_Map {
	
	public ArrayList<Wall2D> walls;
	
	public ArrayList<Vector2f> spawnPoints;
	
	public int sizeX = 0;
	
	public int sizeY = 0;
	
	public R11_World world;
	
	Vertices vertices;
	
	public R11_Map(R11_World world) {
		this.sizeX = 480;
		this.sizeY = 320;
		this.world = world;
		walls = new ArrayList<Wall2D>();
		spawnPoints = new ArrayList<Vector2f>();
		
		vertices = new Vertices(world.getGraphics(), 4, 6, false, false);
		
		vertices.setVertices(new float[] {
			0, 0,
			this.sizeX, 0,
			this.sizeX, this.sizeY,
			0, this.sizeY
		}, 0, 8);
			
		vertices.setIndices(new short[] {0, 1, 2, 2, 3, 0 }, 0, 6);
	}
	
	public void update(float deltaTime) {
		
	}
	
	void renderBackground() {
		world.getGraphics().getGL().glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		vertices.bind();
        vertices.draw(GL10.GL_TRIANGLES, 0, 6);
        vertices.unbind();
	}
	
	public void render() {
		renderBackground();
		renderSpawnPoints();
		for (int i = 0; i < walls.size(); i++) {
			walls.get(i).render();
		}
	}
	
	public void renderSpawnPoints() {
		GL10 gl = world.getGL();
		gl.glPushMatrix();
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glColor4f(1, 1, 1, 1);
		
		SpriteBatcher batcher = world.screen.batcher;
		
		batcher.beginBatch(world.screen.texture);
		for (int i = 0; i < spawnPoints.size(); i++) {
			batcher.drawSprite(
				spawnPoints.get(i).x, 
				spawnPoints.get(i).y, 
				24, 24, world.screen.spawnRegion
			);
		}
		batcher.endBatch();
		
		gl.glDisable(GL10.GL_TEXTURE_2D);
		gl.glPopMatrix();
	}
	
	void clear() {
		walls.clear();
	}
	
	public boolean loadMap(String fileName) {
		
		boolean result = true;
		
		clear();
		
		JSONObject mapConfig = world.game.getFileIO().getJsonFromFile(fileName);
		
		try {
			sizeX = mapConfig.getInt("sizeX");
			sizeY = mapConfig.getInt("sizeY");
			JSONArray wallsConfig = mapConfig.getJSONArray("walls");
			for (int i = 0; i < wallsConfig.length(); i++) {
				walls.add(Wall2D.wallWithJsonArray(world, wallsConfig.getJSONArray(i)));
			}
			JSONArray spawnPointsConfig = mapConfig.getJSONArray("spawnPoints");
			for (int i = 0; i < spawnPointsConfig.length(); i++) {
				JSONArray spj = spawnPointsConfig.getJSONArray(i);
				spawnPoints.add(new Vector2f(
					spj.getInt(0), 
					spj.getInt(1)
				));
			}
		} catch (Exception e) {
			result = false;
		} finally {
			if (!result) {
				clear();
			}
		}
		
		return result;
	}
	
	public Vector2f getRandomSpawnPoint() {
		int range = spawnPoints.size();
		return spawnPoints.get(MathUtil.randomInt(range));
	}
}
