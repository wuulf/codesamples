package com.rodinalex.andgames.r11.screen;

import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.rodinalex.andgames.fw.AndInput.TouchEvent;
import com.rodinalex.andgames.fw.collisions.CollisionDetector2D;
import com.rodinalex.andgames.fw.gl.Camera2D;
import com.rodinalex.andgames.fw.gl.GLScreen;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.gl.Texture;
import com.rodinalex.andgames.fw.gl.TextureRegion;
import com.rodinalex.andgames.fw.math.Vector2f;
import com.rodinalex.andgames.fw.ui.GLButtonBase;
import com.rodinalex.andgames.fw.ui.GLButtonBaseDelegate;
import com.rodinalex.andgames.r11.R11_Game;
import com.rodinalex.andgames.r11.R11_World;

public class Screen_Combat extends GLScreen implements GLButtonBaseDelegate {

	//public Camera2D guiCam;
	
	Hud_Combat hud;
	
	R11_World world;
	
	Vector2f touchPoint;
	
	public SpriteBatcher batcher;
	
	public Texture texture;
	
	public TextureRegion pistolRegion;
	
	public TextureRegion bulletRegion;
	
	public TextureRegion healthBarRegion;
	
	public TextureRegion botRegion;
	
	public TextureRegion wallSegmentRegion;
	
	public TextureRegion crosshairRegion;
	
	public TextureRegion ripRegion;
	
	public TextureRegion spawnRegion;
	
	public Screen_Combat(R11_Game game) {
		
		super(game);
		guiCam = new Camera2D(graphics, 480, 320);
		hud = new Hud_Combat(game, this);
		world = new R11_World(game, this);
		batcher =  new SpriteBatcher(graphics, 200);
		
		touchPoint = new Vector2f();
        touchDown = new Vector2f();
        touchUp = new Vector2f();
	}
	
	@Override
	public void update(float deltaTime) {
		
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
	    int len = touchEvents.size();
	    
	    for(int i = 0; i < len; i++) {
	        TouchEvent event = touchEvents.get(i);
	        touchPoint.set(event.x, event.y);
            guiCam.touchToWorld(touchPoint);
            
	        if (event.type == TouchEvent.kTouchDown) {
	        	tryHudItems(touchPoint, event.pointer, true);
        		continue;
	        }
	        
	        if (event.type == TouchEvent.kTouchUp) {
	        	tryHudItems(touchPoint, event.pointer, false);
	        }
	    }
	    
	    world.update(deltaTime);
		hud.update(deltaTime);
	}

	void tryHudItems(final Vector2f point, int pointer, boolean touchDown) {
		for (int i = 0; i < hud.items.size(); i++) {
			if (CollisionDetector2D.intersectAABBPoint(
				hud.items.get(i).getBox(), point) 
				|| hud.items.get(i).getPointer() == pointer
			) {
				hud.items.get(i).markTouched(touchDown, pointer);
			} 
		}
	}
	
	@Override
	public void render() {
		GL10 gl = graphics.getGL();     
		gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        guiCam.setViewportAndMatrices();
        
        gl.glEnable(GL10.GL_BLEND);
        
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);               
        
        world.render();
        
        gl.glLoadIdentity();
        hud.render();
        gl.glDisable(GL10.GL_BLEND);
	}
	
	@Override
	public void pause() {
		texture.release();
		world.release();
	}

	@Override
	public void resume() {
		texture = Texture.textureFromFile(game.getGLGraphics(), game.getFileIO(), "tex/R11_Texture_04_10_2014.png");
		pistolRegion = new TextureRegion(texture, 0, 0, 32, 32);
		bulletRegion = new TextureRegion(texture, 32, 0, 4, 4);
		botRegion = new TextureRegion(texture, 64, 0, 32, 32);
		healthBarRegion = new TextureRegion(texture, 32, 4, 32, 3);
		wallSegmentRegion = new TextureRegion(texture, 32, 7, 32, 12);
		crosshairRegion = new TextureRegion(texture, 32, 19, 10, 10);
		ripRegion = new TextureRegion(texture, 0, 32, 24, 24);
		spawnRegion = new TextureRegion(texture, 32, 32, 24, 24);
	}

	@Override
	public void release() {
		texture.release();
		world.release();
	}

	@Override
	public void GLButtonBase_buttonPressed(GLButtonBase button) {
		//turn left
		if (button.equals(hud.items.get(0))) {
			world.rotateLeft();
		}
		
		//move down
		if (button.equals(hud.items.get(1))) {
			world.moveDown();
		}
		
		//turn right
		if (button.equals(hud.items.get(2))) {
			world.rotateRight();
		}
		
		//move up
		if (button.equals(hud.items.get(3))) {
			world.moveUp();
		}
		
		//fire
		if (button.equals(hud.items.get(4))) {
			world.fire();
		}
	}

	@Override
	public void GLButtonBase_buttonReleased(GLButtonBase button) {
		//turn left
		if (button.equals(hud.items.get(0))) {
			//System.out.println("left release");
		}
		
		//move down
		if (button.equals(hud.items.get(1))) {
			//System.out.println("down release");
		}
		
		//turn right
		if (button.equals(hud.items.get(2))) {
			//System.out.println("right release");
		}
		
		//move up
		if (button.equals(hud.items.get(3))) {
			//System.out.println("up release");
		}
		
		//fire
		if (button.equals(hud.items.get(4))) {
			//System.out.println("fire release");
		}
	}
}
