package com.rodinalex.andgames.r11;

import com.rodinalex.andgames.fw.math.Vector2f;


public class R11_PlayerBot extends R11_Bot {

	public R11_PlayerBot(R11_World world) {
		super(world);
		steering.target = position;
	}
	
	@Override
	public void update(float deltaTime) {
		
		updateStatus(deltaTime);
		
		if (botStatus == kBotStatus_Alive) {
			this.addForce(this.steering.calculate());
			this.integrate(deltaTime);
		}
		weapon.update(deltaTime);
		movingState.update(deltaTime);
		shootingState.update(deltaTime);
	}
	
	@Override
	public void spawnAt(Vector2f position) {
		if (botStatus != kBotStatus_Spawing) {
			setDefaultStats();
			setSpawning();
			setPosition(position);
			steering.target = position;
			setMass(1.0f);
		}
	}
}
