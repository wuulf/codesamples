package com.rodinalex.andgames.r12.state;

import org.json.JSONObject;

import com.rodinalex.andgames.fw.fsm.Puppet;
import com.rodinalex.andgames.fw.fsm.PuppetState;
import com.rodinalex.andgames.fw.fsm.Puppeteer;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.messaging.Telegram;
import com.rodinalex.andgames.r12.map.Door;

public class State_Door_Opened extends PuppetState<Door> {
	
	public State_Door_Opened(JSONObject config, Puppeteer<Door> puppeteer) {
		super(config, puppeteer);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(Puppet<Door> puppet, SpriteBatcher batcher) {

	}

	@Override
	public void enter(Puppet<Door> puppet) {
		
	}

	@Override
	public void update(Puppet<Door> puppet, float deltaTime) {
		
	}

	@Override
	public void exit(Puppet<Door> puppet) {
		
	}

	@Override
	public boolean handleMessage(Puppet<Door> puppet, Telegram msg) {
		return false;
	}
}
