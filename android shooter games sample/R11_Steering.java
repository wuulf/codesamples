package com.rodinalex.andgames.r11;

import java.util.ArrayList;

import com.rodinalex.andgames.fw.collisions.CollisionDetector2D;
import com.rodinalex.andgames.fw.math.Vector2f;

public class R11_Steering {
	
	R11_Bot bot;
	R11_World world;
	Vector2f totalSteeringForce;
	public Vector2f target;
	Vector2f[] feelers;
	
	static final float kDeceleration = 0.2f;
	static final float kFeelerLength = 18.0f;
	
	static final float kWeightArrive = 1.0f;
	static final float kWeightWallAvoidance = 10.0f;
	static final float kWeigthSeparation = 10.0f;
	
	boolean seekOn = false;
	boolean arriveOn = true;
	boolean wallAvoidanceOn = true;
	boolean separationOn = true;
	
	public R11_Steering(R11_Bot bot, R11_World world) {
		this.bot = bot;
		this.world = world;
		totalSteeringForce = new Vector2f();
		feelers = new Vector2f[3];
		feelers[0] = new Vector2f();
		feelers[1] = new Vector2f();
		feelers[2] = new Vector2f();
	}
	
	public Vector2f calculate() {
		totalSteeringForce.zero();
		totalSteeringForce = calculatePrioritized();
		return totalSteeringForce;
	}
	
	public void setFeelers() {
		
		Vector2f botHeading = bot.getHeading();
		
		feelers[0].set(Vector2f.add(
			bot.getPosition(), 
			botHeading.clone().scale(bot.getSpeed())
		));
		
		feelers[1].set(
			Vector2f.add(
				bot.getPosition(), 
				Vector2f.add(
					botHeading, 
					botHeading.perp()
				).scale(kFeelerLength/2.0f)
			)
		);
		
		feelers[2].set(Vector2f.add(
				bot.getPosition(), 
				Vector2f.add(
					botHeading, 
					botHeading.perpReverse()
				).scale(kFeelerLength/2.0f)
			)
		);
	}
	
	boolean accumulateForce(Vector2f total, Vector2f force) {
		float magCurrent = total.len();
		float magLeft = bot.maxForce - magCurrent;
		if (magLeft <= 0.0f) return false;
		float magToAdd = force.len();
		
		if (magToAdd < magLeft) {
			total.add(force);
		} else {
			magToAdd = magLeft;
			total.add(force.clone().norm().scale(magToAdd));
		}
		return true;
	}
	
	Vector2f calculatePrioritized() {
		Vector2f force;
		
		if (wallAvoidanceOn) {
			force = wallAvoidance(world.map.walls).scale(kWeightWallAvoidance);
			if (!accumulateForce(totalSteeringForce, force)) return totalSteeringForce;
		}
		
		if (separationOn) {
			force = separation(world.bots).scale(kWeigthSeparation);
			if (!accumulateForce(totalSteeringForce, force)) {
				return totalSteeringForce;
			}
	    }
		
		if (seekOn) {
			force = seek(target);
			if (!accumulateForce(totalSteeringForce, force)) {
				return totalSteeringForce;
			}
		}
		
		if (arriveOn) {
			force = arrive(target).scale(kWeightArrive);
			if (!accumulateForce(totalSteeringForce, force)) {
				return totalSteeringForce;
			}
		}
		
		return totalSteeringForce;
	}
	
	public Vector2f seek(Vector2f vTarget) {
		Vector2f desiredVelocity = Vector2f.norm(
			Vector2f.sub(vTarget, bot.getPosition()))
			.scale(bot.maxSpeed);
		return Vector2f.sub(desiredVelocity, bot.getVelocity());
	}
	
	public Vector2f arrive(Vector2f vTarget) {
		
		Vector2f toTarget = Vector2f.sub(vTarget, bot.getPosition());
		
		float dist = toTarget.len();
		
		if (dist > 0) {
			float speed = dist / kDeceleration;
			
			speed = Math.min(speed, bot.maxSpeed);
			
			Vector2f desiredVelocity = toTarget.scale(speed/dist);
			
			return (Vector2f.sub(desiredVelocity, bot.getVelocity())); 
		}	
		
		toTarget.set(0,0);
		
		return toTarget;
	}
	
	public Vector2f separation(final ArrayList<R11_Bot> bots) {  
		Vector2f steeringForce = new Vector2f();
		float len = 0.0f;
		for (int i = 0; i < bots.size(); i++) {
			if( bots.get(i) != bot ) {
				Vector2f toAgent = Vector2f.sub(bot.getPosition(), bots.get(i).getPosition());
				len = toAgent.len();
				if (len >= 0.001f);
				//TODO - fix magic numbers - 20;
				steeringForce.add(toAgent.norm().scale(1/len));
			}
		}
		return steeringForce;
	}
	
	public Vector2f wallAvoidance(final ArrayList<Wall2D> walls) {
		setFeelers();
		float dist = 0.0f;
		float minDist = Float.MAX_VALUE;
		int wallIndex = -1;
		int feelerIndex = -1;
		
		Vector2f steeringForce = new Vector2f(0, 0);
		Vector2f xPoint = new Vector2f();
		//Vector2f closestPoint = null;
		
		for (int fi = 0; fi < feelers.length; fi++) {
			for (int wi = 0; wi < walls.size(); wi++) {
				Wall2D wall = walls.get(wi);
				if (CollisionDetector2D.intersectLines2D(
					bot.getPosition(), 
					feelers[fi], 
					wall.vA, 
					wall.vB,
					xPoint
				) && (dist = Vector2f.distSq(bot.getPosition(), xPoint)) < minDist) {
					minDist = dist;
					wallIndex = wi;
					feelerIndex = fi;
				}
			}
		}

		if (wallIndex >= 0) {
			Vector2f pV = Vector2f.sub(feelers[feelerIndex], xPoint);
			steeringForce = walls.get(wallIndex).normal().scale(pV.len());
		}
		
		return steeringForce;
	}
	
}
