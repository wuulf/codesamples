package com.rodinalex.andgames.r11;

import javax.microedition.khronos.opengles.GL10;

import org.json.JSONArray;

import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.gl.Vertices;
import com.rodinalex.andgames.fw.math.Vector2f;

public class Wall2D {
	
	public Vector2f vA;
	public Vector2f vB;
	public Vector2f vN;
	Vector2f vABn;
	public boolean inner = false;
	
	public static final int kWallWidth = 12;
	public static final int kWallSegmentLength = 32;
	public static final float kShadowLength = 480.0f;
	
	public int numberOfTextureSegments = 0;
	
	Vertices shadowVertices = null;
	
	public R11_World world;

	public Wall2D(R11_World world, Vector2f a, Vector2f b, Vector2f n, boolean inner) {
		
		vA = new Vector2f(a.x, a.y);
		vB = new Vector2f(b.x, b.y);
		vN = new Vector2f(n.x, n.y);
		vABn = Vector2f.sub(vB, vA).norm();
		this.inner = inner;
		
		this.world = world;
		
		int wallLen = (int)Vector2f.dist(vA, vB);
		
		numberOfTextureSegments = wallLen / kWallSegmentLength;
		
		//TODO - make smaller segments in textures;
		if (wallLen % kWallSegmentLength != 0) {
			System.out.println("Invalid wall length" + numberOfTextureSegments);
		}
		
		if (inner) {
			
			shadowVertices = new Vertices(world.getGraphics(), 4, 6, false, false);
			
			shadowVertices.setVertices(new float[] {
				0.0f, 0.0f,
				1.0f, 0.0f,
				1.0f, 1.0f,
				0.0f, 1.0f,
			}, 0, 8);
				
			shadowVertices.setIndices(new short[] {0, 1, 2, 2, 3, 0 }, 0, 6);
		}
		
	}
	
	public static Wall2D wallWithJsonArray(R11_World world, JSONArray arr) throws Exception {
		Wall2D wall = null;
		try {
			wall = new Wall2D(
				world,
				new Vector2f(arr.getInt(0), arr.getInt(1)),
				new Vector2f(arr.getInt(2), arr.getInt(3)),
				new Vector2f(arr.getInt(4), arr.getInt(5)),
				arr.getBoolean(6)
			);
		} catch (Exception e) {
			throw new Exception("Unable to read wall from json array: invalid config");
		}
		return wall;
	}
	
	public void render() {
		
		Vector2f vNp = vN.perpReverse();
		
		float angle = vNp.angleDeg();
		
		GL10 gl = world.getGraphics().getGL();
		
		SpriteBatcher batcher = world.screen.batcher;
		
		gl.glPushMatrix();
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glColor4f(1, 1, 1, 1);
		
		batcher.beginBatch(world.screen.texture);
		for (int i = 0; i < numberOfTextureSegments; i++) {
			float offsetFactor = kWallSegmentLength / 2  + i * kWallSegmentLength;
			batcher.drawSprite(
				vA.x  + vABn.x * offsetFactor - vN.x * kWallWidth/2, 
				vA.y  + vABn.y * offsetFactor - vN.y * kWallWidth/2, 
				kWallSegmentLength, kWallWidth, 
				angle, 
				world.screen.wallSegmentRegion
			);
		}
		batcher.endBatch();
		
		gl.glDisable(GL10.GL_TEXTURE_2D);
		gl.glPopMatrix();
	}

	public Vector2f from() {
		return vA.clone();
	}
	
	public Vector2f to() {
		return vB.clone();
	}
	
	public Vector2f normal() {
		return vN.clone();
	}
	
	public void dropShadow(Vector2f vS) {
		if (inner) {
			Vector2f vSAn = Vector2f.sub(vA, vS).norm().scale(kShadowLength);
			Vector2f vSBn = Vector2f.sub(vB, vS).norm().scale(kShadowLength);
			shadowVertices.setVertices(new float[]{
				vA.x - vN.x*kWallWidth/2, vA.y - vN.y*kWallWidth/2,
				vB.x - vN.x*kWallWidth/2, vB.y - vN.y*kWallWidth/2,
				vB.x + vSBn.x, vB.y + vSBn.y,
				vA.x + vSAn.x, vA.y + vSAn.y
			}, 0, 8);
			
			shadowVertices.bind();
			shadowVertices.draw(GL10.GL_TRIANGLES, 0, 6);
			shadowVertices.unbind();
		}
	}
}
