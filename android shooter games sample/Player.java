package com.rodinalex.andgames.r12.bot;

import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.math.Vector2f;
import com.rodinalex.andgames.r12.R12_World;
import com.rodinalex.andgames.r12.armory.R12_PlasmaRifle;
import com.rodinalex.andgames.r12.state.Puppeteer_Player;

public class Player extends Bot {

	public Player(R12_World world, Puppeteer_Player puppeteer) {
		super(world, puppeteer);
	}
	
	@Override
	public void init() {
		width = height = 0.6f;
		spriteWidth = 0.4f;
	    spriteHeight = 0.8f;
		health = maxHealth = kMaxHealth;
		speed = 1.5f;
		
		position = new Vector2f(2.0f, 1.0f);
		oldPosition = new Vector2f(0.0f, 0.0f);
		facing = new Vector2f(0.0f, 1.0f);
		heading = new Vector2f(0.0f, 1.0f);
		
		weapon = new R12_PlasmaRifle(this, world, 4.0f, 6.0f);
		world.entityManager.registerEntity(this);
		actionController = null;
		
		fov = (float)Math.PI * 2.0f/3.0f;
		viewDistanceSq = 9.0f;
		boundingRadius = 0.3f;
	}
	
	@Override
	public void update(float deltaTime) {
		
		String moveAction = actionController.getBotMoveAction(this);
		String shootAction = actionController.getBotShootAction(this);
		
		oldPosition.set(position);
		
		if (moveAction.equals("move")) {
			position.addScaled(heading, speed*deltaTime);
		}
		
		if (!world.entityCanMove(this)) {
			position.set(oldPosition);
		}
		
		if (shootAction.equals("fire")) {
			Vector2f tgt = R12_World.vectorPool.newObject();
			Vector2f.add(position, facing, tgt);
			fireAt(tgt);
			R12_World.vectorPool.free(tgt);
		} else {
			facing.set(heading);
		}

		/*weapon.update(deltaTime);
		movingState.update(deltaTime);
		shootingState.update(deltaTime); */
	}
	
	@Override
	public void fireAt(Vector2f target) {
		if (weapon.isReadyToFire()) {
			weapon.fireAt(target);
			//shootingState.setRendering("shot_rifle_plasma");
		} else {
			
		}
	}

	@Override
	public void render(SpriteBatcher batcher) {
		puppeteer.render(puppet, batcher);
	}
}
