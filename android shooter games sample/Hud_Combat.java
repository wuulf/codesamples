package com.rodinalex.andgames.r11.screen;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import com.rodinalex.andgames.fw.GLGame;
import com.rodinalex.andgames.fw.collisions.AABB2D;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.ui.GLButtonBase;
import com.rodinalex.andgames.fw.ui.UIItem;

public class Hud_Combat extends UIItem {
	
	public GLGame game;
	
	public Screen_Combat screen;
	
	public ArrayList<UIItem> items;
	
	public Hud_Combat(GLGame game, Screen_Combat screen) {
		
		this.game = game;
		this.screen = screen;
		items = new ArrayList<UIItem>();
		
		float kButtonSize = 42.0f;
		float kButtonGap  = 6.0f;
	    float kButtonBL = 10.0f;
		
		//turn left button
		items.add(new GLButtonBase(
			game, screen,
			kButtonBL, kButtonBL, kButtonSize, kButtonSize
		).setDelegate(screen));
		
		//move down button
		items.add(new GLButtonBase(
			game, screen,
			kButtonBL + kButtonSize + kButtonGap, 
			kButtonBL, 
			kButtonSize, kButtonSize
		).setDelegate(screen));
		
		//turn right button
		items.add(new GLButtonBase(
			game, screen,
			kButtonBL + 2*kButtonSize + 2*kButtonGap, 
			kButtonBL, 
			kButtonSize, kButtonSize
		).setDelegate(screen));
		
		
		//move up button
		items.add(new GLButtonBase(
			game, screen,
			kButtonBL + kButtonSize + kButtonGap, 
			kButtonBL + kButtonSize + kButtonGap, 
			kButtonSize, kButtonSize
		).setDelegate(screen));
		
		//fire weapon button
		items.add(new GLButtonBase(
			game, screen,
			480 - kButtonSize - kButtonBL, 
			kButtonBL, 
			kButtonSize, kButtonSize
		).setDelegate(screen));
	}

	@Override
	public void update(float deltaTime) {
		for (int i = 0; i < items.size(); i++) {
			items.get(i).update(deltaTime);
		}
	}

	@Override
	public void render() {
		
		GL10 gl = game.getGLGraphics().getGL();
		
		gl.glColor4f( 0.2f, 0.2f, 0.2f, 1.0f);
        /*maskVertices.bind();
        maskVertices.draw(GL10.GL_TRIANGLES, 0, 6);
        maskVertices.unbind();*/
		for (int i = 0; i < items.size(); i++) {
			items.get(i).render();
		}
		
		SpriteBatcher batcher = screen.batcher;
		
		gl.glPushMatrix();
			gl.glEnable(GL10.GL_TEXTURE_2D);
			gl.glColor4f(1, 1, 1, 1);
			batcher.beginBatch(screen.texture);
			batcher.drawSprite(240, 80, 10, 10, screen.crosshairRegion);
			batcher.endBatch();
			gl.glDisable(GL10.GL_TEXTURE_2D);
		gl.glPopMatrix();
	}

	@Override
	public AABB2D getBox() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void markTouched(boolean touchDown, int pointer) {
		// TODO Auto-generated method stub
	}

	@Override
	public int getPointer() {
		return -1;
	}

}
