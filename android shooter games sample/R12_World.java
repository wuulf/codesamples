package com.rodinalex.andgames.r12;

import java.util.ArrayList;

import javax.microedition.khronos.opengles.GL10;

import com.rodinalex.andgames.fw.Pool;
import com.rodinalex.andgames.fw.Pool.PoolObjectFactory;
import com.rodinalex.andgames.fw.collisions.AABB2D;
import com.rodinalex.andgames.fw.collisions.CollisionDetector2D;
import com.rodinalex.andgames.fw.game.EntityManager;
import com.rodinalex.andgames.fw.game.GameEntity;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.math.Vector2f;
import com.rodinalex.andgames.fw.messaging.MessageDispatcher;
import com.rodinalex.andgames.r12.armory.R12_Projectile;
import com.rodinalex.andgames.r12.bot.Bot;
import com.rodinalex.andgames.r12.bot.Player;
import com.rodinalex.andgames.r12.intel.BalloonActionController;
import com.rodinalex.andgames.r12.screen.Screen_Battle;
import com.rodinalex.andgames.r12.state.Puppeteer_Player;

public class R12_World {
	
	public static float kEpsilon = 0.001f;
	
	public static final int kMsg_BotHit = 11;
	
	public static final String kTestMap = "map/test02.json";
	
	public static final String kPlayerAnimationDescriptor = "cfg/player.des";
	public static final String kBotAnimationDescriptor = "cfg/bot.des";
	public static final String kObjectAnimationDescriptor = "cfg/objects.des";
	public static final String kProjectileAnimationDescriptor = "cfg/projectiles.des";
	
	public R12_Map map;
	
	ArrayList<R12_Projectile> projectiles;
	
	public ArrayList<Bot> bots;
	
	public MessageDispatcher messageDispatcher;
	
	public EntityManager entityManager;
	
	public BalloonActionController balloonActionController; 
	
	public R12_Game game;
	
	public Bot player;
	
	public Screen_Battle screen;
	
	private AABB2D srcBox;
	private AABB2D tgtBox;
	public static Pool<Vector2f> vectorPool;
	
	public Puppeteer_Player botMaster;

	public boolean hasProjectileToRender = false;
	public boolean hasBotToRender = false;
	
	public R12_World(R12_Game game, Screen_Battle screen) {
		this.game = game;
		this.screen = screen;
		init();
	}
	
	void init() {
		
		map = new R12_Map(this);
		
		entityManager = new EntityManager();
		
		botMaster = new Puppeteer_Player(game);
		
		messageDispatcher = new MessageDispatcher(entityManager);
		
		balloonActionController = new BalloonActionController();
		
		projectiles = new ArrayList<R12_Projectile>();
		bots = new ArrayList<Bot>();
		
		initBots();
		
		map.loadMap(kTestMap);
		
		srcBox = new AABB2D(0.0f, 0.0f, 0.0f, 0.0f);
		tgtBox = new AABB2D(0.0f, 0.0f, 0.0f, 0.0f);
		
		PoolObjectFactory<Vector2f> factory = new PoolObjectFactory<Vector2f>() {
			@Override
			public Vector2f createObject() {
				return new Vector2f(0.0f, 0.0f);
			}
		};
		vectorPool = new Pool<Vector2f>(factory, 20);
		
	}
	
	public void initBots() {
		Bot bot = new Player(this, botMaster);
		bot.setActionController(screen.hud);
		bots.add(bot);
		player = bot;
		player.position.set(2.0f, 1.0f);
	}
	
	public void addProjectile(R12_Projectile projectile) {
		projectiles.add(projectile);
	}
	
	public void clearDeadProjectiles() {
		/*for (Iterator<R12_Projectile> it = projectiles.iterator(); it.hasNext();) {
			R12_Projectile proj = it.next();
			if (proj.dead) {
				entityManager.removeEntity(proj);
				it.remove();
			}
		}*/
	}
	
	public void updateProjectiles(float deltaTime) {
		clearDeadProjectiles();
		/*int len = projectiles.size();
		for (int i = 0; i < len; i++) {
			projectiles.get(i).update(deltaTime);
		}*/
	}
	
	public void updateBots(float deltaTime) {
		int len = bots.size();
	    for (int i = 0; i < len; i++) {
	    	bots.get(i).update(deltaTime);
	    }
	}
	
	public void updateDoors(float deltaTime) {
		/*int len = map.doors.size();
		for (int i = 0; i < len; i++) {
			map.doors.get(i).update(deltaTime);
		}*/
	}
	
	public void update(float deltaTime) {
		updateProjectiles(deltaTime);
		updateBots(deltaTime);
		//TODO refactor
		updateDoors(deltaTime);
		map.update(deltaTime);
	}
	
	public void renderBots(SpriteBatcher batcher) {
	    for (int i =0; i < bots.size(); i++) {
	    	if (!bots.get(i).equals(player)) {
	    		bots.get(i).render(batcher);
	    	}
	    }
	}
	
	public void renderProjectiles(SpriteBatcher batcher) {
	    /*for (int i = 0; i < projectiles.size(); i++) {
	    	projectiles.get(i).render(batcher);
	    }*/
	}
	
	public void setEntityBoundingBox(GameEntity entity, AABB2D box) {
		Vector2f pos = vectorPool.newObject();
		pos.set(entity.position);
		float w = entity.width;
		float h = entity.height;
		box.set(pos.x - w/2.0f, pos.y - h/2.0f , w , h);
		vectorPool.free(pos);
	}
	
	public void setTileBoundingBox(int tileIndex, final AABB2D box) {
		int tileX = tileIndex % map.sizeX;
		int tileY = tileIndex / map.sizeX;
		box.set(tileX - 0.5f, tileY - 0.5f, 1.0f, 1.0f);
	}
	
	public int getPositionTileIndex(Vector2f pos) {
		return map.getTileByCoords(0, pos.x, pos.y);
	}
	
	//TODO - refactor to game entities list along with doors
	public void markObjectsToRender() {
		/*R12_Bot bot = null;
		R12_Projectile proj = null;
		
		hasProjectileToRender = false;
		hasBotToRender = false;
		
		for (int i = 0; i < bots.size(); i++) {
	    	bot = bots.get(i);
	    	if (map.isInRenderRange(bot.position)) {
	    		bot.toBeRendered = true;
	    		if (!bot.equals(player)) {
	    			hasBotToRender = true;
	    		}
	    	} else {
	    		bot.toBeRendered = false;
	    	}
	    }
		
	    for (int i = 0; i < projectiles.size(); i++) {
	    	proj = projectiles.get(i);
	    	if (map.isInRenderRange(proj.position) && !proj.dead) {
	    		proj.toBeRendered = true;
	    		hasProjectileToRender = true;
	    	} else {
	    		proj.toBeRendered = false;
	    	}
	    }*/
	}
	
	public void render(SpriteBatcher batcher) {
		
		screen.guiCam.position.set(player.position);
		screen.guiCam.setViewportAndMatrices();
		GL10 gl = game.getGLGraphics().getGL();
		gl.glEnable(GL10.GL_BLEND);
	    gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
	    map.render(batcher);
	    
	    //markObjectsToRender();
	    
	    /*Texture tex = null;
	    
	    tex = game.getAssetMaster().getTexture(playerAnimator.texturePath);
	    batcher.beginBatch(tex);
	    player.render(batcher);
		batcher.endBatch();
	    
	    if (hasBotToRender) {
		    tex = game.getAssetMaster().getTexture(botAnimator.texturePath);
		    batcher.beginBatch(tex);
		    renderBots(batcher);
			batcher.endBatch();
	    }
		
	    if (hasProjectileToRender) {
			tex = game.getAssetMaster().getTexture(projectileAnimator.texturePath);
			batcher.beginBatch(tex);
			renderProjectiles(batcher);
			batcher.endBatch();
	    } */
			
	    gl.glDisable(GL10.GL_BLEND);
	}
	
	public boolean entitiesCollideBoxes(final GameEntity fst, final GameEntity snd) {
		setEntityBoundingBox(fst, srcBox);
		setEntityBoundingBox(snd, tgtBox);
		return CollisionDetector2D.intersectAABBAABB(srcBox, tgtBox);
	}
	
	public boolean entitiesCollideCircles(final GameEntity fst, final GameEntity snd) {
		
		Vector2f pos1 = vectorPool.newObject();
		Vector2f pos2 = vectorPool.newObject();
		pos1.set(fst.position);
		pos2.set(snd.position);
		
		boolean res = CollisionDetector2D.intersectCircleCircle(
			pos1, fst.boundingRadius, 
			pos2, snd.boundingRadius
		);
		
		//clean up
		vectorPool.free(pos1);
		vectorPool.free(pos2);
		
		return res;
	}
	
	public boolean entityCanMove(GameEntity entity) {
		
		Vector2f pos = vectorPool.newObject();
		pos.set(entity.position);
		
		int tile = getPositionTileIndex(pos);
		
		//clean up after use
		vectorPool.free(pos);
		
		map.setNeighbors(0, tile);
		
		setEntityBoundingBox(entity, srcBox);
		
		int targetTile = -1;
		
		for (int i = 0; i < map.neighbors.length; i++) {
			targetTile = map.neighbors[i];
			if (targetTile != -1) {
				setTileBoundingBox(targetTile, tgtBox);
				if (CollisionDetector2D.intersectAABBAABB(srcBox, tgtBox) && map.isTileBump(0, targetTile)) {
					return false;
				}
			}
		}
		return true;
	}
	
	public boolean isLineObstructedByObjects(final Vector2f srcPoint, final Vector2f tgtPoint) {
		return false;
	}
	
	public boolean isLineObstructedByDoors(final Vector2f srcPoint, final Vector2f tgtPoint) {
		
		/*int len = map.doors.size();
		Vector2f toTarget = vectorPool.newObject();
		Vector2f.sub(tgtPoint, srcPoint, toTarget);
		Vector2f vX = vectorPool.newObject();
		
		for (int i = 0; i < len; i++) {
			setEntityBoundingBox(map.doors.get(i), srcBox);
			if (CollisionDetector2D.intersectSegmentAABB(srcPoint, tgtPoint, srcBox, vectorPool) 
				&& map.doors.get(i).closed
			){
				//clean up
				vectorPool.free(toTarget);
				vectorPool.free(vX);
				return true;
			}
		}
		

		//clean up
		vectorPool.free(toTarget);
		vectorPool.free(vX);*/
		
		return false;
	}
	
	public boolean isLineObstructedByWorldGeometry(final Vector2f srcPoint, final Vector2f tgtPoint) {
		
		float lenSq = Vector2f.distSq(srcPoint, tgtPoint);
		
		Vector2f rayEnd = vectorPool.newObject();
		Vector2f toTarget = vectorPool.newObject();
		Vector2f toTargetNorm = vectorPool.newObject();
		
		srcPoint.clone(rayEnd); //start point
		Vector2f.sub(tgtPoint, srcPoint, toTarget); //movement vector src->tgt
		toTarget.getNorm(toTargetNorm); //normalized movement vector (src->tgt)
		
		float currentLenSq = 0.0f; 
		int currentTileIndex = -1;
		
		while (currentLenSq <= lenSq) {
			rayEnd.addScaled(toTargetNorm, 0.5f);
			currentTileIndex = map.getTileByCoords(0, rayEnd.x, rayEnd.y);
			if (map.isTileBump(0, currentTileIndex)) {
				
				//clean up the mess
				vectorPool.free(rayEnd);
				vectorPool.free(toTarget);
				vectorPool.free(toTargetNorm);
				
				return true;
			}
			Vector2f.sub(rayEnd, srcPoint, toTarget);
			currentLenSq = toTarget.lenSq();
		}
		
		//clean up the mess
		vectorPool.free(rayEnd);
		vectorPool.free(toTarget);
		vectorPool.free(toTargetNorm);
		
		return false;
	}
	
	public boolean isInFOV(
			Vector2f srcPos,
			Vector2f srcFac,
			Vector2f tgtPos,
            float srcFOV
    ) {
		Vector2f toTarget = vectorPool.newObject();
		Vector2f.sub(tgtPos, srcPos, toTarget);
		toTarget.norm();
		
		boolean res = srcFac.dot(toTarget) >= Math.cos(srcFOV / 2.0f);
		
		//clean up the mess
		vectorPool.free(toTarget);
		
		return res; 
	}
}
