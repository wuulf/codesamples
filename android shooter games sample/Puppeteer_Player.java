package com.rodinalex.andgames.r12.state;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rodinalex.andgames.fw.fsm.Puppeteer;
import com.rodinalex.andgames.fw.gl.Texture;
import com.rodinalex.andgames.fw.gl.TextureRegion;
import com.rodinalex.andgames.r12.R12_Game;
import com.rodinalex.andgames.r12.bot.Bot;

public class Puppeteer_Player extends Puppeteer<Bot> {
	
	public static String kConfigFile = "cfg/player.des";
	public Texture texture;
	
	public TextureRegion[] frames;
	public State_Player_Alive stateAlive;
	public State_Player_Move_Forward stateMoveForward;
	public State_Player_Move_Backward stateMoveBackward;
	
	public Puppeteer_Player(R12_Game game) {
		try {
			//load config
			JSONObject config = game.getAssetMaster().jsonFromFile(kConfigFile);
			
			//load sprites
			texture = game.getAssetMaster().loadTextureFromFile(config.getString("texturePath"));
			JSONArray framesJSON = config.getJSONArray("frames");
			int numberOfFrames = framesJSON.length();
			frames = new TextureRegion[numberOfFrames];
			JSONArray frame = null;
			for (int i = 0; i < numberOfFrames; i++) {
				frame = framesJSON.getJSONArray(i);
				frames[i] = new TextureRegion(
					texture,
					frame.getInt(0),
					frame.getInt(1),
					frame.getInt(2),
					frame.getInt(3)
				);
			}
			
			//load states
			JSONObject states = config.getJSONObject("states");
			stateMoveForward = new State_Player_Move_Forward(states.getJSONObject("move_forward"), this);
			stateMoveBackward = new State_Player_Move_Backward(states.getJSONObject("move_backward"), this);
		} catch (Exception e) {
			throw new RuntimeException("CAN'T INITIALIZE PLAYER PUPPETEER");
		}
	}
}
