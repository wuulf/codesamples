package com.rodinalex.andgames.r11;

import com.rodinalex.andgames.fw.gl.SpriteBatcher;

public class R11_BotShootingState {

	public final float kSpriteWidth = 48.0f;
	public final float kSpriteHeight = 48.0f;
	
	public float timeElapsed = 0;
	
	public int currentFrameIndex = 0;
	
	public R11_Bot bot;
	
	public R11_World world;
	
	public R11_BotAction currentAction;
	
	public R11_BotShootingState(R11_World world, R11_Bot bot) {
		this.world = world;
		this.bot = bot;
		//TODO something is not very pleasant
		setAction("shot_idle");
	}
	
	public void setAction(String actionName) {
		if (actionName != null) {
			this.currentAction = world.botManager.getAction(actionName);
			currentFrameIndex = 0;
			timeElapsed = 0;
		} else {
			//TODO - hmm, hope I won't remove action for good
			currentAction = null;
		}
	}
	
	public void update(float deltaTime) {
		
		timeElapsed += deltaTime;
		currentFrameIndex = currentAction.getFrameIndex(timeElapsed, currentAction.looping);
		
		if (currentAction != null && 
			currentAction.looping == 0 && 
			currentFrameIndex > currentAction.getNumberOfFrames() - 1
		) {
			setAction("shot_idle");
		}
	}
	
	public void render(SpriteBatcher batcher, float angle) {
		if (currentAction != null) {
			batcher.drawSprite(
				bot.position.x,
				bot.position.y,
				kSpriteWidth,
				kSpriteHeight,
				angle,
				world.botManager.getActionFrame(currentAction.getFrameNumber(currentFrameIndex))
			);
		}
	}
}
