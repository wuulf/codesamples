package com.rodinalex.andgames.r11;

import java.util.ArrayList;
import java.util.Iterator;

import javax.microedition.khronos.opengles.GL10;

import com.rodinalex.andgames.fw.GLGraphics;
import com.rodinalex.andgames.fw.collisions.CollisionDetector2D;
import com.rodinalex.andgames.fw.game.EntityManager;
import com.rodinalex.andgames.fw.math.Vector2f;
import com.rodinalex.andgames.fw.messaging.MessageDispatcher;
import com.rodinalex.andgames.r11.armory.R11_MessageInfo_ProjectileHit;
import com.rodinalex.andgames.r11.armory.R11_Projectile;
import com.rodinalex.andgames.r11.screen.Screen_Combat;

public class R11_World {
	
	public static final int kMsg_BotHit = 11;
	
	public R11_Map map;
	
	ArrayList<R11_Projectile> projectiles;
	
	ArrayList<R11_Bot> bots;
	
	public MessageDispatcher messageDispatcher;
	
	public EntityManager entityManager;
	
	public R11_BotManager botManager;
	
	public R11_FireworkFactory fireworkFactory;
	
	public R11_PlayerBot player;
	
	float rotationAngleStep = 1.0f;
	
	public R11_Game game;
	
	public Screen_Combat screen;
	
	public R11_World(R11_Game game, Screen_Combat screen) {
		this.game = game;
		this.screen = screen;
		init();
	}
	
	void init() {
		
		map = new R11_Map(this);
		entityManager = new EntityManager();
		messageDispatcher = new MessageDispatcher(entityManager);
		fireworkFactory = new R11_FireworkFactory(this);
		botManager = new R11_BotManager(game, "cfg/Config_Bot.cfg");
		
		projectiles = new ArrayList<R11_Projectile>();
		bots = new ArrayList<R11_Bot>();
		
		if (map.loadMap("map/TestMap_00.cfg")) {
			System.out.println("Map loaded ok!");
		} else {
			System.out.println("Map load failure!");
		}
		initBots();
	}
	
	
	public void spawnBots() {
		for (int i = 0; i < bots.size(); i++) {
			tryToSpawnBot(bots.get(i));
		}
	}
	
	public void initBots() {
		//R11_Bot bot;
		
		player = new R11_PlayerBot(this);
		bots.add(player);
		
		/*bot = new R11_Bot(this);
		bot.target = player;
		bots.add(bot);
		
		bot = new R11_Bot(this);
		bot.target = player;
		bots.add(bot);*/
		
		player.target = null;
	}
	
	public GLGraphics getGraphics() {
		return this.game.getGLGraphics();
	}
	
	public GL10 getGL(){
		return this.game.getGLGraphics().getGL();
	}
	
	public void addProjectile(R11_Projectile projectile) {
		projectiles.add(projectile);
	}
	
	public void clearDeadProjectiles() {
		for (Iterator<R11_Projectile> it = projectiles.iterator(); it.hasNext();) {
			R11_Projectile proj = it.next();
			if (proj.dead) {
				entityManager.removeEntity(proj);
				it.remove();
			}
		}
	}
	
	public void updateProjectiles(float deltaTime) {
		
		clearDeadProjectiles();
		
		Vector2f currentPosition = null;
		Vector2f newPosition = null;
		Vector2f xPoint = new Vector2f();
		R11_Bot bot = null;
		R11_Projectile p = null;
		float minDist = Float.MAX_VALUE;
		float dist = 0;
		int botHitId = -1;
		R11_Bot botHit = null;
    
		for (int i = 0; i < projectiles.size(); i++) {
			p = projectiles.get(i);
    	
			//update - and then see what will happen
			//store current position
			currentPosition = p.getPosition();
			p.update(deltaTime);
			newPosition = p.getPosition();
			minDist = Float.MAX_VALUE;
			botHitId = -1;
			
			//Detect collision with walls
			for (int wi = 0; wi < map.walls.size(); wi++) {
				Wall2D wall = map.walls.get(wi);
				if (CollisionDetector2D.intersectLines2D(
					currentPosition, 
					newPosition, 
					wall.vA, 
					wall.vB,
					xPoint
				) && (dist = Vector2f.distSq(currentPosition, xPoint)) < minDist) {
					minDist = dist;
					p.dead = true;
				}
			}
			
			//Detect collision with bots
			for (int bi = 0; bi < bots.size(); bi++) {
				bot = bots.get(bi);
				if (CollisionDetector2D.intersectLineCircle2D(
					currentPosition, 
					newPosition, 
					bot.getPosition(), 
					bot.boundingRadius, 
					xPoint
				) && (dist = Vector2f.distSq(currentPosition, xPoint)) < minDist) {
					minDist = dist;
					botHitId = bot.getID();
					p.dead = true;
					botHit = bot;
				}
			}
			
			//finally if it's collided with smth - create explosion
			if (p.dead) {
				p.setPosition(xPoint);
				int explosionType = 0;
				if (botHitId != -1) {
					explosionType = 6;
					messageDispatcher.registerMessage(
						p.getID(), 
						botHitId, 
						kMsg_BotHit, 
						new R11_MessageInfo_ProjectileHit(p.getDamage())
					);
					p.setPosition(botHit.position);
				} 
				fireworkFactory.createExplosion(explosionType, p.getPosition());
			}
		}
	}
	
	public void update(float deltaTime) {
		
		updateProjectiles(deltaTime);
	    
	    fireworkFactory.update(deltaTime);
	    
	    R11_Bot bot = null;
	    
	    for (int i =0; i < bots.size(); i++) {
	    	bot = bots.get(i);
	    	bot.update(deltaTime);
	    	if (bot.botStatus == R11_Bot.kBotStatus_Dead) {
	    		tryToSpawnBot(bot);
	    	}
	    }
	    
	    map.update(deltaTime);
	}
	
	public void render() {
		GL10 gl = game.getGLGraphics().getGL();
		
		alignWorldWithPlayer();
	    map.render();
	    
	    //render bots
	    gl.glColor4f(1.0f, 0.0f, 1.0f, 1.0f);
	    for (int i = 0; i < bots.size(); i++) {
	    	bots.get(i).render();
	    }
	    
	    //render fireworks
	    fireworkFactory.render(gl);
	    
	    //render projectiles
	    for (int i = 0; i < projectiles.size(); i++) {
	    	projectiles.get(i).render();
	    }
	  
	    //renderShadows(player.getPosition());
	}
	
	void alignWorldWithPlayer() {
		
		GL10 gl = game.getGLGraphics().getGL();
		
		//TODO - WHY THE HELL 90?????!!!
		/*float rotationAngle = 90 - player.getHeading().angleDeg();
		
		rotationAngle = (int)rotationAngle % 360;
		
		float angleRad = rotationAngle * 6.28f / 360;
	    float cos = (float)Math.cos(angleRad);
	    float sin = (float)Math.sin(angleRad);*/
	    
	    Vector2f pos = player.getPosition();
	    
	    gl.glTranslatef(pos.x, pos.y, 0);
	    
	    /*gl.glTranslatef(
	    	240,// + pos.y * sin - pos.x * cos, 
	    	160,// - pos.y * cos - pos.x *sin,
	    	0
	    );*/
	    
	    //gl.glRotatef(rotationAngle, 0, 0, 1);
	}
	
	public void moveDown() {
		player.steering.target = Vector2f.add(player.position, player.getHeading().scale(-10.0f));
		player.setMovingAction("walk_backward");
	}
	
	public void moveUp() {
		player.steering.target = Vector2f.add(player.position, player.getHeading().scale(10.0f));
		player.setMovingAction("walk_forward");
	}
	
	public void rotateLeft() {
		player.heading.rotDeg(rotationAngleStep);
	}
	
	public void rotateRight() {
		player.heading.rotDeg(-rotationAngleStep);
	}
	
	public void fire() {
		player.fireAt(Vector2f.add(player.position, player.getHeading().scale(100.0f)));
	}
	
	public boolean tryToSpawnBot(R11_Bot bot) {
		int attempts = map.spawnPoints.size();
		while (--attempts >= 0) { 
			Vector2f pos = map.getRandomSpawnPoint();
			boolean available = true;
	
			for (int i = 0; i < bots.size(); i++) {
				if (Vector2f.dist(pos, bots.get(i).getPosition()) < bots.get(i).boundingRadius) {
					available = false;
				}
			}
			
			if (available) {
				bot.spawnAt(pos);
				return true;   
			}
		}
		return false;
	}
	
	void renderShadows(Vector2f source) {
		getGL().glColor4f(0.1f, 0.1f, 0.1f, 1.0f);
		for (int i = 0; i < map.walls.size(); i++) {
			map.walls.get(i).dropShadow(source);
		}
	}
	
	public void release() {
		botManager.release();
	}
}
