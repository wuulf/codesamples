package com.rodinalex.andgames.r12.state;

import org.json.JSONObject;

import com.rodinalex.andgames.fw.fsm.Puppet;
import com.rodinalex.andgames.fw.fsm.PuppetState;
import com.rodinalex.andgames.fw.fsm.Puppeteer;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.messaging.Telegram;
import com.rodinalex.andgames.r12.bot.Bot;

public class State_Player_Alive extends PuppetState<Bot> {

	public State_Player_Alive(JSONObject config, Puppeteer<Bot> puppeteer) {
		super(config, puppeteer);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(Puppet<Bot> puppet, SpriteBatcher batcher) {

	}

	@Override
	public void enter(Puppet<Bot> puppet) {
		
	}

	@Override
	public void update(Puppet<Bot> puppet, float deltaTime) {
		
	}

	@Override
	public void exit(Puppet<Bot> puppet) {
		
	}

	@Override
	public boolean handleMessage(Puppet<Bot> puppet, Telegram msg) {
		return false;
	}

}
