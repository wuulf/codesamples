package com.rodinalex.andgames.r12;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.rodinalex.andgames.fw.GLGame;
import com.rodinalex.andgames.fw.d2.MapLayer;
import com.rodinalex.andgames.fw.d2.TileSet;
import com.rodinalex.andgames.fw.game.AssetMaster;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.gl.Texture;
import com.rodinalex.andgames.fw.math.Vector2f;
import com.rodinalex.andgames.r12.map.R12_Door;

public class R12_Map {
	
	protected ArrayList<MapLayer> layers;
	protected ArrayList<TileSet> tileSets;
	public ArrayList<R12_Door> doors;
	
	private GLGame game;
	
	public R12_World world;
	
	public int[] neighbors;
	
	public int lbx; //rendering range start x
	public int lby; //rendering range start y
	public int rtx; //rendering range end x
	public int rty; //rendering range end y
	public int rrbs;
	public int rrbe;
	
	//TODO - fix it - load size from config
	public int sizeX = 15;
	public int sizeY = 15;
	public int kTileWidth = 32;
	public int kTileHeight = 32;
	
	//TODO - fix it!
	public int numberOfObjectsToRender = 0;
	
	public R12_Map(R12_World world) {
		this.game = world.game;
		this.world = world;
		neighbors = new int[9];
		layers = new ArrayList<MapLayer>();
		tileSets = new ArrayList<TileSet>();
		doors = new ArrayList<R12_Door>();
	}
	
	public void update(float deltaTime) {
		/*for (int i = 0; i < doors.size(); i++) {
			doors.get(i).update(deltaTime);
		}*/
	}
	
	public void clear() {}
	
	public boolean loadMap(String fileName) {
		
		loadFromFile(fileName);
		
		boolean result = true;
		
		clear();
		
		return result;
	}
		
	//Default method
	public int getTileByCoords(Vector2f position) {
		int col = (int)(position.x + 0.45f);
		int row = (int)(position.y + 0.45f);
		int tileIndex = row * sizeX + col;
		return tileIndex;
	}
	
	public int getTileByCoords(int layerIndex, float x, float y) {
		MapLayer layer = layers.get(layerIndex);
		int col = (int)(x + 0.5f);
		int row = (int)(y + 0.5f);
		int tileIndex = row * layer.width + col;
		return tileIndex;
	}
	
	public boolean isTileBump(int layerIndex, int tileIndex) {
		int tileType = layers.get(layerIndex).tiles[tileIndex];
		TileSet ts = tileSets.get(layerIndex);
		return ts.bumps[tileType] == 1;
	}
	
	//!!!Only works if sizeX, sizeY > 2
	public void setNeighbors(int layerIndex, int tileIndex) {
		
		MapLayer layer  = layers.get(layerIndex);
		int size = layer.size;
		int w = layer.width;
		
		int lt = tileIndex - w - 1;
		neighbors[0] = lt < 0 || lt % sizeX == w - 1 ? -1 : lt;
		
		neighbors[1] = tileIndex - w < 0 ? -1 : tileIndex - w;
		
		int rt = tileIndex - w + 1;
		neighbors[2] = rt < 0 || rt % sizeX == 0 ? -1 : rt;
		
		neighbors[3] = tileIndex % w == 0 ? -1 : tileIndex - 1;
		
		neighbors[4] = tileIndex;
		
		neighbors[5] = tileIndex % w == w - 1 ? -1 : tileIndex + 1;
		
		int lb = tileIndex + w - 1;
		neighbors[6] = lt > size || lt % w == w - 1 ? -1 : lb;
		
		neighbors[7] = tileIndex + w > size ? -1 : tileIndex + w;
		
		int rb = tileIndex + w + 1;
		neighbors[8] = rb > size || rb % w == 0 ? -1 : rb;
	}
	
	//Works for maps generated with "Tiled map editor"
	public R12_Map loadFromFile(String fileName) {
		try {
			JSONObject jsonMapConfig = game.getAssetMaster().jsonFromFile(fileName);
			//load layers
			JSONArray jsonMapLayers = jsonMapConfig.getJSONArray("layers");
			int mlLen = jsonMapLayers.length();
			if (mlLen > 0) {
				MapLayer ml = null;
				for (int i = 0; i < mlLen; i++) {
					JSONObject mlConfig = jsonMapLayers.getJSONObject(i);
					//For now - loading only tiles (object later)
					String mlType = mlConfig.getString("type");
					if (mlType.equals("tilelayer")) {
						ml = MapLayer.mapLayerWithJson(game, jsonMapLayers.getJSONObject(i));
						layers.add(ml);
						System.out.println("Map layer loaded successfully: " + ml.tiles.length);
						continue;
					}
					
					/*
					//TODO - refactor to proper factories
					if (mlType.equals("objectgroup")) {
						JSONArray objJson = mlConfig.getJSONArray("objects");
						int objLen = objJson.length();
						JSONObject objConfig = null;
						String objType = null;
						for (int objInd = 0; objInd < objLen; objInd++ ) {
							objConfig = objJson.getJSONObject(objInd);
							objType = objConfig.getString("type");
							
							if (objType.equals("door")) {
								R12_Door door = new R12_Door(world);
								door.loadFromJson(objConfig);
								doors.add(door);
								continue;
							}
							
							if (objType.equals("bot")) {
								world.botFactory.createBot(objConfig);
							}
						}
					}*/
				}
			}
			
			//load tilesets
			JSONArray jsonTileSets = jsonMapConfig.getJSONArray("tilesets");
			int tsLen = jsonTileSets.length();
			if (tsLen > 0) {
				TileSet tileSet = null;
				for (int i = 0; i < tsLen; i++) {
					//Create tileSet instance using json config
					JSONObject tsConfig = jsonTileSets.getJSONObject(i);
					int tsType = Integer.valueOf(tsConfig.getJSONObject("properties").getString("set_type"));
					//For now - loading only tiles (object later)
					if (tsType == 0) {
						tileSet = TileSet.tileSetWithJson(game, jsonTileSets.getJSONObject(i));
						//Put it in hash map for future use;
						tileSets.add(tileSet);
					} else {
					}
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("Can't load map from file '" + fileName + "' : invalid config.");
		}
		
		return null;
	}
	
	public void render(final SpriteBatcher batcher) {
		setRenderRangeBounds(world.player.position);
		markObjectsToRender();
		renderLayers(batcher);
		renderObjects(batcher);
	}
	
	//TODO - refactor to not only doors
	public void markObjectsToRender() {
		
		/*
		numberOfObjectsToRender = 0;
		int len = doors.size();
		
		R12_Door door = null;
		for (int i = 0; i < len; i++) {
			door = doors.get(i);
			if (isInRenderRange(door.position)) {
				door.toBeRendered = true;
				numberOfObjectsToRender++;
			} else {
				door.toBeRendered = false;
			}
		}*/
		
	}
	
	public void setRenderRangeBounds(Vector2f pos) {
		
		int fw = (int)world.screen.frustumWidth;
		int fh = (int)world.screen.frustumHeight;
		
		int ip = (int)(pos.y + 0.5f);
		int jp = (int)(pos.x + 0.5f);

		//bottom left coordinates
		lby = Math.max(ip - fh, 0);
		lbx = Math.max(jp - fw, 0);
		
		//top right coordinates
		rty = Math.min(ip + fh, sizeY - 1);
		rtx = Math.min(jp + fw, sizeX - 1);
		
		rrbs = lby * sizeX + lbx;
		rrbe = rty * sizeX + rtx;
	}
	
	public boolean isInRenderRange(Vector2f pos) {
		return isInRenderRange(getTileByCoords(pos));
	}
	
	public boolean isInRenderRange(int tileIndex) {
		
		int x = tileIndex % sizeX;
		int y = tileIndex / sizeX;
		
		if (tileIndex < rrbs || tileIndex > rrbe) {
			return false;
		}
		
		if (x > rtx || x < lbx)  {
			return false; 
		}
		
		if (y > rty || y < lby) {
			return false;
		}
		
		return true;
	}
	
	public void renderLayers(final SpriteBatcher batcher) {
		Texture tex = null;
		AssetMaster am = game.getAssetMaster();
		TileSet ts = null;
		MapLayer ml = null;
		for (int i = 0; i < layers.size(); i++) {
			ts = tileSets.get(i);
			ml = layers.get(i);
			tex = am.getTexture(ts.fileName);
			
			batcher.beginBatch(tex);
			
			for (int t = rrbs; t <= rrbe; t++) {
				if (isInRenderRange(t)) {
					int x = t % ml.width;
					int y = t / ml.width;
					batcher.drawSprite( x, y, 1, 1, ts.tiles.get(ml.tiles[t]));
				}
			}
			
			batcher.endBatch(); 
		}
	}
	
	public void renderObjects(final SpriteBatcher batcher) {
		
		if (numberOfObjectsToRender < 1) {
			return;
		}
		
		//int len = doors.size();
		
		/*
		batcher.beginBatch(game.getAssetMaster().getTexture(R12_AM.kItemsTexture));
		
		for (int i = 0; i < len; i++) {
			doors.get(i).render(batcher);
		}
		
		batcher.endBatch();*/
	}
}
