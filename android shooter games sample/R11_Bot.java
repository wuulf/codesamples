package com.rodinalex.andgames.r11;

import javax.microedition.khronos.opengles.GL10;

import com.rodinalex.andgames.fw.game.GameEntity;
import com.rodinalex.andgames.fw.gl.SpriteBatcher;
import com.rodinalex.andgames.fw.gl.Vertices;
import com.rodinalex.andgames.fw.math.Vector2f;
import com.rodinalex.andgames.fw.messaging.Telegram;
import com.rodinalex.andgames.fw.phys.PointMass2D;
import com.rodinalex.andgames.fw.phys.Rotatable;
import com.rodinalex.andgames.r11.armory.R11_Firearm;
import com.rodinalex.andgames.r11.armory.R11_MessageInfo_ProjectileHit;
import com.rodinalex.andgames.r11.armory.R11_ShotGun;

public class R11_Bot extends PointMass2D implements GameEntity, Rotatable {

	int id;
	public static final float kMaxTurnRate = (float)Math.PI/36.0f;
	public static final float kHealthBarWidth = 32.0f;
	public static final float kHealthBarHeight = 3.0f;
	public static final float kMaxHealth = 100.0f;
	
	//Bot statuses
	public static final int kBotStatus_Alive = 0;
	public static final int kBotStatus_Dead = 1;
	public static final int kBotStatus_Spawing = 2;
	public static final int kBotStatus_Dying = 3;
	public static final float kDying_Time = 1.5f; //1 second duration
	public static final float kSpawing_Time = 0.5f; //1 second duration
	
	public Vector2f heading;
	public float maxTurnRate;
	public R11_World world;
	public float boundingRadius;
	public float maxSpeed;
	public R11_Steering steering;
	public R11_Firearm weapon;
	public R11_Bot target;
	public float maxShootDistance = 150.0f;
	
	public R11_BotShootingState shootingState;
	public R11_BotMovingState movingState;
	
	public int botStatus = R11_Bot.kBotStatus_Dead;
	public float timeInStatus = 0.0f;
	
	public float health;
	public float maxHealth;
	
	Vertices headingVertices;
	
	public R11_Bot(R11_World world) {
		this.world = world;
		this.maxTurnRate = kMaxTurnRate;
		this.boundingRadius = 16.0f;
		this.maxSpeed = 40.0f;
		this.maxForce = 40.0f;
		this.maxHealth = kMaxHealth;
		this.heading = new Vector2f(0.0f, 1.0f);
		this.steering = new R11_Steering(this, world);
		setDefaultStats();
		this.weapon = new R11_ShotGun(this, world, 100.0f, 1.0f);
		world.entityManager.registerEntity(this);
		shootingState = new R11_BotShootingState(world, this);
		movingState = new R11_BotMovingState(world, this);
		
		/*headingVertices = new Vertices(world.getGraphics(), 2, 0, false, false);
		
		headingVertices.setVertices(new float[] {
			0, 0,
			0, 64
		}, 0, 4);*/
	}
	
	public void updateStatus(float deltaTime) {
		
		if (botStatus == kBotStatus_Spawing) {
			timeInStatus += deltaTime;
			if (timeInStatus >= kSpawing_Time) {
				timeInStatus = 0.0f;
				setAlive();
			} else {
				//TODO - continue spawning
			}
		}
		
		if (botStatus == kBotStatus_Dying) {
			timeInStatus += deltaTime;
			if (timeInStatus >= kDying_Time) {
				timeInStatus = 0.0f;
				setDead();
			} else {
				//TODO - continue dying
			}
		}
	}
	
	public void update(float deltaTime) {
		
		updateStatus(deltaTime);
		
		if (botStatus == kBotStatus_Alive) {
			this.steering.target = target.getPosition();
			this.addForce(this.steering.calculate());
			this.integrate(deltaTime);
			Vector2f newVelocity = velocity.clone();
			
			if (newVelocity.lenSq() >= 0.001f) {
				setHeading(newVelocity.norm());
			}
			
			//Test shooting
			if (targetIsReadyToBeShot( target.getPosition())) {
				fireAt( target.getPosition());
			}
			
			if (velocity.lenSq() > 0.1f) {
				setMovingAction("walk_forward");
			}
		}
		
		//TODO - hmmm??
		weapon.update(deltaTime);
		movingState.update(deltaTime);
		shootingState.update(deltaTime);
	}
	
	public void render() {
        
		GL10 gl = world.getGL();
		
		SpriteBatcher batcher = world.screen.batcher;
		gl.glPushMatrix();
		gl.glEnable(GL10.GL_TEXTURE_2D);
		gl.glColor4f(1, 1, 1, 1);
		
		Vector2f hp = heading.perp();
		
		batcher.beginBatch(world.botManager.texture);
		//TODO - what's wrong with the angle - why perp?
		//if (botStatus == kBotStatus_Dying) {
			/*batcher.drawSprite(position.x, position.y, 24, 24, world.screen.ripRegion);
		} else {
			batcher.drawSprite(
				position.x - heading.x * 18.0f + hp.x * 2.0f, 
				position.y - heading.y * 18.0f + hp.y * 2.0f,
				3, 
				32 * health/kMaxHealth,
				heading.angleDeg(),
				world.screen.healthBarRegion
			);
			batcher.drawSprite(position.x, position.y, 32, 32, hp.angleDeg(), world.screen.botRegion);*/
			movingState.render(batcher, hp.angleDeg());
			shootingState.render(batcher, hp.angleDeg());
		//}
		batcher.endBatch();
		
		gl.glDisable(GL10.GL_TEXTURE_2D);
		gl.glPopMatrix();
		
		
		/* //Heading drawing 
		gl.glColor4f(1.0f, 0.0f, 0.0f, 0.5f);
		gl.glPushMatrix();
			gl.glTranslatef(position.x, position.y, 0);
			gl.glRotatef(heading.perp().angleDeg(), 0, 0, 1);
			headingVertices.bind();
			headingVertices.draw(GL10.GL_LINES, 0, 2);
			headingVertices.unbind();
		gl.glPopMatrix();
		*/
	}
	
	public void fireAt(Vector2f target) {
		if (weapon.isReadyToFire()) {
			weapon.fireAt(target);
			shootingState.setAction("shot_shotgun");
		} else {
			
		}
	}
	
	public void spawnAt(Vector2f position) {
		if (botStatus != kBotStatus_Spawing) {
			setDefaultStats();
			setSpawning();
			setPosition(position);
		}
	}
	
	public void setDefaultStats() {
		setPosition(0.0f, 0.0f);
		setHeading(0.0f, 1.0f);
		setVelocity(0.0f, 0.0f);
		setAcceleration(0.0f, 0.0f);
		setMass(1.0f);
		setMaxHealth();
		zeroNetForce();
		setDead();
	}
	
	public void setSpawning() {
		if (botStatus != kBotStatus_Spawing) {
			timeInStatus = 0.0f;
			botStatus = kBotStatus_Spawing;
		}
	}
	
	public void setAlive() {
		if (botStatus != kBotStatus_Alive) {
			timeInStatus = 0.0f;
			botStatus = kBotStatus_Alive;
		}
	}
	
	public void setDying() {
		if (botStatus == kBotStatus_Alive) {
			timeInStatus = 0.0f;
			botStatus = kBotStatus_Dying;
		}
	}
	
	public void setDead() {
		if (botStatus != kBotStatus_Dead) {
			timeInStatus = 0.0f;
			botStatus = kBotStatus_Dead;
		}
	}
	
	public void setMaxHealth() {
		health = kMaxHealth;
	}
	
	public boolean targetIsReadyToBeShot(Vector2f target) {
		Vector2f toTarget = Vector2f.sub(target, position);
		if (Vector2f.angle(toTarget, heading) < 15.0f && toTarget.len() < maxShootDistance) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void setHeading(float x, float y) {
		heading.x = x;
		heading.y = y;
	}

	@Override
	public void setHeading(Vector2f vec) {
		heading.x = vec.x;
		heading.y = vec.y;
	}

	@Override
	public Vector2f getHeading() {
		return heading.clone();
	}

	@Override
	public boolean rotateHeadingTo(Vector2f target) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean handleMessage(Telegram msg) {
		if (msg.msgType == R11_World.kMsg_BotHit) {
			
			//if spawning, dead or dying
			if (botStatus != kBotStatus_Alive) {
				return true;
			}
			
			float damage = ((R11_MessageInfo_ProjectileHit)msg.auxInfo).damage;
			//TODO - proper handling
			if (health - damage > 0) {
				health -= damage;
			} else {
				setDying();
			}
			return true;
		}
		return false;
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public void setID(int id) {
		this.id = id;
	}
	
	public void setMovingAction(String actionName) {
		movingState.setAction(actionName);
	}
	
}
