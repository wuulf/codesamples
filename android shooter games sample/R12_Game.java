package com.rodinalex.andgames.r12;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import com.rodinalex.andgames.fw.GLGame;
import com.rodinalex.andgames.fw.gl.GLScreen;
import com.rodinalex.andgames.r12.screen.Screen_MainMenu;

public class R12_Game extends GLGame{

	boolean firstTimeCreate = true;
	
	@Override
	public GLScreen getStartScreen() {
		return new Screen_MainMenu(this);
	}
	
	@Override
	public void init() {
		super.init();
		setAssetMaster(new R12_AM(this));
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {         
	    super.onSurfaceCreated(gl, config);
	    if(firstTimeCreate) {
	    	getAssetMaster().load();
	        firstTimeCreate = false;            
	    } else {
	        getAssetMaster().reload();
	    }
	}
}
