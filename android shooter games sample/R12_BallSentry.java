package com.rodinalex.andgames.r12.bot;


public class R12_BallSentry { //extends R12_Bot {

	/*
	public R12_BallSentry(R12_World world, Animator animator) {
		super(world, animator);
	}
	
	@Override
	public void init() {
		width = height = 0.8f;
		spriteWidth = spriteHeight = 0.7f;
		maxHealth = kMaxHealth;
		speed = 0.5f;
		oldPosition = new Vector2f(0.0f, 0.0f);
		facing = new Vector2f();
		setDefaultStats();
		world.entityManager.registerEntity(this);
		movingState = new R12_BotMovingState(world, this);
		velocity.set(0.0f, speed);
		heading.set(0.0f, 1.0f);
		actionController = null;
		weapon = new R12_LaserCannonSmall(this, world, 3.0f, 1.0f);

		fov = (float)Math.PI * 2.0f/3.0f;
		viewDistanceSq = 9.0f;
		boundingRadius = 0.2f;
	}
	
	@Override
	public void update(float deltaTime) {
		
		updateStatus(deltaTime);
		
		if (botStatus == kBotStatus_Alive) {
			
			Vector2f headingVec = R12_World.vectorPool.newObject();
			Vector2f toTargetVec = R12_World.vectorPool.newObject();
			
			if (isTargetShootable(world.player.position)) {

				Vector2f.sub(world.player.position, position, toTargetVec);
				heading.set(toTargetVec.getNorm(headingVec));
				fireAt(world.player.position);
				setMovingRendering("ball_bot_move");
				
			} else {
			
				oldPosition.set(position);
				
				position.addScaled(velocity, deltaTime);
				
				heading.set(velocity.getNorm(headingVec));
				
				boolean movePossible = false;
				
				int k = 0; //failover
				
				while (k < 20) {
					k++;
					movePossible = world.entityCanMove(this);
					
					if (movePossible) {
						break;
					} else {
						position.set(oldPosition);
						
						int x = 1 - MathUtil.randomInt(0, 2);
						int y = 1 - MathUtil.randomInt(0, 2);
						
						if (x * y != 0 || (x == 0 && y == 0)) { 
							continue;
						}
						
						velocity.set(x * speed, y * speed);
						position.addScaled(velocity, deltaTime);
					}
					
					heading.set(velocity.getNorm(headingVec));
				}
			}
			
			setMovingRendering("ball_sentry_move");
			//clean up the mess
			R12_World.vectorPool.free(headingVec);
			R12_World.vectorPool.free(toTargetVec);
		}
		
		weapon.update(deltaTime);
		movingState.update(deltaTime);
	}
	
	@Override
	public void render(SpriteBatcher batcher) {
		if (!toBeRendered) return;
		float angle = heading.angleDeg() - 90;
		movingState.render(batcher, angle);
	}
	
	@Override
	public void fireAt(Vector2f target) {
		if (weapon.isReadyToFire()) {
			weapon.fireAt(target);
		} 
	}
	
	@Override
	public boolean handleMessage(Telegram msg) {
		if (msg.msgType == R12_World.kMsg_BotHit) {
			
			//if spawning, dead or dying
			if (botStatus != kBotStatus_Alive) {
				return true;
			}
			
			float damage = ((R12_MessageInfo_ProjectileHit)msg.auxInfo).damage;
			
			if (health - damage > 0) {
				health -= damage;
				//setMovingRendering("ball_sentry_hit");
			} else {
				setDying();
			}
			
			return true;
		}
		return false;
	}
	
	@Override
	public void setDying() {
		if (botStatus == kBotStatus_Alive) {
			timeInStatus = 0.0f;
			botStatus = kBotStatus_Dying;
			setMovingRendering("ball_sentry_dying");
		}
	}
	
	@Override
	public void setDead() {
		if (botStatus != kBotStatus_Dead) {
			timeInStatus = 0.0f;
			botStatus = kBotStatus_Dead;
			setMovingRendering("ball_sentry_dead");
		}
	}
	*/
}
