/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.improvead.datasync;

import com.mgm.core.env.Config;
import com.mgm.core.env.Environment;
import com.mgm.core.env.Service;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import org.javalite.activejdbc.Base;

/**
 *
 * @author alexrodin
 */
public class DataSyncWorker {
    
    private static final String MSG_TOKEN = "{{value}}";
    private static final String MSG_INVALID_RANGE = "Invalid date range {{value}}. Usage: -d <days_back>";
    private static final String MSG_INVALID_DP = "Invalid demand partner id {{value}}. Usage: -p <dp_id>";
    private static final String MSG_INVALID_NUMBER_ARGS = "Invalid number of arguments. Usage: [-p <dp_id>] [-d <days_back]";
    
    private HashMap<Integer, DataSyncHelper> helpers;
    private String[] controlKeys = new String[]{"p", "d"};
    private boolean argsOk = true;
    private boolean dpFilterOn = false;
    private boolean rangeFilterOn = false;
    
    private String dateTo;
    private String dateFrom;
    
    private static Calendar cal;
    private static SimpleDateFormat formatter;
    
    private static Mode workMode = Mode.Sync;
    
    enum Mode {
        Sync,
        SyncDetail
    }
    
    static {
        cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        formatter = new SimpleDateFormat("yyyy-MM-dd");
    }
    
    public DataSyncWorker() {
        helpers = new HashMap();        
        helpers.put(1, new AdxHelper());
        helpers.put(2, new AdSenseHelper());
        helpers.put(6, new RubiconHelper());
        helpers.put(9, new AOLHelper());
        helpers.put(11, new CriteoHelper());
        helpers.put(10, new OpenXHelper());
    }
    
    private void setDefaultDates() {
        dateTo = formatter.format(cal.getTime());
        cal.add(Calendar.DATE, -2);
        dateFrom = formatter.format(cal.getTime());
    }
    
    private void setDefaultDates(int days) {
        dateTo = formatter.format(cal.getTime());
        cal.add(Calendar.DATE, 0-days);
        dateFrom = formatter.format(cal.getTime());
    }
    
    private void setDefaultHelpers() {
        for (Map.Entry<Integer, DataSyncHelper> entry : helpers.entrySet()) {
            entry.getValue().setEnabled(true);
        }
    }
    
    private boolean hasKey(String key) {
        return Arrays.asList(controlKeys).contains(key);
    }
    
    private void parseArgs(String[] args) {
        System.out.println("Parsing args: " + Arrays.toString(args));
        for (int i = 0, len = args.length; i < len; i++) {
            String token = args[i];
            String key;
            if (token.startsWith("-")) {
                key = token.substring(1);
                if (hasKey(key)) {
                    switch (key) {
                        case "p" : {
                            if (len > i + 1) {
                                setSelectedHelper(args[i + 1]);
                            } else {
                                System.out.println(MSG_INVALID_DP.replace(MSG_TOKEN, "<null>"));
                                argsOk = false;
                            }
                            break;
                        }
                        case "d" : {
                            if (len > i + 1) {
                                setSelectedRange(args[i + 1]);
                            } else {
                                System.out.println(MSG_INVALID_RANGE.replace(MSG_TOKEN, "<null>"));
                                argsOk = false;
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
    
    private void setSelectedHelper(String arg) {
        try {
            Integer selected = Integer.valueOf(arg);
            DataSyncHelper helper = helpers.get(selected);
            if (helper == null) {
                System.out.println(MSG_INVALID_DP.replace(MSG_TOKEN, arg));
                argsOk = false;
            } else {
                helper.setEnabled(true);
                dpFilterOn = true;
                System.out.println("Helper selected: " + selected);
            }
        } catch (NumberFormatException e) {
            System.out.println(MSG_INVALID_DP.replace(MSG_TOKEN, arg));
            argsOk = false;
        }
        
    }
    
    private void setSelectedRange(String range) {
        try {
            Integer selected = Integer.valueOf(range);
            if (selected < 0) {
                System.out.println(MSG_INVALID_RANGE.replace(MSG_TOKEN, range + " argument should be greater then zero"));
                argsOk = false;
            } else {
                cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                dateTo = formatter.format(cal.getTime());
                cal.add(Calendar.DATE, -selected);
                dateFrom = formatter.format(cal.getTime());
                rangeFilterOn = true;
                System.out.println("Range selected: " + selected);
            }
        } catch (NumberFormatException e) {
            argsOk = false;
            System.out.println(MSG_INVALID_RANGE.replace(MSG_TOKEN, range));
        }
    }
    
    private void sync() {
        System.out.println("Synchronizing data from: " + dateFrom + " to: " + dateTo);
        
        if (!dpFilterOn) {
            setDefaultHelpers();
        }
        
        if (!rangeFilterOn) {
            setDefaultDates();
        }
        
        DataSyncHelper helper;
        
        for (Map.Entry<Integer, DataSyncHelper> entry : helpers.entrySet()) {
            helper = entry.getValue();
            if (helper.isEnabled()) {
                helper.setDateFrom(dateFrom);
                helper.setDateTo(dateTo);
                helper.sync();
            }
        }
    }
    
    private void syncDetail() {
        System.out.println("Synchronizing data from: " + dateFrom + " to: " + dateTo);
        
        if (!dpFilterOn) {
            setDefaultHelpers();
        }
        
        if (!rangeFilterOn) {
            setDefaultDates(2);
        }
        
        DataSyncHelper helper;
        
        for (Map.Entry<Integer, DataSyncHelper> entry : helpers.entrySet()) {
            helper = entry.getValue();
            if (helper.isEnabled()) {
                helper.setDateFrom(dateFrom);
                helper.setDateTo(dateTo);
                helper.syncDetail();
            }
        }
    }
    
    public static void main(String[] args) {
        
        System.out.println("Setting Environment " + System.getProperty("env"));
        Config.INSTANCE.setEnv(Environment.stringToEnv(System.getProperty("env")));
        
        Base.open(Service.INSTANCE.getPgpool());
        
        DataSyncWorker worker = new DataSyncWorker();
        int len = args.length;
        System.out.println("Arguments" + Arrays.toString(args));
        if (len > 0) {
            if (len == 2 || len == 4) {
                worker.parseArgs(args);
                if (!worker.argsOk) {
                    return;
                }
            } else {
                System.out.println(MSG_INVALID_NUMBER_ARGS);
                return;
            }
        }
        
        switch (System.getProperty("mode", "")) {
            case "detail":
                workMode = Mode.SyncDetail;
                break;
            default:
                workMode = Mode.Sync;
                break;
        }
        
        System.out.println("mode: "+workMode);
        
        switch (workMode) {
            case Sync:
                worker.sync();
                break;
            case SyncDetail:
                worker.syncDetail();
                break;
        }
        
        Base.close();
    }
}
