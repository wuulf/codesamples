/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.improvead.datasync;

import com.improvead.model.DemandPartner;
import com.improvead.model.report.DataInternal;
import com.improvead.service.api.DemandPartnerAPIException;
import com.improvead.service.api.rubicon.RubiconAPI;
import com.mgm.core.data.activemodel.stat.StatSiteDetail;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 * @author alexrodin
 */
public class RubiconHelper extends DataSyncHelper {
    
    public final String url = "api.rubiconproject.com";
    public final int port = 443;
    public final String username = "has been removed";
    public final String password = "has been removed";
    public final String reportURL = "https://api.rubiconproject.com/sellers/api/reports/v1/13548?"
        + "currency=EUR&"
        + "columns="
        + "Time_Date,"    
        + "Site_Name,"
        + "Traffic_Impressions,"
        + "Prorated_NetworkImpressions,"
        + "Prorated_Revenue,"
        + "Prorated_ECPM&"
        + "source=standard&start=$from$&end=$to$"
    ;
    
    public final String reportURLDetail = "https://api.rubiconproject.com/sellers/api/reports/v1/13548?"
        + "currency=EUR&"
        + "columns="
        + "Time_Date,"    
        + "Site_Name,"
        + "Size_Dimensions,"
        + "Country_Name,"
        + "Traffic_Impressions,"
        + "Prorated_NetworkImpressions,"
        + "Prorated_Revenue,"
        + "Prorated_ECPM&"
        + "source=standard&start=$from$&end=$to$"
    ;
    
    public RubiconHelper() {
        dataRecords = new ArrayList<>();
        siteMap = new HashMap<>();
        dataDetailRecords = new ArrayList<>();
        
        formatMap = new HashMap();
        demandPartnerId = DemandPartner.DEMAND_PARTNER_RUBICON_ID;
    }
    
    @Override
    public void loadData() throws DemandPartnerAPIException {
        
        System.out.println("Loading Rubicon Data: " + dateFrom + " : " + dateTo);
        
        rawData = null;
        
        try {

            CredentialsProvider credsProvider = new BasicCredentialsProvider();
            
            credsProvider.setCredentials(
                new AuthScope(url, port),
                new UsernamePasswordCredentials(username, password))
            ;
            
            CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider)
                .build()
            ;
            
            try {
                HttpGet httpget = new HttpGet(reportURL.replace("$from$", dateFrom).replace("$to$", dateTo));
                httpget.addHeader("Accept", "text/csv");
                CloseableHttpResponse response = httpclient.execute(httpget);
                try {
                    rawData = IOUtils.toString(response.getEntity().getContent());
                } finally {
                    response.close();
                }
            } finally {
                httpclient.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DemandPartnerAPIException(ex);
        }
    }
    
    @Override
    public void parseData() {
        CSVParser parser = null;
        Reader in = null;
        
        try {
            System.out.println("Parsing Rubicon Data: " + dateFrom + " : " + dateTo);
            
            if (rawData == null) {
                System.out.print("No data found!");
                return;
            }
            
            in = new StringReader(rawData);
            parser = new CSVParser(in, CSVFormat.DEFAULT);
            List<CSVRecord> records = parser.getRecords();
            
            if (records.size() == 1) {
                System.out.print("No data found!");
                return;
            }
            
            for (CSVRecord csvRecord : records.subList(1, records.size())) {
                //System.out.println(csvRecord);
                int siteId = getSiteIdByDomain(csvRecord.get(1));
                DataInternal model = DataInternal.modelWithRubiconCSVRecord(csvRecord, siteId);
                //System.out.println(model);
                if (!model.invalid) {
                    dataRecords.add(model);
                } else {
                    System.out.println(csvRecord);
                    System.out.println(model);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(RubiconAPI.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (parser != null) {
                try {
                    parser.close();
                } catch (IOException ioex) {
                    //TODO - add something
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ioex) {
                    //TODO - add something
                }
            }
        }
    }

    @Override
    public void loadDataDetail() throws DemandPartnerAPIException {
        System.out.println("Loading Rubicon Detail Data: " + dateFrom + " : " + dateTo);
        
        rawData = null;
        
//        try {
//            rawData = new String(Files.readAllBytes(Paths.get("c:\\tmp\\rubi.txt")));
//        } catch (IOException ex) {
//            Logger.getLogger(RubiconHelper.class.getName()).log(Level.SEVERE, null, ex);
//        }        
//        if (true) {
//            return;
//        }
        
        try {

            CredentialsProvider credsProvider = new BasicCredentialsProvider();
            
            credsProvider.setCredentials(
                new AuthScope(url, port),
                new UsernamePasswordCredentials(username, password))
            ;
            
            CloseableHttpClient httpclient = HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider)
                .build()
            ;
            
            try {
                HttpGet httpget = new HttpGet(reportURLDetail.replace("$from$", dateFrom).replace("$to$", dateTo));
                httpget.addHeader("Accept", "text/csv");
                CloseableHttpResponse response = httpclient.execute(httpget);
                try {
                    rawData = IOUtils.toString(response.getEntity().getContent());
                } finally {
                    response.close();
                }
                
//                try(  PrintWriter out = new PrintWriter( "c:\\tmp\\rubi.txt" )  ){
//                    out.println( rawData );
//                }
                
            } finally {
                httpclient.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new DemandPartnerAPIException(ex);
        }
    }

    @Override
    public void parseDataDetail() {
        CSVParser parser = null;
        Reader in = null;
        
        HashMap<String, String> geoMap = new HashMap<>();        
        Arrays.stream(Locale.getISOCountries())
            .forEach(t -> {
                Locale o = new Locale("", t);
                geoMap.put(o.getDisplayName().toLowerCase(), o.getCountry().toLowerCase());
            });
        
        try {
            System.out.println("Parsing Rubicon Detail Data: " + dateFrom + " : " + dateTo);
            
            if (rawData == null) {
                System.out.print("No data found!");
                return;
            }
            
            in = new StringReader(rawData);
            parser = new CSVParser(in, CSVFormat.DEFAULT);
            List<CSVRecord> records = parser.getRecords();
            
            if (records.size() == 1) {
                System.out.print("No data found!");
                return;
            }           
            
            for (CSVRecord csvRecord : records.subList(1, records.size())) {
                
                int siteId = getSiteIdByDomain(csvRecord.get(1));
                if (siteId == -1) {
                    System.out.println("ERROR: site not found: "+csvRecord.get(1));
                    continue;
                }

                int formatId = getFormatIdByName(parseFormatString(csvRecord.get(2)));
                if (formatId == -1) {
                    System.out.println("ERROR: format not found: "+csvRecord.get(2));
                    continue;
                }
                
                String countryCode = "";
                String countryName = csvRecord.get(3).toLowerCase();
                
                if (!geoMap.containsKey(countryName)) {
                    System.out.println("ERROR: country not found: "+csvRecord.get(3));
                    continue;
                } else {
                    countryCode = geoMap.get(countryName);
                }
                
                Float impsP = Float.parseFloat(csvRecord.get(5));

                StatSiteDetail model = new StatSiteDetail();
                model.setDate("date", csvRecord.get(0));
                model.set("demand_partner_id", this.demandPartnerId);
                model.set("site_id", siteId);
                model.set("format_id", formatId);
                model.set("geo", countryCode);
                model.setInteger("imps_total", csvRecord.get(4));
                model.setInteger("imps_paid", impsP.intValue());
                model.setFloat("revenue", csvRecord.get(6));

                dataDetailRecords.add(model);
            }
        } catch (IOException ex) {
            Logger.getLogger(RubiconAPI.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (parser != null) {
                try {
                    parser.close();
                } catch (IOException ioex) {
                    //TODO - add something
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException ioex) {
                    //TODO - add something
                }
            }
        }
    }
    
    protected String parseFormatString(String s) {       
        Pattern p = Pattern.compile("(\\d+x\\d+)");
        Matcher m = p.matcher(s);
        while (m.find()) {
            return m.group(0);
        }
        
        return "";
    }
}
