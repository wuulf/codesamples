/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.improvead.datasync;

import com.improvead.model.Site;
import com.improvead.model.report.DataInternal;
import com.improvead.service.ReportService;
import com.mgm.core.service.SiteService;
import com.improvead.service.api.DemandPartnerAPIException;
import com.mgm.core.data.activemodel.Format;
import com.mgm.core.data.activemodel.stat.StatSiteDetail;
import com.mgm.core.data.model.SiteModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;

/**
 *
 * @author alexrodin
 */
public abstract class DataSyncHelper {
    
    public static final int DEFAULT_FORMAT_ID = 1;
    
    protected int demandPartnerId;
    protected String rawData;
    protected String dateFrom;
    protected String dateTo;
    
    protected ArrayList<DataInternal> dataRecords;
    protected ArrayList<StatSiteDetail> dataDetailRecords;
    
    protected List<SiteModel> sites;
    protected HashMap<String, Integer> siteMap;
    protected HashMap<String, Integer> formatMap;
    
    private boolean enabled = false;
    
    //Cache sites' data for quick access
    public void beforeLoad() {
        sites = SiteService.getAll();
        for (SiteModel site : sites) {
            siteMap.put(site.getDomainName().toLowerCase(), site.getV1Id());
        }

        // fill formatmap with elements like 300x250:15
        Format.findAll().forEach(t-> {
            try {
            formatMap.put(
                String.format("%sx%s", t.getString("width"), t.getString("height")),
                t.getInteger("id"));
            } catch (Exception e) {}
        });
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }
    
    //Get siteId by domain name
    public int getSiteIdByDomain(String domain) {
        domain = domain.toLowerCase();
        Integer siteId = siteMap.get(domain);
        if (siteId == null) {
            return -1;
        }
        return siteId;
    }
    
    public int getFormatIdByName(String name) {
        name = name.toLowerCase();
        Integer formatId = formatMap.get(name);
        if (formatId == null) {
            return -1;
        }
        return formatId;
    }
    
    public void sync() {
        if (dateFrom == null || dateTo == null) {
            System.out.println("Date range not set for " + this.getClass());
        } else {
            sync(dateFrom, dateTo);
        }
    }
    
    public void sync(String dateFrom, String dateTo) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        try {
            beforeLoad();
            loadData();
            parseData();
            storeData(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void syncDetail() {
        if (dateFrom == null || dateTo == null) {
            System.out.println("Date range not set for " + this.getClass());
        } else {
            syncDetail(dateFrom, dateTo);
        }
    }
    
    public void syncDetail(String dateFrom, String dateTo) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        try {
            beforeLoad();
            loadDataDetail();
            parseDataDetail();
            storeDataDetail(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    //Store data to database
    public void storeData(boolean clearOld) {
        System.out.println("Records fetched successfully: " + dataRecords.size());
        System.out.println("Saving data for dpid: " + demandPartnerId + " from: " + dateFrom + " to: " + dateTo);
        ReportService.insertDataInternal(dataRecords, dateFrom, dateTo, demandPartnerId, clearOld);
    }
    
    //Store data to database
    public void storeDataDetail(boolean clearOld) {
        
        int cnt = 0;
        int cntErr = 0;
        
        for (StatSiteDetail model : dataDetailRecords) {
            
            if (clearOld) {
                try {
                    LazyList<Model> recs = StatSiteDetail.where("date=? and demand_partner_id=? and site_id=? and format_id=? and geo=?",
                    model.getDate("date"),
                    model.get("demand_partner_id"),
                    model.get("site_id"),
                    model.get("format_id"),
                    model.get("geo"));

                    for (Model m : recs) {
                        m.delete();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                model.saveIt();
                cnt++;
            } catch (Exception e) {
                cntErr++;
                e.printStackTrace();
            }
        }    
        
        System.out.println("Saved rows: "+cnt+" Errors: "+cntErr);
    }
    
    //Load raw data from demand partner
    public abstract void loadData() throws DemandPartnerAPIException;
    
    //Parse loaded data and create DataInternal Models
    public abstract void parseData();
    
    public abstract void loadDataDetail() throws DemandPartnerAPIException;
    
    public abstract void parseDataDetail();
}
