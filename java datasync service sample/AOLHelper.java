/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.improvead.datasync;

import com.improvead.model.DemandPartner;
import com.improvead.model.report.DataInternal;
import com.improvead.service.api.DemandPartnerAPIException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Properties;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.SearchTerm;

/**
 *
 * @author alexrodin
 */
public class AOLHelper extends DataSyncHelper {

    private static final String SERVER = "imap.gmail.com";
    private static final String LOGIN = "has been removed";
    private static final String PASSWORD = "has been removed";
            
    private static final String SUBJECT_MARKER = "AOLYESTERDAYREPORT";
    static final String DELIMETER = ";";
    private static final Calendar CALENDAR;
    private static final SimpleDateFormat FORMATTER;
    
    static {
        CALENDAR = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    }
    
    public AOLHelper() {
        dataRecords = new ArrayList<>();
        siteMap = new HashMap<>();
        demandPartnerId = DemandPartner.DEMAND_PARTNER_AOL_ID;
        dataDetailRecords = new ArrayList<>();
        formatMap = new HashMap();
    }
    
    @Override
    public void loadData() throws DemandPartnerAPIException {
        Message[] newReports = getUnreadReports();
        Message msg;
        for (int i = 0, len = newReports.length; i < len; i++) {
            try {
                msg = newReports[i];
                System.out.println(msg.getSubject() + " " +  msg.getSentDate());
                String contentType = msg.getContentType();
                String fileName = "";
                if (contentType.contains("multipart")) {
                    Multipart multiPart = (Multipart) msg.getContent();
                    int numberOfParts = multiPart.getCount();
                    System.out.println("Number of parts : " + numberOfParts);
                    //Grab only first attachment
                    for (int j = 0; j < numberOfParts; j++) {
                        MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(j);
                        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
                            fileName = part.getFileName();
                            System.out.println("Found file : " + fileName);
                            parseData(part.getInputStream());
                        }
                    }
                } else {
                    System.out.println("Invalid message - no Attachment");
                }
            } catch (MessagingException | IOException mex) {
                mex.printStackTrace();
            }
        }
    }

    @Override
    public void parseData() {
        //Does Nothing - all is done in overridden method
    }
    
    private void parseData(InputStream in) {
        BufferedReader reader = null;
        String[] tokens;
        String line;
        boolean searchingSection = true;
        boolean sectionFound = false;
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            while ((line = reader.readLine()) != null) {
                
                if (line.contains("Daily")) {
                    searchingSection = false;
                    continue;
                } 
                
                if (!searchingSection && !sectionFound) {
                    sectionFound = true;
                    continue;
                }
                
                if (sectionFound) {
                    tokens = line.split(DELIMETER);
                    tokens[0] = tokens[0].replace("\"", "");
                    int siteId = getSiteIdByDomain(tokens[1].replace("\"", ""));
                    tokens[2] = tokens[2].replace(",", "");
                    tokens[3] = tokens[3].replace(",", "");
                    DataInternal model = DataInternal.modelWithStringArrayAOL(tokens, siteId);
                    System.out.println(Arrays.toString(tokens));
                    System.out.println(model);
                    dataRecords.add(model);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    private Message[] getUnreadReports() {
        Message[] result = null;
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");
        try {
            Session session = Session.getInstance(props, null);
            Store store = session.getStore();
            store.connect(SERVER, LOGIN, PASSWORD);
            Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_ONLY);
            result = inbox.search(new SearchTerm() {
                @Override
                public boolean match(Message msg) {
                    try {
                        //MOVE DATE AHEAD 1 DAY DUE TO THE FACT THAT REPORT SENT IS FOR PREVIOUS DAY
                        CALENDAR.setTime(FORMATTER.parse(dateFrom));
                        CALENDAR.add(Calendar.DATE, 1);
                        if (msg.getSubject().contains(SUBJECT_MARKER) 
                            && msg.getSentDate().after(CALENDAR.getTime())) 
                        {
                            return true;
                        }
                    } catch (MessagingException | java.text.ParseException ex) {
                        Logger.getLogger(AOLHelper.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    
    public static void main(String[] args) {
        AOLHelper helper = new AOLHelper();
        try {
            helper.setDateFrom("2016-11-09");
            helper.loadData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadDataDetail() throws DemandPartnerAPIException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void parseDataDetail() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
